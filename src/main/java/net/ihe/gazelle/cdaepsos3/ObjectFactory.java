package net.ihe.gazelle.cdaepsos3;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {}
    
    /**
     * Create an instance of {@link  POCDMT000040InfrastructureRootTypeId}
     * 
     */
    public POCDMT000040InfrastructureRootTypeId createPOCDMT000040InfrastructureRootTypeId() {
        return new POCDMT000040InfrastructureRootTypeId();
    }

    /**
     * Create an instance of {@link  POCDMT000040RecordTarget}
     * 
     */
    public POCDMT000040RecordTarget createPOCDMT000040RecordTarget() {
        return new POCDMT000040RecordTarget();
    }

    /**
     * Create an instance of {@link  POCDMT000040PatientRole}
     * 
     */
    public POCDMT000040PatientRole createPOCDMT000040PatientRole() {
        return new POCDMT000040PatientRole();
    }

    /**
     * Create an instance of {@link  POCDMT000040Patient}
     * 
     */
    public POCDMT000040Patient createPOCDMT000040Patient() {
        return new POCDMT000040Patient();
    }

    /**
     * Create an instance of {@link  POCDMT000040Guardian}
     * 
     */
    public POCDMT000040Guardian createPOCDMT000040Guardian() {
        return new POCDMT000040Guardian();
    }

    /**
     * Create an instance of {@link  POCDMT000040Person}
     * 
     */
    public POCDMT000040Person createPOCDMT000040Person() {
        return new POCDMT000040Person();
    }

    /**
     * Create an instance of {@link  POCDMT000040Organization}
     * 
     */
    public POCDMT000040Organization createPOCDMT000040Organization() {
        return new POCDMT000040Organization();
    }

    /**
     * Create an instance of {@link  POCDMT000040OrganizationPartOf}
     * 
     */
    public POCDMT000040OrganizationPartOf createPOCDMT000040OrganizationPartOf() {
        return new POCDMT000040OrganizationPartOf();
    }

    /**
     * Create an instance of {@link  POCDMT000040Birthplace}
     * 
     */
    public POCDMT000040Birthplace createPOCDMT000040Birthplace() {
        return new POCDMT000040Birthplace();
    }

    /**
     * Create an instance of {@link  POCDMT000040Place}
     * 
     */
    public POCDMT000040Place createPOCDMT000040Place() {
        return new POCDMT000040Place();
    }

    /**
     * Create an instance of {@link  POCDMT000040LanguageCommunication}
     * 
     */
    public POCDMT000040LanguageCommunication createPOCDMT000040LanguageCommunication() {
        return new POCDMT000040LanguageCommunication();
    }

    /**
     * Create an instance of {@link  POCDMT000040Author}
     * 
     */
    public POCDMT000040Author createPOCDMT000040Author() {
        return new POCDMT000040Author();
    }

    /**
     * Create an instance of {@link  POCDMT000040AssignedAuthor}
     * 
     */
    public POCDMT000040AssignedAuthor createPOCDMT000040AssignedAuthor() {
        return new POCDMT000040AssignedAuthor();
    }

    /**
     * Create an instance of {@link  POCDMT000040AuthoringDevice}
     * 
     */
    public POCDMT000040AuthoringDevice createPOCDMT000040AuthoringDevice() {
        return new POCDMT000040AuthoringDevice();
    }

    /**
     * Create an instance of {@link  POCDMT000040MaintainedEntity}
     * 
     */
    public POCDMT000040MaintainedEntity createPOCDMT000040MaintainedEntity() {
        return new POCDMT000040MaintainedEntity();
    }

    /**
     * Create an instance of {@link  POCDMT000040DataEnterer}
     * 
     */
    public POCDMT000040DataEnterer createPOCDMT000040DataEnterer() {
        return new POCDMT000040DataEnterer();
    }

    /**
     * Create an instance of {@link  POCDMT000040AssignedEntity}
     * 
     */
    public POCDMT000040AssignedEntity createPOCDMT000040AssignedEntity() {
        return new POCDMT000040AssignedEntity();
    }

    /**
     * Create an instance of {@link  POCDMT000040Informant12}
     * 
     */
    public POCDMT000040Informant12 createPOCDMT000040Informant12() {
        return new POCDMT000040Informant12();
    }

    /**
     * Create an instance of {@link  POCDMT000040RelatedEntity}
     * 
     */
    public POCDMT000040RelatedEntity createPOCDMT000040RelatedEntity() {
        return new POCDMT000040RelatedEntity();
    }

    /**
     * Create an instance of {@link  POCDMT000040Custodian}
     * 
     */
    public POCDMT000040Custodian createPOCDMT000040Custodian() {
        return new POCDMT000040Custodian();
    }

    /**
     * Create an instance of {@link  POCDMT000040AssignedCustodian}
     * 
     */
    public POCDMT000040AssignedCustodian createPOCDMT000040AssignedCustodian() {
        return new POCDMT000040AssignedCustodian();
    }

    /**
     * Create an instance of {@link  POCDMT000040CustodianOrganization}
     * 
     */
    public POCDMT000040CustodianOrganization createPOCDMT000040CustodianOrganization() {
        return new POCDMT000040CustodianOrganization();
    }

    /**
     * Create an instance of {@link  POCDMT000040InformationRecipient}
     * 
     */
    public POCDMT000040InformationRecipient createPOCDMT000040InformationRecipient() {
        return new POCDMT000040InformationRecipient();
    }

    /**
     * Create an instance of {@link  POCDMT000040IntendedRecipient}
     * 
     */
    public POCDMT000040IntendedRecipient createPOCDMT000040IntendedRecipient() {
        return new POCDMT000040IntendedRecipient();
    }

    /**
     * Create an instance of {@link  POCDMT000040LegalAuthenticator}
     * 
     */
    public POCDMT000040LegalAuthenticator createPOCDMT000040LegalAuthenticator() {
        return new POCDMT000040LegalAuthenticator();
    }

    /**
     * Create an instance of {@link  POCDMT000040Authenticator}
     * 
     */
    public POCDMT000040Authenticator createPOCDMT000040Authenticator() {
        return new POCDMT000040Authenticator();
    }

    /**
     * Create an instance of {@link  POCDMT000040Participant1}
     * 
     */
    public POCDMT000040Participant1 createPOCDMT000040Participant1() {
        return new POCDMT000040Participant1();
    }

    /**
     * Create an instance of {@link  POCDMT000040AssociatedEntity}
     * 
     */
    public POCDMT000040AssociatedEntity createPOCDMT000040AssociatedEntity() {
        return new POCDMT000040AssociatedEntity();
    }

    /**
     * Create an instance of {@link  POCDMT000040InFulfillmentOf}
     * 
     */
    public POCDMT000040InFulfillmentOf createPOCDMT000040InFulfillmentOf() {
        return new POCDMT000040InFulfillmentOf();
    }

    /**
     * Create an instance of {@link  POCDMT000040Order}
     * 
     */
    public POCDMT000040Order createPOCDMT000040Order() {
        return new POCDMT000040Order();
    }

    /**
     * Create an instance of {@link  POCDMT000040DocumentationOf}
     * 
     */
    public POCDMT000040DocumentationOf createPOCDMT000040DocumentationOf() {
        return new POCDMT000040DocumentationOf();
    }

    /**
     * Create an instance of {@link  POCDMT000040ServiceEvent}
     * 
     */
    public POCDMT000040ServiceEvent createPOCDMT000040ServiceEvent() {
        return new POCDMT000040ServiceEvent();
    }

    /**
     * Create an instance of {@link  POCDMT000040Performer1}
     * 
     */
    public POCDMT000040Performer1 createPOCDMT000040Performer1() {
        return new POCDMT000040Performer1();
    }

    /**
     * Create an instance of {@link  POCDMT000040RelatedDocument}
     * 
     */
    public POCDMT000040RelatedDocument createPOCDMT000040RelatedDocument() {
        return new POCDMT000040RelatedDocument();
    }

    /**
     * Create an instance of {@link  POCDMT000040ParentDocument}
     * 
     */
    public POCDMT000040ParentDocument createPOCDMT000040ParentDocument() {
        return new POCDMT000040ParentDocument();
    }

    /**
     * Create an instance of {@link  POCDMT000040ClinicalDocument}
     * 
     */
    public POCDMT000040ClinicalDocument createPOCDMT000040ClinicalDocument() {
        return new POCDMT000040ClinicalDocument();
    }

    /**
     * Create an instance of {@link  POCDMT000040Authorization}
     * 
     */
    public POCDMT000040Authorization createPOCDMT000040Authorization() {
        return new POCDMT000040Authorization();
    }

    /**
     * Create an instance of {@link  POCDMT000040Consent}
     * 
     */
    public POCDMT000040Consent createPOCDMT000040Consent() {
        return new POCDMT000040Consent();
    }

    /**
     * Create an instance of {@link  POCDMT000040Component1}
     * 
     */
    public POCDMT000040Component1 createPOCDMT000040Component1() {
        return new POCDMT000040Component1();
    }

    /**
     * Create an instance of {@link  POCDMT000040EncompassingEncounter}
     * 
     */
    public POCDMT000040EncompassingEncounter createPOCDMT000040EncompassingEncounter() {
        return new POCDMT000040EncompassingEncounter();
    }

    /**
     * Create an instance of {@link  POCDMT000040ResponsibleParty}
     * 
     */
    public POCDMT000040ResponsibleParty createPOCDMT000040ResponsibleParty() {
        return new POCDMT000040ResponsibleParty();
    }

    /**
     * Create an instance of {@link  POCDMT000040EncounterParticipant}
     * 
     */
    public POCDMT000040EncounterParticipant createPOCDMT000040EncounterParticipant() {
        return new POCDMT000040EncounterParticipant();
    }

    /**
     * Create an instance of {@link  POCDMT000040Location}
     * 
     */
    public POCDMT000040Location createPOCDMT000040Location() {
        return new POCDMT000040Location();
    }

    /**
     * Create an instance of {@link  POCDMT000040HealthCareFacility}
     * 
     */
    public POCDMT000040HealthCareFacility createPOCDMT000040HealthCareFacility() {
        return new POCDMT000040HealthCareFacility();
    }

    /**
     * Create an instance of {@link  POCDMT000040Component2}
     * 
     */
    public POCDMT000040Component2 createPOCDMT000040Component2() {
        return new POCDMT000040Component2();
    }

    /**
     * Create an instance of {@link  POCDMT000040NonXMLBody}
     * 
     */
    public POCDMT000040NonXMLBody createPOCDMT000040NonXMLBody() {
        return new POCDMT000040NonXMLBody();
    }

    /**
     * Create an instance of {@link  POCDMT000040StructuredBody}
     * 
     */
    public POCDMT000040StructuredBody createPOCDMT000040StructuredBody() {
        return new POCDMT000040StructuredBody();
    }

    /**
     * Create an instance of {@link  POCDMT000040Component3}
     * 
     */
    public POCDMT000040Component3 createPOCDMT000040Component3() {
        return new POCDMT000040Component3();
    }

    /**
     * Create an instance of {@link  POCDMT000040Section}
     * 
     */
    public POCDMT000040Section createPOCDMT000040Section() {
        return new POCDMT000040Section();
    }

    /**
     * Create an instance of {@link  POCDMT000040Subject}
     * 
     */
    public POCDMT000040Subject createPOCDMT000040Subject() {
        return new POCDMT000040Subject();
    }

    /**
     * Create an instance of {@link  POCDMT000040RelatedSubject}
     * 
     */
    public POCDMT000040RelatedSubject createPOCDMT000040RelatedSubject() {
        return new POCDMT000040RelatedSubject();
    }

    /**
     * Create an instance of {@link  POCDMT000040SubjectPerson}
     * 
     */
    public POCDMT000040SubjectPerson createPOCDMT000040SubjectPerson() {
        return new POCDMT000040SubjectPerson();
    }

    /**
     * Create an instance of {@link  POCDMT000040Entry}
     * 
     */
    public POCDMT000040Entry createPOCDMT000040Entry() {
        return new POCDMT000040Entry();
    }

    /**
     * Create an instance of {@link  POCDMT000040Act}
     * 
     */
    public POCDMT000040Act createPOCDMT000040Act() {
        return new POCDMT000040Act();
    }

    /**
     * Create an instance of {@link  POCDMT000040Specimen}
     * 
     */
    public POCDMT000040Specimen createPOCDMT000040Specimen() {
        return new POCDMT000040Specimen();
    }

    /**
     * Create an instance of {@link  POCDMT000040SpecimenRole}
     * 
     */
    public POCDMT000040SpecimenRole createPOCDMT000040SpecimenRole() {
        return new POCDMT000040SpecimenRole();
    }

    /**
     * Create an instance of {@link  POCDMT000040PlayingEntity}
     * 
     */
    public POCDMT000040PlayingEntity createPOCDMT000040PlayingEntity() {
        return new POCDMT000040PlayingEntity();
    }

    /**
     * Create an instance of {@link  POCDMT000040Performer2}
     * 
     */
    public POCDMT000040Performer2 createPOCDMT000040Performer2() {
        return new POCDMT000040Performer2();
    }

    /**
     * Create an instance of {@link  POCDMT000040Participant2}
     * 
     */
    public POCDMT000040Participant2 createPOCDMT000040Participant2() {
        return new POCDMT000040Participant2();
    }

    /**
     * Create an instance of {@link  POCDMT000040ParticipantRole}
     * 
     */
    public POCDMT000040ParticipantRole createPOCDMT000040ParticipantRole() {
        return new POCDMT000040ParticipantRole();
    }

    /**
     * Create an instance of {@link  POCDMT000040Device}
     * 
     */
    public POCDMT000040Device createPOCDMT000040Device() {
        return new POCDMT000040Device();
    }

    /**
     * Create an instance of {@link  POCDMT000040Entity}
     * 
     */
    public POCDMT000040Entity createPOCDMT000040Entity() {
        return new POCDMT000040Entity();
    }

    /**
     * Create an instance of {@link  POCDMT000040EntryRelationship}
     * 
     */
    public POCDMT000040EntryRelationship createPOCDMT000040EntryRelationship() {
        return new POCDMT000040EntryRelationship();
    }

    /**
     * Create an instance of {@link  POCDMT000040Encounter}
     * 
     */
    public POCDMT000040Encounter createPOCDMT000040Encounter() {
        return new POCDMT000040Encounter();
    }

    /**
     * Create an instance of {@link  POCDMT000040Reference}
     * 
     */
    public POCDMT000040Reference createPOCDMT000040Reference() {
        return new POCDMT000040Reference();
    }

    /**
     * Create an instance of {@link  POCDMT000040ExternalAct}
     * 
     */
    public POCDMT000040ExternalAct createPOCDMT000040ExternalAct() {
        return new POCDMT000040ExternalAct();
    }

    /**
     * Create an instance of {@link  POCDMT000040ExternalObservation}
     * 
     */
    public POCDMT000040ExternalObservation createPOCDMT000040ExternalObservation() {
        return new POCDMT000040ExternalObservation();
    }

    /**
     * Create an instance of {@link  POCDMT000040ExternalProcedure}
     * 
     */
    public POCDMT000040ExternalProcedure createPOCDMT000040ExternalProcedure() {
        return new POCDMT000040ExternalProcedure();
    }

    /**
     * Create an instance of {@link  POCDMT000040ExternalDocument}
     * 
     */
    public POCDMT000040ExternalDocument createPOCDMT000040ExternalDocument() {
        return new POCDMT000040ExternalDocument();
    }

    /**
     * Create an instance of {@link  POCDMT000040Precondition}
     * 
     */
    public POCDMT000040Precondition createPOCDMT000040Precondition() {
        return new POCDMT000040Precondition();
    }

    /**
     * Create an instance of {@link  POCDMT000040Criterion}
     * 
     */
    public POCDMT000040Criterion createPOCDMT000040Criterion() {
        return new POCDMT000040Criterion();
    }

    /**
     * Create an instance of {@link  POCDMT000040Observation}
     * 
     */
    public POCDMT000040Observation createPOCDMT000040Observation() {
        return new POCDMT000040Observation();
    }

    /**
     * Create an instance of {@link  POCDMT000040ReferenceRange}
     * 
     */
    public POCDMT000040ReferenceRange createPOCDMT000040ReferenceRange() {
        return new POCDMT000040ReferenceRange();
    }

    /**
     * Create an instance of {@link  POCDMT000040ObservationRange}
     * 
     */
    public POCDMT000040ObservationRange createPOCDMT000040ObservationRange() {
        return new POCDMT000040ObservationRange();
    }

    /**
     * Create an instance of {@link  POCDMT000040ObservationMedia}
     * 
     */
    public POCDMT000040ObservationMedia createPOCDMT000040ObservationMedia() {
        return new POCDMT000040ObservationMedia();
    }

    /**
     * Create an instance of {@link  POCDMT000040Organizer}
     * 
     */
    public POCDMT000040Organizer createPOCDMT000040Organizer() {
        return new POCDMT000040Organizer();
    }

    /**
     * Create an instance of {@link  POCDMT000040Component4}
     * 
     */
    public POCDMT000040Component4 createPOCDMT000040Component4() {
        return new POCDMT000040Component4();
    }

    /**
     * Create an instance of {@link  POCDMT000040Procedure}
     * 
     */
    public POCDMT000040Procedure createPOCDMT000040Procedure() {
        return new POCDMT000040Procedure();
    }

    /**
     * Create an instance of {@link  POCDMT000040RegionOfInterest}
     * 
     */
    public POCDMT000040RegionOfInterest createPOCDMT000040RegionOfInterest() {
        return new POCDMT000040RegionOfInterest();
    }

    /**
     * Create an instance of {@link  POCDMT000040RegionOfInterestValue}
     * 
     */
    public POCDMT000040RegionOfInterestValue createPOCDMT000040RegionOfInterestValue() {
        return new POCDMT000040RegionOfInterestValue();
    }

    /**
     * Create an instance of {@link  POCDMT000040SubstanceAdministration}
     * 
     */
    public POCDMT000040SubstanceAdministration createPOCDMT000040SubstanceAdministration() {
        return new POCDMT000040SubstanceAdministration();
    }

    /**
     * Create an instance of {@link  POCDMT000040Consumable}
     * 
     */
    public POCDMT000040Consumable createPOCDMT000040Consumable() {
        return new POCDMT000040Consumable();
    }

    /**
     * Create an instance of {@link  POCDMT000040ManufacturedProduct}
     * 
     */
    public POCDMT000040ManufacturedProduct createPOCDMT000040ManufacturedProduct() {
        return new POCDMT000040ManufacturedProduct();
    }

    /**
     * Create an instance of {@link  POCDMT000040LabeledDrug}
     * 
     */
    public POCDMT000040LabeledDrug createPOCDMT000040LabeledDrug() {
        return new POCDMT000040LabeledDrug();
    }

    /**
     * Create an instance of {@link  POCDMT000040Material}
     * 
     */
    public POCDMT000040Material createPOCDMT000040Material() {
        return new POCDMT000040Material();
    }

    /**
     * Create an instance of {@link  POCDMT000040Supply}
     * 
     */
    public POCDMT000040Supply createPOCDMT000040Supply() {
        return new POCDMT000040Supply();
    }

    /**
     * Create an instance of {@link  POCDMT000040Product}
     * 
     */
    public POCDMT000040Product createPOCDMT000040Product() {
        return new POCDMT000040Product();
    }

    /**
     * Create an instance of {@link  POCDMT000040Component5}
     * 
     */
    public POCDMT000040Component5 createPOCDMT000040Component5() {
        return new POCDMT000040Component5();
    }

    /**
     * Create an instance of {@link  TSEPSOSTZ}
     * 
     */
    public TSEPSOSTZ createTSEPSOSTZ() {
        return new TSEPSOSTZ();
    }




}
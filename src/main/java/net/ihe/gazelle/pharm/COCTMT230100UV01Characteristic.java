package net.ihe.gazelle.pharm;


import net.ihe.gazelle.datatypes.CS;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.voc.ActClassObservation;
import net.ihe.gazelle.voc.NullFlavor;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.ArrayList;
import java.util.List;

/**
 * Description of the class COCTMT230100UV01Characteristic.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "COCT_MT230100UV01.Characteristic", propOrder = {
        "realmCode",
        "typeId",
        "templateId",
        "classCode",
        "moodCode",
        "nullFlavor"
})
@XmlRootElement(name = "COCT_MT230100UV01.Characteristic")
public class COCTMT230100UV01Characteristic implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @XmlElement(name = "realmCode", namespace = "urn:hl7-org:pharm")
    public List<CS> realmCode;
    @XmlElement(name = "typeId", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.infr.AllInfrastructureRootTypeId typeId;
    @XmlElement(name = "templateId", namespace = "urn:hl7-org:pharm")
    public List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> templateId;
    @XmlAttribute(name = "nullFlavor")
    public NullFlavor nullFlavor;
    @XmlAttribute(name = "classCode", required = true)
    public ActClassObservation classCode;
    @XmlAttribute(name = "moodCode", required = true)
    public net.ihe.gazelle.medication2.ActMoodEventOccurrence2 moodCode;

    /**
     * An attribute containing marshalled element node
     */
    @XmlTransient
    private org.w3c.dom.Node _xmlNodePresentation;


    /**
     * Return realmCode.
     * @return realmCode
     */
    public List<net.ihe.gazelle.datatypes.CS> getRealmCode() {
        if (realmCode == null) {
            realmCode = new ArrayList<CS>();
        }
        return realmCode;
    }

    /**
     * Set a value to attribute realmCode.
     * @param realmCode.
     */
    public void setRealmCode(List<net.ihe.gazelle.datatypes.CS> realmCode) {
        this.realmCode = realmCode;
    }



    /**
     * Add a realmCode to the realmCode collection.
     * @param realmCode_elt Element to add.
     */
    public void addRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
        this.getRealmCode().add(realmCode_elt);
    }

    /**
     * Remove a realmCode to the realmCode collection.
     * @param realmCode_elt Element to remove
     */
    public void removeRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
        this.getRealmCode().remove(realmCode_elt);
    }

    /**
     * Return typeId.
     * @return typeId
     */
    public net.ihe.gazelle.infr.AllInfrastructureRootTypeId getTypeId() {
        return typeId;
    }

    /**
     * Set a value to attribute typeId.
     * @param typeId.
     */
    public void setTypeId(net.ihe.gazelle.infr.AllInfrastructureRootTypeId typeId) {
        this.typeId = typeId;
    }




    /**
     * Return templateId.
     * @return templateId
     */
    public List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> getTemplateId() {
        if (templateId == null) {
            templateId = new ArrayList<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId>();
        }
        return templateId;
    }

    /**
     * Set a value to attribute templateId.
     * @param templateId.
     */
    public void setTemplateId(List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> templateId) {
        this.templateId = templateId;
    }



    /**
     * Add a templateId to the templateId collection.
     * @param templateId_elt Element to add.
     */
    public void addTemplateId(net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId_elt) {
        this.getTemplateId().add(templateId_elt);
    }

    /**
     * Remove a templateId to the templateId collection.
     * @param templateId_elt Element to remove
     */
    public void removeTemplateId(net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId_elt) {
        this.getTemplateId().remove(templateId_elt);
    }

    /**
     * Return classCode.
     * @return classCode
     */
    public ActClassObservation getClassCode() {
        return classCode;
    }

    /**
     * Set a value to attribute classCode.
     * @param classCode.
     */
    public void setClassCode(ActClassObservation classCode) {
        this.classCode = classCode;
    }




    /**
     * Return moodCode.
     * @return moodCode
     */
    public net.ihe.gazelle.medication2.ActMoodEventOccurrence2 getMoodCode() {
        return moodCode;
    }

    /**
     * Set a value to attribute moodCode.
     * @param moodCode.
     */
    public void setMoodCode(net.ihe.gazelle.medication2.ActMoodEventOccurrence2 moodCode) {
        this.moodCode = moodCode;
    }




    /**
     * Return nullFlavor.
     * @return nullFlavor
     */
    public NullFlavor getNullFlavor() {
        return nullFlavor;
    }

    /**
     * Set a value to attribute nullFlavor.
     * @param nullFlavor.
     */
    public void setNullFlavor(NullFlavor nullFlavor) {
        this.nullFlavor = nullFlavor;
    }





    public Node get_xmlNodePresentation() {
        if (_xmlNodePresentation == null){
            JAXBContext jc;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder db = null;
            Document doc = null;
            try {
                db = dbf.newDocumentBuilder();
                doc = db.newDocument();
            } catch (ParserConfigurationException e1) {}
            try {
                jc = JAXBContext.newInstance("net.ihe.gazelle.pharm");
                Marshaller m = jc.createMarshaller();
                m.marshal(this, doc);
                _xmlNodePresentation = doc.getElementsByTagNameNS("urn:hl7-org:pharm", "COCT_MT230100UV01.Characteristic").item(0);
            } catch (JAXBException e) {
                try{
                    db = dbf.newDocumentBuilder();
                    _xmlNodePresentation = db.newDocument();
                }
                catch(Exception ee){}
            }
        }
        return _xmlNodePresentation;
    }

    public void set_xmlNodePresentation(Node _xmlNodePresentation) {
        this._xmlNodePresentation = _xmlNodePresentation;
    }





    /**
     * validate by a module of validation
     *
     */
    public static void validateByModule(net.ihe.gazelle.pharm.COCTMT230100UV01Characteristic cOCTMT230100UV01Characteristic, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
        if (cOCTMT230100UV01Characteristic != null){
            cvm.validate(cOCTMT230100UV01Characteristic, _location, diagnostic);
            {
                int i = 0;
                for (net.ihe.gazelle.datatypes.CS realmCode: cOCTMT230100UV01Characteristic.getRealmCode()){
                    net.ihe.gazelle.datatypes.CS.validateByModule(realmCode, _location + "/realmCode[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            net.ihe.gazelle.infr.AllInfrastructureRootTypeId.validateByModule(cOCTMT230100UV01Characteristic.getTypeId(), _location + "/typeId", cvm, diagnostic);
            {
                int i = 0;
                for (net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId: cOCTMT230100UV01Characteristic.getTemplateId()){
                    net.ihe.gazelle.infr.AllInfrastructureRootTemplateId.validateByModule(templateId, _location + "/templateId[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }
        }
    }

}

package net.ihe.gazelle.pharm;


import net.ihe.gazelle.datatypes.CS;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.voc.NullFlavor;
import net.ihe.gazelle.voc.RoleClassIngredientEntity;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.ArrayList;
import java.util.List;

/**
 * Description of the class COCTMT230100UV01Ingredient.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "COCT_MT230100UV01.Ingredient", propOrder = {
        "realmCode",
        "typeId",
        "templateId",
        "quantity",
        "ingredientSubstance",
        "classCode",
        "nullFlavor"
})
@XmlRootElement(name = "COCT_MT230100UV01.Ingredient")
public class COCTMT230100UV01Ingredient implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @XmlElement(name = "realmCode", namespace = "urn:hl7-org:pharm")
    public List<CS> realmCode;
    @XmlElement(name = "typeId", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.infr.AllInfrastructureRootTypeId typeId;
    @XmlElement(name = "templateId", namespace = "urn:hl7-org:pharm")
    public List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> templateId;
    @XmlElement(name = "quantity", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.datatypes.RTO quantity;
    @XmlElement(name = "ingredientSubstance", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.pharm.COCTMT230100UV01Substance ingredientSubstance;
    @XmlAttribute(name = "nullFlavor")
    public NullFlavor nullFlavor;
    @XmlAttribute(name = "classCode", required = true)
    public RoleClassIngredientEntity classCode;

    /**
     * An attribute containing marshalled element node
     */
    @XmlTransient
    private org.w3c.dom.Node _xmlNodePresentation;


    /**
     * Return realmCode.
     * @return realmCode
     */
    public List<net.ihe.gazelle.datatypes.CS> getRealmCode() {
        if (realmCode == null) {
            realmCode = new ArrayList<CS>();
        }
        return realmCode;
    }

    /**
     * Set a value to attribute realmCode.
     * @param realmCode.
     */
    public void setRealmCode(List<net.ihe.gazelle.datatypes.CS> realmCode) {
        this.realmCode = realmCode;
    }



    /**
     * Add a realmCode to the realmCode collection.
     * @param realmCode_elt Element to add.
     */
    public void addRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
        this.realmCode.add(realmCode_elt);
    }

    /**
     * Remove a realmCode to the realmCode collection.
     * @param realmCode_elt Element to remove
     */
    public void removeRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
        this.realmCode.remove(realmCode_elt);
    }

    /**
     * Return typeId.
     * @return typeId
     */
    public net.ihe.gazelle.infr.AllInfrastructureRootTypeId getTypeId() {
        return typeId;
    }

    /**
     * Set a value to attribute typeId.
     * @param typeId.
     */
    public void setTypeId(net.ihe.gazelle.infr.AllInfrastructureRootTypeId typeId) {
        this.typeId = typeId;
    }




    /**
     * Return templateId.
     * @return templateId
     */
    public List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> getTemplateId() {
        if (templateId == null) {
            templateId = new ArrayList<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId>();
        }
        return templateId;
    }

    /**
     * Set a value to attribute templateId.
     * @param templateId.
     */
    public void setTemplateId(List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> templateId) {
        this.templateId = templateId;
    }



    /**
     * Add a templateId to the templateId collection.
     * @param templateId_elt Element to add.
     */
    public void addTemplateId(net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId_elt) {
        this.templateId.add(templateId_elt);
    }

    /**
     * Remove a templateId to the templateId collection.
     * @param templateId_elt Element to remove
     */
    public void removeTemplateId(net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId_elt) {
        this.templateId.remove(templateId_elt);
    }

    /**
     * Return quantity.
     * @return quantity
     */
    public net.ihe.gazelle.datatypes.RTO getQuantity() {
        return quantity;
    }

    /**
     * Set a value to attribute quantity.
     * @param quantity.
     */
    public void setQuantity(net.ihe.gazelle.datatypes.RTO quantity) {
        this.quantity = quantity;
    }




    /**
     * Return ingredient.
     * @return ingredient
     */
    public net.ihe.gazelle.pharm.COCTMT230100UV01Substance getIngredientSubstance() {
        return ingredientSubstance;
    }

    /**
     * Set a value to attribute ingredient.
     * @param ingredient.
     */
    public void setIngredientSubstance(net.ihe.gazelle.pharm.COCTMT230100UV01Substance ingredientSubstance) {
        this.ingredientSubstance = ingredientSubstance;
    }




    /**
     * Return classCode.
     * @return classCode
     */
    public RoleClassIngredientEntity getClassCode() {
        return classCode;
    }

    /**
     * Set a value to attribute classCode.
     * @param classCode.
     */
    public void setClassCode(RoleClassIngredientEntity classCode) {
        this.classCode = classCode;
    }




    /**
     * Return nullFlavor.
     * @return nullFlavor
     */
    public NullFlavor getNullFlavor() {
        return nullFlavor;
    }

    /**
     * Set a value to attribute nullFlavor.
     * @param nullFlavor.
     */
    public void setNullFlavor(NullFlavor nullFlavor) {
        this.nullFlavor = nullFlavor;
    }





    public Node get_xmlNodePresentation() {
        if (_xmlNodePresentation == null){
            JAXBContext jc;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder db = null;
            Document doc = null;
            try {
                db = dbf.newDocumentBuilder();
                doc = db.newDocument();
            } catch (ParserConfigurationException e1) {}
            try {
                jc = JAXBContext.newInstance("net.ihe.gazelle.pharm");
                Marshaller m = jc.createMarshaller();
                m.marshal(this, doc);
                _xmlNodePresentation = doc.getElementsByTagNameNS("urn:hl7-org:pharm", "COCT_MT230100UV01.Ingredient").item(0);
            } catch (JAXBException e) {
                try{
                    db = dbf.newDocumentBuilder();
                    _xmlNodePresentation = db.newDocument();
                }
                catch(Exception ee){}
            }
        }
        return _xmlNodePresentation;
    }

    public void set_xmlNodePresentation(Node _xmlNodePresentation) {
        this._xmlNodePresentation = _xmlNodePresentation;
    }





    /**
     * validate by a module of validation
     *
     */
    public static void validateByModule(net.ihe.gazelle.pharm.COCTMT230100UV01Ingredient cOCTMT230100UV01Ingredient, String location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
        if (cOCTMT230100UV01Ingredient != null){
            cvm.validate(cOCTMT230100UV01Ingredient, location, diagnostic);
            {
                int i = 0;
                for (net.ihe.gazelle.datatypes.CS realmCode: cOCTMT230100UV01Ingredient.getRealmCode()){
                    net.ihe.gazelle.datatypes.CS.validateByModule(realmCode, location + "/realmCode[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            net.ihe.gazelle.infr.AllInfrastructureRootTypeId.validateByModule(cOCTMT230100UV01Ingredient.getTypeId(), location + "/typeId", cvm, diagnostic);
            {
                int i = 0;
                for (net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId: cOCTMT230100UV01Ingredient.getTemplateId()){
                    net.ihe.gazelle.infr.AllInfrastructureRootTemplateId.validateByModule(templateId, location + "/templateId[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            net.ihe.gazelle.datatypes.RTO.validateByModule(cOCTMT230100UV01Ingredient.getQuantity(), location + "/quantity", cvm, diagnostic);
            net.ihe.gazelle.pharm.COCTMT230100UV01Substance.validateByModule(cOCTMT230100UV01Ingredient.getIngredientSubstance(), location + "/ingredientSubstance", cvm, diagnostic);
        }
    }

}
package net.ihe.gazelle.pharm;


import net.ihe.gazelle.datatypes.CS;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.voc.EntityClassManufacturedMaterial;
import net.ihe.gazelle.voc.EntityDeterminerDetermined;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.ArrayList;
import java.util.List;

/**
 * Description of the class COCTMT230100UV01Product.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "COCT_MT230100UV01.Product", propOrder = {
        "realmCode",
        "typeId",
        "templateId",
        "asContent",
        "asSpecializedKind",
        "part",
        "ingredient",
        "classCode",
        "determinerCode"
})
@XmlRootElement(name = "COCT_MT230100UV01.Product")
public class COCTMT230100UV01Product implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @XmlElement(name = "realmCode", namespace = "urn:hl7-org:pharm")
    public List<CS> realmCode;
    @XmlElement(name = "typeId", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.cdaepsos4.POCDMT000040InfrastructureRootTypeId typeId;
    @XmlElement(name = "templateId", namespace = "urn:hl7-org:pharm")
    public List<net.ihe.gazelle.datatypes.II> templateId;
    @XmlElement(name = "asContent", namespace = "urn:hl7-org:pharm")
    public List<net.ihe.gazelle.pharm.COCTMT230100UV01Content> asContent;
    @XmlElement(name = "asSpecializedKind", namespace = "urn:hl7-org:pharm")
    public List<net.ihe.gazelle.pharm.COCTMT230100UV01SpecializedKind> asSpecializedKind;
    @XmlElement(name = "part", namespace = "urn:hl7-org:pharm")
    public List<net.ihe.gazelle.pharm.COCTMT230100UV01Part> part;
    @XmlElement(name = "ingredient", namespace = "urn:hl7-org:pharm")
    public List<net.ihe.gazelle.pharm.COCTMT230100UV01Ingredient> ingredient;
    @XmlAttribute(name = "classCode", required = true)
    public EntityClassManufacturedMaterial classCode;
    @XmlAttribute(name = "determinerCode", required = true)
    public EntityDeterminerDetermined determinerCode;

    /**
     * An attribute containing marshalled element node
     */
    @XmlTransient
    private org.w3c.dom.Node _xmlNodePresentation;


    /**
     * Return realmCode.
     * @return realmCode
     */
    public List<net.ihe.gazelle.datatypes.CS> getRealmCode() {
        if (realmCode == null) {
            realmCode = new ArrayList<CS>();
        }
        return realmCode;
    }

    /**
     * Set a value to attribute realmCode.
     * @param realmCode.
     */
    public void setRealmCode(List<net.ihe.gazelle.datatypes.CS> realmCode) {
        this.realmCode = realmCode;
    }



    /**
     * Add a realmCode to the realmCode collection.
     * @param realmCode_elt Element to add.
     */
    public void addRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
        this.realmCode.add(realmCode_elt);
    }

    /**
     * Remove a realmCode to the realmCode collection.
     * @param realmCode_elt Element to remove
     */
    public void removeRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
        this.realmCode.remove(realmCode_elt);
    }

    /**
     * Return typeId.
     * @return typeId
     */
    public net.ihe.gazelle.cdaepsos4.POCDMT000040InfrastructureRootTypeId getTypeId() {
        return typeId;
    }

    /**
     * Set a value to attribute typeId.
     * @param typeId.
     */
    public void setTypeId(net.ihe.gazelle.cdaepsos4.POCDMT000040InfrastructureRootTypeId typeId) {
        this.typeId = typeId;
    }




    /**
     * Return templateId.
     * @return templateId
     */
    public List<net.ihe.gazelle.datatypes.II> getTemplateId() {
        if (templateId == null) {
            templateId = new ArrayList<net.ihe.gazelle.datatypes.II>();
        }
        return templateId;
    }

    /**
     * Set a value to attribute templateId.
     * @param templateId.
     */
    public void setTemplateId(List<net.ihe.gazelle.datatypes.II> templateId) {
        this.templateId = templateId;
    }



    /**
     * Add a templateId to the templateId collection.
     * @param templateId_elt Element to add.
     */
    public void addTemplateId(net.ihe.gazelle.datatypes.II templateId_elt) {
        this.templateId.add(templateId_elt);
    }

    /**
     * Remove a templateId to the templateId collection.
     * @param templateId_elt Element to remove
     */
    public void removeTemplateId(net.ihe.gazelle.datatypes.II templateId_elt) {
        this.templateId.remove(templateId_elt);
    }

    public List<net.ihe.gazelle.pharm.COCTMT230100UV01Content> getAsContent() {
        if(asContent == null) {
            asContent = new ArrayList<COCTMT230100UV01Content>();
        }
        return asContent;
    }

    /**
     * Set a value to attribute asContent.
     * @param asContent.
     */
    public void setAsContent(List<net.ihe.gazelle.pharm.COCTMT230100UV01Content> asContent) {
        this.asContent = asContent;
    }

    /**
     * Return asSpecializedKind.
     * @return asSpecializedKind
     */
    public List<net.ihe.gazelle.pharm.COCTMT230100UV01SpecializedKind> getAsSpecializedKind() {
        if (asSpecializedKind == null) {
            asSpecializedKind = new ArrayList<net.ihe.gazelle.pharm.COCTMT230100UV01SpecializedKind>();
        }
        return asSpecializedKind;
    }

    /**
     * Set a value to attribute asSpecializedKind.
     * @param asSpecializedKind.
     */
    public void setAsSpecializedKind(List<net.ihe.gazelle.pharm.COCTMT230100UV01SpecializedKind> asSpecializedKind) {
        this.asSpecializedKind = asSpecializedKind;
    }


    /**
     * Add a asSpecializedKind to the asSpecializedKind collection.
     * @param asSpecializedKind_elt Element to add.
     */
    public void addAsSpecializedKind(net.ihe.gazelle.pharm.COCTMT230100UV01SpecializedKind asSpecializedKind_elt) {
        this.getAsSpecializedKind().add(asSpecializedKind_elt);
    }

    /**
     * Remove a asSpecializedKind to the asSpecializedKind collection.
     * @param asSpecializedKind_elt Element to remove
     */
    public void removeAsSpecializedKind(net.ihe.gazelle.pharm.COCTMT230100UV01SpecializedKind asSpecializedKind_elt) {
        this.getAsSpecializedKind().remove(asSpecializedKind_elt);
    }

    public List<COCTMT230100UV01Part> getPart() {
        if(part == null) {
            part = new ArrayList<COCTMT230100UV01Part>();
        }
        return part;
    }

    public void setPart(List<COCTMT230100UV01Part> part) {
        this.part = part;
    }

    /**
     * Add a par to the part collection.
     * @param part_elt Element to add.
     */
    public void addPart(net.ihe.gazelle.pharm.COCTMT230100UV01Part part_elt) {
        this.getPart().add(part_elt);
    }

    /**
     * Remove a part to the part collection.
     * @param part_elt Element to remove
     */
    public void removePart(net.ihe.gazelle.pharm.COCTMT230100UV01Part part_elt) {
        this.getPart().remove(part_elt);
    }

    public List<COCTMT230100UV01Ingredient> getIngredient() {
        if(ingredient == null) {
            ingredient = new ArrayList<COCTMT230100UV01Ingredient>();
        }
        return ingredient;
    }

    public void setIngredient(List<COCTMT230100UV01Ingredient> ingredient) {
        this.ingredient = ingredient;
    }

    /**
     * Add a ingredient to the ingredient collection.
     * @param ingredient_elt Element to add.
     */
    public void addIngredient(net.ihe.gazelle.pharm.COCTMT230100UV01Ingredient ingredient_elt) {
        this.getIngredient().add(ingredient_elt);
    }

    /**
     * Remove a ingredient to the ingredient collection.
     * @param ingredient_elt Element to remove
     */

    public void removeIngredient(net.ihe.gazelle.pharm.COCTMT230100UV01Ingredient ingredient_elt) {
        this.getIngredient().remove(ingredient_elt);
    }

    /**
     * Return classCode.
     * @return classCode
     */
    public EntityClassManufacturedMaterial getClassCode() {
        return classCode;
    }

    /**
     * Set a value to attribute classCode.
     * @param classCode.
     */
    public void setClassCode(EntityClassManufacturedMaterial classCode) {
        this.classCode = classCode;
    }


    /**
     * Return determinerCode.
     * @return determinerCode
     */
    public EntityDeterminerDetermined getDeterminerCode() {
        return determinerCode;
    }

    /**
     * Set a value to attribute determinerCode.
     * @param determinerCode.
     */
    public void setDeterminerCode(EntityDeterminerDetermined determinerCode) {
        this.determinerCode = determinerCode;
    }



    public Node get_xmlNodePresentation() {
        if (_xmlNodePresentation == null){
            JAXBContext jc;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder db = null;
            Document doc = null;
            try {
                db = dbf.newDocumentBuilder();
                doc = db.newDocument();
            } catch (ParserConfigurationException e1) {}
            try {
                jc = JAXBContext.newInstance("net.ihe.gazelle.pharm");
                Marshaller m = jc.createMarshaller();
                m.marshal(this, doc);
                _xmlNodePresentation = doc.getElementsByTagNameNS("urn:hl7-org:pharm", "COCT_MT230100UV01.Product").item(0);
            } catch (JAXBException e) {
                try{
                    db = dbf.newDocumentBuilder();
                    _xmlNodePresentation = db.newDocument();
                }
                catch(Exception ee){}
            }
        }
        return _xmlNodePresentation;
    }

    public void set_xmlNodePresentation(Node _xmlNodePresentation) {
        this._xmlNodePresentation = _xmlNodePresentation;
    }





    /**
     * validate by a module of validation
     *
     */
    public static void validateByModule(net.ihe.gazelle.pharm.COCTMT230100UV01Product cOCTMT230100UV01Product, String location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
        if (cOCTMT230100UV01Product != null){
            cvm.validate(cOCTMT230100UV01Product, location, diagnostic);
            {
                int i = 0;
                for (net.ihe.gazelle.datatypes.CS realmCode: cOCTMT230100UV01Product.getRealmCode()){
                    net.ihe.gazelle.datatypes.CS.validateByModule(realmCode, location + "/realmCode[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            net.ihe.gazelle.cdaepsos4.POCDMT000040InfrastructureRootTypeId.validateByModule(cOCTMT230100UV01Product.getTypeId(), location + "/typeId", cvm, diagnostic);
            {
                int i = 0;
                for (net.ihe.gazelle.datatypes.II templateId: cOCTMT230100UV01Product.getTemplateId()){
                    net.ihe.gazelle.datatypes.II.validateByModule(templateId, location + "/templateId[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            {
                int i = 0;
                for (net.ihe.gazelle.pharm.COCTMT230100UV01Content asContent: cOCTMT230100UV01Product.getAsContent()){
                    net.ihe.gazelle.pharm.COCTMT230100UV01Content.validateByModule(asContent, location + "/asContent[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            {
                int i = 0;
                for (net.ihe.gazelle.pharm.COCTMT230100UV01SpecializedKind asSpecializedKind: cOCTMT230100UV01Product.getAsSpecializedKind()){
                    net.ihe.gazelle.pharm.COCTMT230100UV01SpecializedKind.validateByModule(asSpecializedKind, location + "/asSpecializedKind[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            {
                int i = 0;
                for (net.ihe.gazelle.pharm.COCTMT230100UV01Part part: cOCTMT230100UV01Product.getPart()){
                    net.ihe.gazelle.pharm.COCTMT230100UV01Part.validateByModule(part, location + "/part[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            {
                int i = 0;
                for (net.ihe.gazelle.pharm.COCTMT230100UV01Ingredient ingredient: cOCTMT230100UV01Product.getIngredient()){
                    net.ihe.gazelle.pharm.COCTMT230100UV01Ingredient.validateByModule(ingredient, location + "/ingredient[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }


        }
    }

}
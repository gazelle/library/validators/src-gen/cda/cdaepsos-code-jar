package net.ihe.gazelle.pharm;

import net.ihe.gazelle.datatypes.CD;
import net.ihe.gazelle.datatypes.CS;
import net.ihe.gazelle.datatypes.ED;
import net.ihe.gazelle.datatypes.PQ;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.voc.EntityClassContainer;
import net.ihe.gazelle.voc.EntityDeterminer;
import net.ihe.gazelle.voc.NullFlavor;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "COCT_MT230100UV01.PackagedProduct", propOrder = {
        "realmCode",
        "typeId",
        "templateId",
        "code",
        "name",
        "desc",
        "formCode",
        "capacityQuantity",
        "capTypeCode",
        "asContent",
        "classCode",
        "determinerCode",
        "nullFlavor"
})
@XmlRootElement(name = "COCT_MT230100UV01.PackagedProduct")
public class COCTMT230100UV01PackagedProduct implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @XmlElement(name = "realmCode", namespace = "urn:hl7-org:pharm")
    public List<CS> realmCode;
    @XmlElement(name = "typeId", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.infr.AllInfrastructureRootTypeId typeId;
    @XmlElement(name = "templateId", namespace = "urn:hl7-org:pharm")
    public List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> templateId;
    @XmlElement(name = "code", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.datatypes.CD code;
    @XmlElement(name = "name", namespace = "urn:hl7-org:pharm")
    public List<net.ihe.gazelle.datatypes.EN> name;
    @XmlElement(name = "desc", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.datatypes.ED desc;
    @XmlElement(name = "formCode", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.datatypes.CD formCode;
    @XmlElement(name = "capacityQuantity", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.datatypes.PQ capacityQuantity;
    @XmlElement(name = "capTypeCode", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.datatypes.CD capTypeCode;
    @XmlElement(name = "asContent", namespace = "urn:hl7-org:pharm")
    public List<COCTMT230100UV01Content1> asContent;
    @XmlAttribute(name = "classCode", required = true)
    public EntityClassContainer classCode;
    @XmlAttribute(name = "determinerCode", required = true)
    public EntityDeterminer determinerCode;
    @XmlAttribute(name = "nullFlavor")
    public NullFlavor nullFlavor;

    /**
     * An attribute containing marshalled element node
     */
    @XmlTransient
    private org.w3c.dom.Node _xmlNodePresentation;


    /**
     * Return realmCode.
     * @return realmCode
     */
    public List<net.ihe.gazelle.datatypes.CS> getRealmCode() {
        if (realmCode == null) {
            realmCode = new ArrayList<CS>();
        }
        return realmCode;
    }

    /**
     * Set a value to attribute realmCode.
     * @param realmCode.
     */
    public void setRealmCode(List<net.ihe.gazelle.datatypes.CS> realmCode) {
        this.realmCode = realmCode;
    }



    /**
     * Add a realmCode to the realmCode collection.
     * @param realmCode_elt Element to add.
     */
    public void addRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
        this.getRealmCode().add(realmCode_elt);
    }

    /**
     * Remove a realmCode to the realmCode collection.
     * @param realmCode_elt Element to remove
     */
    public void removeRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
        this.getRealmCode().remove(realmCode_elt);
    }

    /**
     * Return typeId.
     * @return typeId
     */
    public net.ihe.gazelle.infr.AllInfrastructureRootTypeId getTypeId() {
        return typeId;
    }

    /**
     * Set a value to attribute typeId.
     * @param typeId.
     */
    public void setTypeId(net.ihe.gazelle.infr.AllInfrastructureRootTypeId typeId) {
        this.typeId = typeId;
    }




    /**
     * Return templateId.
     * @return templateId
     */
    public List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> getTemplateId() {
        if (templateId == null) {
            templateId = new ArrayList<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId>();
        }
        return templateId;
    }

    /**
     * Set a value to attribute templateId.
     * @param templateId.
     */
    public void setTemplateId(List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> templateId) {
        this.templateId = templateId;
    }



    /**
     * Add a templateId to the templateId collection.
     * @param templateId_elt Element to add.
     */
    public void addTemplateId(net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId_elt) {
        this.getTemplateId().add(templateId_elt);
    }

    /**
     * Remove a templateId to the templateId collection.
     * @param templateId_elt Element to remove
     */
    public void removeTemplateId(net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId_elt) {
        this.getTemplateId().remove(templateId_elt);
    }


    /**
     * Return code.
     * @return code
     */
    public net.ihe.gazelle.datatypes.CD getCode() {
        return code;
    }

    /**
     * Set a value to attribute code.
     * @param code.
     */

    public void setCode(net.ihe.gazelle.datatypes.CD code) {
        this.code = code;
    }


    public ED getDesc() {
        return desc;
    }

    public COCTMT230100UV01PackagedProduct setDesc(ED desc) {
        this.desc = desc;
        return this;
    }

    /**
     * Return name.
     * @return name
     */

    public List<net.ihe.gazelle.datatypes.EN> getName() {
        if (name == null) {
            name = new ArrayList<net.ihe.gazelle.datatypes.EN>();
        }
        return name;
    }

    /**
     * Set a value to attribute name.
     * @param name.
     */

    public void setName(List<net.ihe.gazelle.datatypes.EN> name) {
        this.name = name;
    }



    /**
     * Add a name to the name collection.
     * @param name_elt Element to add.
     */

    public void addName(net.ihe.gazelle.datatypes.EN name_elt) {
        this.getName().add(name_elt);
    }

    /**
     * Remove a name to the name collection.
     * @param name_elt Element to remove
     */

    public void removeName(net.ihe.gazelle.datatypes.EN name_elt) {
        this.getName().remove(name_elt);
    }

    /**
     * Return formCode.
     * @return formCode
     */
    public net.ihe.gazelle.datatypes.CD getFormCode() {
        return formCode;
    }

    /**
     * Set a value to attribute formCode.
     * @param formCode.
     */

    public void setFormCode(net.ihe.gazelle.datatypes.CD formCode) {
        this.formCode = formCode;
    }


    /**
     * Return capTypeCode.
     * @return capTypeCode
     */
    public PQ getCapacityQuantity() {
        return capacityQuantity;
    }

    /**
     * Set a value to attribute capTypeCode.
     * @param capTypeCode.
     */
    public COCTMT230100UV01PackagedProduct setCapacityQuantity(PQ capacityQuantity) {
        this.capacityQuantity = capacityQuantity;
        return this;
    }

    /**
     * Return capTypeCode.
     * @return capTypeCode
     */
    public CD getCapTypeCode() {
        return capTypeCode;
    }

    /**
     * Set a value to attribute capTypeCode.
     * @param capTypeCode.
     */

    public COCTMT230100UV01PackagedProduct setCapTypeCode(CD capTypeCode) {
        this.capTypeCode = capTypeCode;
        return this;
    }

    /**
     * Return asContent.
     * @return asContent
     */
    public List<COCTMT230100UV01Content1> getAsContent() {
        if (asContent == null) {
            asContent = new ArrayList<COCTMT230100UV01Content1>();
        }
        return asContent;
    }

    /**
     * Set a value to attribute asContent.
     * @param asContent.
     */
    public void setAsContent(List<COCTMT230100UV01Content1> asContent) {
        this.asContent = asContent;
    }

    /**
     * Add a asContent to the asContent collection.
     * @param asContent_elt Element to add.
     */
    public void addAsContent(COCTMT230100UV01Content1 asContent_elt) {
        this.getAsContent().add(asContent_elt);
    }

    /**
     * Remove a asContent to the asContent collection.
     * @param asContent_elt Element to remove
     */

    public void removeAsContent(COCTMT230100UV01Content1 asContent_elt) {
        this.getAsContent().remove(asContent_elt);
    }



    /**
     * Return classCode.
     * @return classCode
     */
    public EntityClassContainer getClassCode() {
        return classCode;
    }

    /**
     * Set a value to attribute classCode.
     * @param classCode.
     */
    public void setClassCode(EntityClassContainer classCode) {
        this.classCode = classCode;
    }




    /**
     * Return determinerCode.
     * @return determinerCode
     */
    public EntityDeterminer getDeterminerCode() {
        return determinerCode;
    }

    /**
     * Set a value to attribute determinerCode.
     * @param determinerCode.
     */
    public void setDeterminerCode(EntityDeterminer determinerCode) {
        this.determinerCode = determinerCode;
    }




    /**
     * Return nullFlavor.
     * @return nullFlavor
     */
    public NullFlavor getNullFlavor() {
        return nullFlavor;
    }

    /**
     * Set a value to attribute nullFlavor.
     * @param nullFlavor.
     */
    public void setNullFlavor(NullFlavor nullFlavor) {
        this.nullFlavor = nullFlavor;
    }





    public Node get_xmlNodePresentation() {
        if (_xmlNodePresentation == null){
            JAXBContext jc;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder db = null;
            Document doc = null;
            try {
                db = dbf.newDocumentBuilder();
                doc = db.newDocument();
            } catch (ParserConfigurationException e1) {}
            try {
                jc = JAXBContext.newInstance("net.ihe.gazelle.pharm");
                Marshaller m = jc.createMarshaller();
                m.marshal(this, doc);
                _xmlNodePresentation = doc.getElementsByTagNameNS("urn:hl7-org:pharm", "COCT_MT230100UV01.PackagedProduct").item(0);
            } catch (JAXBException e) {
                try{
                    db = dbf.newDocumentBuilder();
                    _xmlNodePresentation = db.newDocument();
                }
                catch(Exception ee){}
            }
        }
        return _xmlNodePresentation;
    }

    public void set_xmlNodePresentation(Node _xmlNodePresentation) {
        this._xmlNodePresentation = _xmlNodePresentation;
    }





    /**
     * validate by a module of validation
     *
     */
    public static void validateByModule(COCTMT230100UV01PackagedProduct cOCTMT230100UV01PackagedProduct, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
        if (cOCTMT230100UV01PackagedProduct != null){
            cvm.validate(cOCTMT230100UV01PackagedProduct, _location, diagnostic);
            {
                int i = 0;
                for (net.ihe.gazelle.datatypes.CS realmCode: cOCTMT230100UV01PackagedProduct.getRealmCode()){
                    net.ihe.gazelle.datatypes.CS.validateByModule(realmCode, _location + "/realmCode[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            net.ihe.gazelle.infr.AllInfrastructureRootTypeId.validateByModule(cOCTMT230100UV01PackagedProduct.getTypeId(), _location + "/typeId", cvm, diagnostic);
            {
                int i = 0;
                for (net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId: cOCTMT230100UV01PackagedProduct.getTemplateId()){
                    net.ihe.gazelle.infr.AllInfrastructureRootTemplateId.validateByModule(templateId, _location + "/templateId[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            net.ihe.gazelle.datatypes.CD.validateByModule(cOCTMT230100UV01PackagedProduct.getCode(), _location + "/code", cvm, diagnostic);
            {
                int i = 0;
                for (net.ihe.gazelle.datatypes.EN name: cOCTMT230100UV01PackagedProduct.getName()){
                    net.ihe.gazelle.datatypes.EN.validateByModule(name, _location + "/name[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }
            {
                int i = 0;
                for (COCTMT230100UV01Content1 asContent: cOCTMT230100UV01PackagedProduct.getAsContent()){
                    COCTMT230100UV01Content1.validateByModule(asContent, _location + "/asContent[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            net.ihe.gazelle.datatypes.CD.validateByModule(cOCTMT230100UV01PackagedProduct.getCapTypeCode(), _location + "/capTypeCode", cvm, diagnostic);
            net.ihe.gazelle.datatypes.ED.validateByModule(cOCTMT230100UV01PackagedProduct.getDesc(), _location + "/desc", cvm, diagnostic);
            net.ihe.gazelle.datatypes.CD.validateByModule(cOCTMT230100UV01PackagedProduct.getFormCode(), _location + "/formCode", cvm, diagnostic);
            net.ihe.gazelle.datatypes.CD.validateByModule(cOCTMT230100UV01PackagedProduct.getCapacityQuantity(), _location + "/capacityQuantity", cvm, diagnostic);
        }
    }

}

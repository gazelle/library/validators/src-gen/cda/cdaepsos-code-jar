package net.ihe.gazelle.pharm;


import net.ihe.gazelle.datatypes.CD;
import net.ihe.gazelle.datatypes.CS;
import net.ihe.gazelle.datatypes.ED;
import net.ihe.gazelle.datatypes.EN;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.voc.EntityClassManufacturedMaterial;
import net.ihe.gazelle.voc.EntityDeterminerDetermined;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.ArrayList;
import java.util.List;

/**
 * Description of the class COCTMT230100UV01Substance.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "COCT_MT230100UV01.Substance", propOrder = {
        "realmCode",
        "typeId",
        "templateId",
        "code",
        "name",
        "desc",
        "classCode",
        "determinerCode"
})
@XmlRootElement(name = "COCT_MT230100UV01.Substance")
public class COCTMT230100UV01Substance implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @XmlElement(name = "realmCode", namespace = "urn:hl7-org:pharm")
    public List<CS> realmCode;
    @XmlElement(name = "typeId", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.infr.AllInfrastructureRootTypeId typeId;
    @XmlElement(name = "templateId", namespace = "urn:hl7-org:pharm")
    public List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> templateId;
    @XmlElement(name = "code", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.datatypes.CD code;
    @XmlElement(name = "name", namespace = "urn:hl7-org:pharm")
    public List<net.ihe.gazelle.datatypes.EN> name;
    @XmlElement(name = "desc", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.datatypes.ED desc;
    @XmlAttribute(name = "classCode", required = true)
    public EntityClassManufacturedMaterial classCode;
    @XmlAttribute(name = "determinerCode", required = true)
    public EntityDeterminerDetermined determinerCode;

    /**
     * An attribute containing marshalled element node
     */
    @XmlTransient
    private org.w3c.dom.Node _xmlNodePresentation;


    /**
     * Return realmCode.
     * @return realmCode
     */
    public List<net.ihe.gazelle.datatypes.CS> getRealmCode() {
        if (realmCode == null) {
            realmCode = new ArrayList<CS>();
        }
        return realmCode;
    }

    /**
     * Set a value to attribute realmCode.
     * @param realmCode.
     */
    public void setRealmCode(List<net.ihe.gazelle.datatypes.CS> realmCode) {
        this.realmCode = realmCode;
    }



    /**
     * Add a realmCode to the realmCode collection.
     * @param realmCode_elt Element to add.
     */
    public void addRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
        this.realmCode.add(realmCode_elt);
    }

    /**
     * Remove a realmCode to the realmCode collection.
     * @param realmCode_elt Element to remove
     */
    public void removeRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
        this.realmCode.remove(realmCode_elt);
    }

    /**
     * Return typeId.
     * @return typeId
     */
    public net.ihe.gazelle.infr.AllInfrastructureRootTypeId getTypeId() {
        return typeId;
    }

    /**
     * Set a value to attribute typeId.
     * @param typeId.
     */
    public void setTypeId(net.ihe.gazelle.infr.AllInfrastructureRootTypeId typeId) {
        this.typeId = typeId;
    }




    /**
     * Return templateId.
     * @return templateId
     */
    public List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> getTemplateId() {
        if (templateId == null) {
            templateId = new ArrayList<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId>();
        }
        return templateId;
    }

    /**
     * Set a value to attribute templateId.
     * @param templateId.
     */
    public void setTemplateId(List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> templateId) {
        this.templateId = templateId;
    }



    /**
     * Add a templateId to the templateId collection.
     * @param templateId_elt Element to add.
     */
    public void addTemplateId(net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId_elt) {
        this.templateId.add(templateId_elt);
    }

    /**
     * Remove a templateId to the templateId collection.
     * @param templateId_elt Element to remove
     */
    public void removeTemplateId(net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId_elt) {
        this.templateId.remove(templateId_elt);
    }

    /**
     * Return code.
     * @return code
     */
    public CD getCode() {
        return code;
    }

    /**
     * Set a value to attribute code.
     * @param code.
     */
    public void setCode(CD code) {
        this.code = code;
    }




    /**
     * Return name.
     * @return name
     */
    public List<net.ihe.gazelle.datatypes.EN> getName() {
        if (name == null) {
            name = new ArrayList<net.ihe.gazelle.datatypes.EN>();
        }
        return name;
    }

    /**
     * Set a value to attribute name.
     * @param name.
     */
    public void setName(List<EN> name) {
        this.name = name;
    }



    /**
     * Add a name to the name collection.
     * @param name_elt Element to add.
     */
    public void addName(net.ihe.gazelle.datatypes.EN name_elt) {
        this.name.add(name_elt);
    }

    /**
     * Remove a name to the name collection.
     * @param name_elt Element to remove
     */
    public void removeName(net.ihe.gazelle.datatypes.EN name_elt) {
        this.name.remove(name_elt);
    }

    /**
     * Return desc.
     * @return desc
     */
    public ED getDesc() {
        return desc;
    }

    /**
     * Set a value to attribute desc.
     * @param desc.
     */
    public void setDesc(net.ihe.gazelle.datatypes.ED desc) {
        this.desc = desc;
    }


    /**
     * Return classCode.
     * @return classCode
     */
    public EntityClassManufacturedMaterial getClassCode() {
        return classCode;
    }

    /**
     * Set a value to attribute classCode.
     * @param classCode.
     */
    public void setClassCode(EntityClassManufacturedMaterial classCode) {
        this.classCode = classCode;
    }

    /**
     * Return determinerCode.
     * @return determinerCode
     */
    public EntityDeterminerDetermined getDeterminerCode() {
        return determinerCode;
    }

    /**
     * Set a value to attribute determinerCode.
     * @param determinerCode.
     */
    public void setDeterminerCode(EntityDeterminerDetermined determinerCode) {
        this.determinerCode = determinerCode;
    }




    public Node get_xmlNodePresentation() {
        if (_xmlNodePresentation == null){
            JAXBContext jc;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder db = null;
            Document doc = null;
            try {
                db = dbf.newDocumentBuilder();
                doc = db.newDocument();
            } catch (ParserConfigurationException e1) {}
            try {
                jc = JAXBContext.newInstance("net.ihe.gazelle.medication");
                Marshaller m = jc.createMarshaller();
                m.marshal(this, doc);
                _xmlNodePresentation = doc.getElementsByTagNameNS("urn:epsos-org:ep:medication", "COCT_MT230100UV.Substance").item(0);
            } catch (JAXBException e) {
                try{
                    db = dbf.newDocumentBuilder();
                    _xmlNodePresentation = db.newDocument();
                }
                catch(Exception ee){}
            }
        }
        return _xmlNodePresentation;
    }

    public void set_xmlNodePresentation(Node _xmlNodePresentation) {
        this._xmlNodePresentation = _xmlNodePresentation;
    }





    /**
     * validate by a module of validation
     *
     */
    public static void validateByModule(net.ihe.gazelle.pharm.COCTMT230100UV01Substance cOCTMT230100UV01Substance, String location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
        if (cOCTMT230100UV01Substance != null){
            cvm.validate(cOCTMT230100UV01Substance, location, diagnostic);
            {
                int i = 0;
                for (net.ihe.gazelle.datatypes.CS realmCode: cOCTMT230100UV01Substance.getRealmCode()){
                    net.ihe.gazelle.datatypes.CS.validateByModule(realmCode, location + "/realmCode[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            net.ihe.gazelle.infr.AllInfrastructureRootTypeId.validateByModule(cOCTMT230100UV01Substance.getTypeId(), location + "/typeId", cvm, diagnostic);
            {
                int i = 0;
                for (net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId: cOCTMT230100UV01Substance.getTemplateId()){
                    net.ihe.gazelle.infr.AllInfrastructureRootTemplateId.validateByModule(templateId, location + "/templateId[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            net.ihe.gazelle.datatypes.CD.validateByModule(cOCTMT230100UV01Substance.getCode(), location + "/code", cvm, diagnostic);
            {
                int i = 0;
                for (net.ihe.gazelle.datatypes.EN name: cOCTMT230100UV01Substance.getName()){
                    net.ihe.gazelle.datatypes.EN.validateByModule(name, location + "/name[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            net.ihe.gazelle.datatypes.ED.validateByModule(cOCTMT230100UV01Substance.getDesc(), location + "/desc", cvm, diagnostic);


        }
    }

}

package net.ihe.gazelle.pharm;



import net.ihe.gazelle.datatypes.CS;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.infr.AllInfrastructureRootTemplateId;
import net.ihe.gazelle.voc.EntityClassManufacturedMaterial;
import net.ihe.gazelle.voc.EntityDeterminerDetermined;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Description of the class COCT_MT230100UV01.ActiveMoietyEntity.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "COCT_MT230100UV01.ActiveMoietyEntity", propOrder = {
        "realmCode",
        "typeId",
        "templateId",
        "asSpecializedKind",
        "classCode",
        "determinerCode",
})
@XmlRootElement(name = "COCT_MT230100UV01.ActiveMoietyEntity")
public class COCTMT230100UV01ActiveMoietyEntity implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @XmlElement(name = "realmCode", namespace = "urn:hl7-org:pharm")
    public List<CS> realmCode;
    @XmlElement(name = "typeId", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.infr.AllInfrastructureRootTypeId typeId;
    @XmlElement(name = "templateId", namespace = "urn:hl7-org:pharm")
    public List<AllInfrastructureRootTemplateId> templateId;
    @XmlElement(name = "asSpecializedKind", namespace = "urn:hl7-org:pharm")
    public List<net.ihe.gazelle.pharm.COCTMT230100UV01SpecializedKind> asSpecializedKind;
    @XmlAttribute(name = "classCode", required = true)
    public EntityClassManufacturedMaterial classCode;
    @XmlAttribute(name = "determinerCode")
    public EntityDeterminerDetermined determinerCode;

    /**
     * An attribute containing marshalled element node
     */
    @XmlTransient
    private org.w3c.dom.Node _xmlNodePresentation;


    /**
     * Return realmCode.
     *
     * @return realmCode
     */
    public List<net.ihe.gazelle.datatypes.CS> getRealmCode() {
        if (realmCode == null) {
            realmCode = new ArrayList<CS>();
        }
        return realmCode;
    }

    /**
     * Return typeId.
     *
     * @return typeId
     */

    public net.ihe.gazelle.infr.AllInfrastructureRootTypeId getTypeId() {
        return typeId;
    }

    /**
     * Set a value to attribute typeId.
     *
     * @param typeId.
     */

    public void setTypeId(net.ihe.gazelle.infr.AllInfrastructureRootTypeId typeId) {
        this.typeId = typeId;
    }

    /**
     * Return templateId.
     *
     * @return templateId
     */

    public List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> getTemplateId() {
        if (templateId == null) {
            templateId = new ArrayList<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId>();
        }
        return templateId;
    }

    /**
     * Return asSpecializedKind.
     *
     * @return asSpecializedKind
     */

    public List<net.ihe.gazelle.pharm.COCTMT230100UV01SpecializedKind> getAsSpecializedKind() {
        if (asSpecializedKind == null) {
            asSpecializedKind = new ArrayList<net.ihe.gazelle.pharm.COCTMT230100UV01SpecializedKind>();
        }
        return asSpecializedKind;
    }

    /**
     * Set a value to attribute asSpecializedKind.
     *
     * @param asSpecializedKind.
     */

    public void setAsSpecializedKind(List<net.ihe.gazelle.pharm.COCTMT230100UV01SpecializedKind> asSpecializedKind) {
        this.asSpecializedKind = asSpecializedKind;
    }

    /**
     * Return classCode.
     *
     * @return classCode
     */

    public EntityClassManufacturedMaterial getClassCode() {
        return classCode;
    }

    /**
     * Set a value to attribute classCode.
     *
     * @param classCode.
     */

    public void setClassCode(EntityClassManufacturedMaterial classCode) {
        this.classCode = classCode;
    }

    /**
     * Return determinerCode.
     *
     * @return determinerCode
     */

    public EntityDeterminerDetermined getDeterminerCode() {
        return determinerCode;
    }

    /**
     * Set a value to attribute determinerCode.
     * @param determinerCode.
     */

    public void setDeterminerCode(EntityDeterminerDetermined determinerCode) {
        this.determinerCode = determinerCode;
    }


    public Node get_xmlNodePresentation() {
        if (_xmlNodePresentation == null){
            JAXBContext jc;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder db = null;
            Document doc = null;
            try {
                db = dbf.newDocumentBuilder();
                doc = db.newDocument();
            } catch (ParserConfigurationException e1) {}
            try {
                jc = JAXBContext.newInstance("net.ihe.gazelle.medication2");
                Marshaller m = jc.createMarshaller();
                m.marshal(this, doc);
                _xmlNodePresentation = doc.getElementsByTagNameNS("urn:epsos-org:ep:medication", "COCT_MT230100UV.SpecializedKind").item(0);
            } catch (JAXBException e) {
                try{
                    db = dbf.newDocumentBuilder();
                    _xmlNodePresentation = db.newDocument();
                }
                catch(Exception ee){}
            }
        }
        return _xmlNodePresentation;
    }


    /**
     * validate by a module of validation
     *
     */
    public static void validateByModule(net.ihe.gazelle.pharm.COCTMT230100UV01ActiveMoietyEntity cOCTMT230100UV01ActiveMoietyEntity, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
        if (cOCTMT230100UV01ActiveMoietyEntity != null){
            cvm.validate(cOCTMT230100UV01ActiveMoietyEntity, _location, diagnostic);
            {
                int i = 0;
                for (net.ihe.gazelle.datatypes.CS realmCode: cOCTMT230100UV01ActiveMoietyEntity.getRealmCode()){
                    net.ihe.gazelle.datatypes.CS.validateByModule(realmCode, _location + "/realmCode[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            net.ihe.gazelle.infr.AllInfrastructureRootTypeId.validateByModule(cOCTMT230100UV01ActiveMoietyEntity.getTypeId(), _location + "/typeId", cvm, diagnostic);
            {
                int i = 0;
                for (net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId: cOCTMT230100UV01ActiveMoietyEntity.getTemplateId()){
                    net.ihe.gazelle.infr.AllInfrastructureRootTemplateId.validateByModule(templateId, _location + "/templateId[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            {
                int i = 0;
                for (net.ihe.gazelle.pharm.COCTMT230100UV01SpecializedKind asSpecializedKind: cOCTMT230100UV01ActiveMoietyEntity.getAsSpecializedKind()){
                    net.ihe.gazelle.pharm.COCTMT230100UV01SpecializedKind.validateByModule(asSpecializedKind, _location + "/asSpecializedKind[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }
        }
    }


}

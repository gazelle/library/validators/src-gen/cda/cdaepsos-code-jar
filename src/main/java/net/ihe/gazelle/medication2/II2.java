/**
 * II2.java
 *
 * File generated from the medication2::II2 uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import java.util.List;

// End of user code
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


/**
 * Description of the class II2.
 *
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "II", propOrder = {
	"assigningAuthorityName",
	"displayable",
	"extension",
	"root"
})
@XmlRootElement(name = "II2")
public class II2 extends net.ihe.gazelle.medication2.ANY2 implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	/**
	 *                       A human readable name or mnemonic
	                            for the assigning                      authority. This name may be
	                            provided solely for the                      convenience of unaided
	                            humans interpreting an II value                      and can have no
	                            computational meaning. Note: no                      automated
	                            processing must depend on the assigning                      authority
	                            name to be present in any form.                   .
	 */
	@XmlAttribute(name = "assigningAuthorityName")
	public java.lang.String assigningAuthorityName;
	/**
	 *                       Specifies if the identifier is
	                            intended for human                      display and data entry
	                            (displayable = true) as                      opposed to pure machine
	                            interoperation (displayable                      = false).
	                        .
	 */
	@XmlAttribute(name = "displayable")
	public java.lang.Boolean displayable;
	/**
	 *                       A character string as a unique
	                            identifier within the                      scope of the identifier root.
	                        .
	 */
	@XmlAttribute(name = "extension")
	public java.lang.String extension;
	/**
	 *                       A unique identifier that guarantees
	                            the global uniqueness                      of the instance identifier.
	                            The root alone may be the                      entire instance
	                            identifier.                   .
	 */
	@XmlAttribute(name = "root")
	public java.lang.String root;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private org.w3c.dom.Node _xmlNodePresentation;
	
	
	/**
	 * Return assigningAuthorityName.
	 * @return assigningAuthorityName :                       A human readable name or mnemonic
	                            for the assigning                      authority. This name may be
	                            provided solely for the                      convenience of unaided
	                            humans interpreting an II value                      and can have no
	                            computational meaning. Note: no                      automated
	                            processing must depend on the assigning                      authority
	                            name to be present in any form.                   
	 */
	public java.lang.String getAssigningAuthorityName() {
	    return assigningAuthorityName;
	}
	
	/**
	 * Set a value to attribute assigningAuthorityName.
	 * @param assigningAuthorityName :                       A human readable name or mnemonic
	                            for the assigning                      authority. This name may be
	                            provided solely for the                      convenience of unaided
	                            humans interpreting an II value                      and can have no
	                            computational meaning. Note: no                      automated
	                            processing must depend on the assigning                      authority
	                            name to be present in any form.                   .
	 */
	public void setAssigningAuthorityName(java.lang.String assigningAuthorityName) {
	    this.assigningAuthorityName = assigningAuthorityName;
	}
	
	
	
	
	/**
	 * Return displayable.
	 * @return displayable :                       Specifies if the identifier is
	                            intended for human                      display and data entry
	                            (displayable = true) as                      opposed to pure machine
	                            interoperation (displayable                      = false).
	                        
	 */
	public java.lang.Boolean getDisplayable() {
	    return displayable;
	}
	
	/**
	 * Set a value to attribute displayable.
	 * @param displayable :                       Specifies if the identifier is
	                            intended for human                      display and data entry
	                            (displayable = true) as                      opposed to pure machine
	                            interoperation (displayable                      = false).
	                        .
	 */
	public void setDisplayable(java.lang.Boolean displayable) {
	    this.displayable = displayable;
	}
	
	
	
	
	/**
	 * Return extension.
	 * @return extension :                       A character string as a unique
	                            identifier within the                      scope of the identifier root.
	                        
	 */
	public java.lang.String getExtension() {
	    return extension;
	}
	
	/**
	 * Set a value to attribute extension.
	 * @param extension :                       A character string as a unique
	                            identifier within the                      scope of the identifier root.
	                        .
	 */
	public void setExtension(java.lang.String extension) {
	    this.extension = extension;
	}
	
	
	
	
	/**
	 * Return root.
	 * @return root :                       A unique identifier that guarantees
	                            the global uniqueness                      of the instance identifier.
	                            The root alone may be the                      entire instance
	                            identifier.                   
	 */
	public java.lang.String getRoot() {
	    return root;
	}
	
	/**
	 * Set a value to attribute root.
	 * @param root :                       A unique identifier that guarantees
	                            the global uniqueness                      of the instance identifier.
	                            The root alone may be the                      entire instance
	                            identifier.                   .
	 */
	public void setRoot(java.lang.String root) {
	    this.root = root;
	}
	
	
	
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.medication2");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:epsos-org:ep:medication", "II2").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(II2 iI2, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (iI2 != null){
   			net.ihe.gazelle.medication2.ANY2.validateByModule(iI2, _location, cvm, diagnostic);
    	}
    }

}

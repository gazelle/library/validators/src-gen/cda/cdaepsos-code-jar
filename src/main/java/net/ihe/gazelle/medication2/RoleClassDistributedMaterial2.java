/**
 * RoleClassDistributedMaterial2.java
 *
 * File generated from the medication2::RoleClassDistributedMaterial2 uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration RoleClassDistributedMaterial2.
 *
 * 
 */

@XmlType(name = "RoleClassDistributedMaterial")
@XmlEnum
@XmlRootElement(name = "RoleClassDistributedMaterial2")
public enum RoleClassDistributedMaterial2 {
	@XmlEnumValue("DST")
	DST("DST"),
	@XmlEnumValue("RET")
	RET("RET");
	
	private final String value;

    RoleClassDistributedMaterial2(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static RoleClassDistributedMaterial2 fromValue(String v) {
        for (RoleClassDistributedMaterial2 c: RoleClassDistributedMaterial2.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}

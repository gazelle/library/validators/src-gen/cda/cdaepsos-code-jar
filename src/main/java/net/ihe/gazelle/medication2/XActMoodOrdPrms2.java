/**
 * XActMoodOrdPrms2.java
 *
 * File generated from the medication2::XActMoodOrdPrms2 uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration XActMoodOrdPrms2.
 *
 * 
 */

@XmlType(name = "XActMoodOrdPrms")
@XmlEnum
@XmlRootElement(name = "XActMoodOrdPrms2")
public enum XActMoodOrdPrms2 {
	@XmlEnumValue("PRMS")
	PRMS("PRMS"),
	@XmlEnumValue("RQO")
	RQO("RQO");
	
	private final String value;

    XActMoodOrdPrms2(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static XActMoodOrdPrms2 fromValue(String v) {
        for (XActMoodOrdPrms2 c: XActMoodOrdPrms2.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}

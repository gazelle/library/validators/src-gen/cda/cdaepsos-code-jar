package net.ihe.gazelle.medication2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {

	private final static QName _AD2Group_QNAME = new QName("", "group:2");
	private final static QName _AD2Delimiter_QNAME = new QName("urn:epsos-org:ep:medication", "delimiter");
	private final static QName _AD2Country_QNAME = new QName("urn:epsos-org:ep:medication", "country");
	private final static QName _AD2State_QNAME = new QName("urn:epsos-org:ep:medication", "state");
	private final static QName _AD2County_QNAME = new QName("urn:epsos-org:ep:medication", "county");
	private final static QName _AD2City_QNAME = new QName("urn:epsos-org:ep:medication", "city");
	private final static QName _AD2PostalCode_QNAME = new QName("urn:epsos-org:ep:medication", "postalCode");
	private final static QName _AD2StreetAddressLine_QNAME = new QName("urn:epsos-org:ep:medication", "streetAddressLine");
	private final static QName _AD2HouseNumber_QNAME = new QName("urn:epsos-org:ep:medication", "houseNumber");
	private final static QName _AD2HouseNumberNumeric_QNAME = new QName("urn:epsos-org:ep:medication", "houseNumberNumeric");
	private final static QName _AD2Direction_QNAME = new QName("urn:epsos-org:ep:medication", "direction");
	private final static QName _AD2StreetName_QNAME = new QName("urn:epsos-org:ep:medication", "streetName");
	private final static QName _AD2StreetNameBase_QNAME = new QName("urn:epsos-org:ep:medication", "streetNameBase");
	private final static QName _AD2StreetNameType_QNAME = new QName("urn:epsos-org:ep:medication", "streetNameType");
	private final static QName _AD2AdditionalLocator_QNAME = new QName("urn:epsos-org:ep:medication", "additionalLocator");
	private final static QName _AD2UnitID_QNAME = new QName("urn:epsos-org:ep:medication", "unitID");
	private final static QName _AD2UnitType_QNAME = new QName("urn:epsos-org:ep:medication", "unitType");
	private final static QName _AD2CareOf_QNAME = new QName("urn:epsos-org:ep:medication", "careOf");
	private final static QName _AD2CensusTract_QNAME = new QName("urn:epsos-org:ep:medication", "censusTract");
	private final static QName _AD2DeliveryAddressLine_QNAME = new QName("urn:epsos-org:ep:medication", "deliveryAddressLine");
	private final static QName _AD2DeliveryInstallationType_QNAME = new QName("urn:epsos-org:ep:medication", "deliveryInstallationType");
	private final static QName _AD2DeliveryInstallationArea_QNAME = new QName("urn:epsos-org:ep:medication", "deliveryInstallationArea");
	private final static QName _AD2DeliveryInstallationQualifier_QNAME = new QName("urn:epsos-org:ep:medication", "deliveryInstallationQualifier");
	private final static QName _AD2DeliveryMode_QNAME = new QName("urn:epsos-org:ep:medication", "deliveryMode");
	private final static QName _AD2DeliveryModeIdentifier_QNAME = new QName("urn:epsos-org:ep:medication", "deliveryModeIdentifier");
	private final static QName _AD2BuildingNumberSuffix_QNAME = new QName("urn:epsos-org:ep:medication", "buildingNumberSuffix");
	private final static QName _AD2PostBox_QNAME = new QName("urn:epsos-org:ep:medication", "postBox");
	private final static QName _AD2Precinct_QNAME = new QName("urn:epsos-org:ep:medication", "precinct");
	private final static QName _AD2UseablePeriod_QNAME = new QName("urn:epsos-org:ep:medication", "useablePeriod");
	private final static QName _ED2Reference_QNAME = new QName("urn:epsos-org:ep:medication", "reference");
	private final static QName _ED2Thumbnail_QNAME = new QName("urn:epsos-org:ep:medication", "thumbnail");
	private final static QName _EN2Group_QNAME = new QName("", "group:2");
	private final static QName _EN2Delimiter_QNAME = new QName("urn:epsos-org:ep:medication", "delimiter");
	private final static QName _EN2Family_QNAME = new QName("urn:epsos-org:ep:medication", "family");
	private final static QName _EN2Given_QNAME = new QName("urn:epsos-org:ep:medication", "given");
	private final static QName _EN2Prefix_QNAME = new QName("urn:epsos-org:ep:medication", "prefix");
	private final static QName _EN2Suffix_QNAME = new QName("urn:epsos-org:ep:medication", "suffix");
	private final static QName _EN2ValidTime_QNAME = new QName("urn:epsos-org:ep:medication", "validTime");
	private final static QName _DocumentRoot2XMLNSPrefixMap_QNAME = new QName("", "xmlns:prefix");
	private final static QName _DocumentRoot2XSISchemaLocation_QNAME = new QName("", "xsi:schemaLocation");
	private final static QName _DocumentRoot2AsContent_QNAME = new QName("urn:epsos-org:ep:medication", "asContent");
	private final static QName _DocumentRoot2AsDistributedProduct_QNAME = new QName("urn:epsos-org:ep:medication", "asDistributedProduct");
	private final static QName _DocumentRoot2AsMedicineManufacturer_QNAME = new QName("urn:epsos-org:ep:medication", "asMedicineManufacturer");
	private final static QName _DocumentRoot2AsSpecializedKind_QNAME = new QName("urn:epsos-org:ep:medication", "asSpecializedKind");
	private final static QName _DocumentRoot2Desc_QNAME = new QName("urn:epsos-org:ep:medication", "desc");
	private final static QName _DocumentRoot2ExpirationTime_QNAME = new QName("urn:epsos-org:ep:medication", "expirationTime");
	private final static QName _DocumentRoot2FormCode_QNAME = new QName("urn:epsos-org:ep:medication", "formCode");
	private final static QName _DocumentRoot2HandlingCode_QNAME = new QName("urn:epsos-org:ep:medication", "handlingCode");
	private final static QName _DocumentRoot2Id_QNAME = new QName("urn:epsos-org:ep:medication", "id");
	private final static QName _DocumentRoot2Ingredient_QNAME = new QName("urn:epsos-org:ep:medication", "ingredient");
	private final static QName _DocumentRoot2Medication_QNAME = new QName("urn:epsos-org:ep:medication", "medication");
	private final static QName _DocumentRoot2Part_QNAME = new QName("urn:epsos-org:ep:medication", "part");
	private final static QName _DocumentRoot2RiskCode_QNAME = new QName("urn:epsos-org:ep:medication", "riskCode");
	private final static QName _DocumentRoot2StabilityTime_QNAME = new QName("urn:epsos-org:ep:medication", "stabilityTime");
	private final static QName _DocumentRoot2SubjectOf1_QNAME = new QName("urn:epsos-org:ep:medication", "subjectOf1");
	private final static QName _DocumentRoot2SubjectOf2_QNAME = new QName("urn:epsos-org:ep:medication", "subjectOf2");
	private final static QName _DocumentRoot2SubjectOf3_QNAME = new QName("urn:epsos-org:ep:medication", "subjectOf3");
	private final static QName _DocumentRoot2SubjectOf4_QNAME = new QName("urn:epsos-org:ep:medication", "subjectOf4");
	private final static QName _DocumentRoot2SubjectOf5_QNAME = new QName("urn:epsos-org:ep:medication", "subjectOf5");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {}
    
    /**
     * Create an instance of {@link  AD2}
     * 
     */
    public AD2 createAD2() {
        return new AD2();
    }

    /**
     * Create an instance of {@link  AdxpDelimiter2}
     * 
     */
    public AdxpDelimiter2 createAdxpDelimiter2() {
        return new AdxpDelimiter2();
    }

    /**
     * Create an instance of {@link  ADXP2}
     * 
     */
    public ADXP2 createADXP2() {
        return new ADXP2();
    }

    /**
     * Create an instance of {@link  ST12}
     * 
     */
    public ST12 createST12() {
        return new ST12();
    }

    /**
     * Create an instance of {@link  ED2}
     * 
     */
    public ED2 createED2() {
        return new ED2();
    }

    /**
     * Create an instance of {@link  TEL2}
     * 
     */
    public TEL2 createTEL2() {
        return new TEL2();
    }

    /**
     * Create an instance of {@link  SXCMTS2}
     * 
     */
    public SXCMTS2 createSXCMTS2() {
        return new SXCMTS2();
    }

    /**
     * Create an instance of {@link  TS12}
     * 
     */
    public TS12 createTS12() {
        return new TS12();
    }

    /**
     * Create an instance of {@link  Thumbnail2}
     * 
     */
    public Thumbnail2 createThumbnail2() {
        return new Thumbnail2();
    }

    /**
     * Create an instance of {@link  AdxpCountry2}
     * 
     */
    public AdxpCountry2 createAdxpCountry2() {
        return new AdxpCountry2();
    }

    /**
     * Create an instance of {@link  AdxpState2}
     * 
     */
    public AdxpState2 createAdxpState2() {
        return new AdxpState2();
    }

    /**
     * Create an instance of {@link  AdxpCounty2}
     * 
     */
    public AdxpCounty2 createAdxpCounty2() {
        return new AdxpCounty2();
    }

    /**
     * Create an instance of {@link  AdxpCity2}
     * 
     */
    public AdxpCity2 createAdxpCity2() {
        return new AdxpCity2();
    }

    /**
     * Create an instance of {@link  AdxpPostalCode2}
     * 
     */
    public AdxpPostalCode2 createAdxpPostalCode2() {
        return new AdxpPostalCode2();
    }

    /**
     * Create an instance of {@link  AdxpStreetAddressLine2}
     * 
     */
    public AdxpStreetAddressLine2 createAdxpStreetAddressLine2() {
        return new AdxpStreetAddressLine2();
    }

    /**
     * Create an instance of {@link  AdxpHouseNumber2}
     * 
     */
    public AdxpHouseNumber2 createAdxpHouseNumber2() {
        return new AdxpHouseNumber2();
    }

    /**
     * Create an instance of {@link  AdxpHouseNumberNumeric2}
     * 
     */
    public AdxpHouseNumberNumeric2 createAdxpHouseNumberNumeric2() {
        return new AdxpHouseNumberNumeric2();
    }

    /**
     * Create an instance of {@link  AdxpDirection2}
     * 
     */
    public AdxpDirection2 createAdxpDirection2() {
        return new AdxpDirection2();
    }

    /**
     * Create an instance of {@link  AdxpStreetName2}
     * 
     */
    public AdxpStreetName2 createAdxpStreetName2() {
        return new AdxpStreetName2();
    }

    /**
     * Create an instance of {@link  AdxpStreetNameBase2}
     * 
     */
    public AdxpStreetNameBase2 createAdxpStreetNameBase2() {
        return new AdxpStreetNameBase2();
    }

    /**
     * Create an instance of {@link  AdxpStreetNameType2}
     * 
     */
    public AdxpStreetNameType2 createAdxpStreetNameType2() {
        return new AdxpStreetNameType2();
    }

    /**
     * Create an instance of {@link  AdxpAdditionalLocator2}
     * 
     */
    public AdxpAdditionalLocator2 createAdxpAdditionalLocator2() {
        return new AdxpAdditionalLocator2();
    }

    /**
     * Create an instance of {@link  AdxpUnitID2}
     * 
     */
    public AdxpUnitID2 createAdxpUnitID2() {
        return new AdxpUnitID2();
    }

    /**
     * Create an instance of {@link  AdxpUnitType2}
     * 
     */
    public AdxpUnitType2 createAdxpUnitType2() {
        return new AdxpUnitType2();
    }

    /**
     * Create an instance of {@link  AdxpCareOf2}
     * 
     */
    public AdxpCareOf2 createAdxpCareOf2() {
        return new AdxpCareOf2();
    }

    /**
     * Create an instance of {@link  AdxpCensusTract2}
     * 
     */
    public AdxpCensusTract2 createAdxpCensusTract2() {
        return new AdxpCensusTract2();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryAddressLine2}
     * 
     */
    public AdxpDeliveryAddressLine2 createAdxpDeliveryAddressLine2() {
        return new AdxpDeliveryAddressLine2();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryInstallationType2}
     * 
     */
    public AdxpDeliveryInstallationType2 createAdxpDeliveryInstallationType2() {
        return new AdxpDeliveryInstallationType2();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryInstallationArea2}
     * 
     */
    public AdxpDeliveryInstallationArea2 createAdxpDeliveryInstallationArea2() {
        return new AdxpDeliveryInstallationArea2();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryInstallationQualifier2}
     * 
     */
    public AdxpDeliveryInstallationQualifier2 createAdxpDeliveryInstallationQualifier2() {
        return new AdxpDeliveryInstallationQualifier2();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryMode2}
     * 
     */
    public AdxpDeliveryMode2 createAdxpDeliveryMode2() {
        return new AdxpDeliveryMode2();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryModeIdentifier2}
     * 
     */
    public AdxpDeliveryModeIdentifier2 createAdxpDeliveryModeIdentifier2() {
        return new AdxpDeliveryModeIdentifier2();
    }

    /**
     * Create an instance of {@link  AdxpBuildingNumberSuffix2}
     * 
     */
    public AdxpBuildingNumberSuffix2 createAdxpBuildingNumberSuffix2() {
        return new AdxpBuildingNumberSuffix2();
    }

    /**
     * Create an instance of {@link  AdxpPostBox2}
     * 
     */
    public AdxpPostBox2 createAdxpPostBox2() {
        return new AdxpPostBox2();
    }

    /**
     * Create an instance of {@link  AdxpPrecinct2}
     * 
     */
    public AdxpPrecinct2 createAdxpPrecinct2() {
        return new AdxpPrecinct2();
    }

    /**
     * Create an instance of {@link  ANYNonNull2}
     * 
     */
    public ANYNonNull2 createANYNonNull2() {
        return new ANYNonNull2();
    }

    /**
     * Create an instance of {@link  BL12}
     * 
     */
    public BL12 createBL12() {
        return new BL12();
    }

    /**
     * Create an instance of {@link  BN12}
     * 
     */
    public BN12 createBN12() {
        return new BN12();
    }

    /**
     * Create an instance of {@link  BXITCD2}
     * 
     */
    public BXITCD2 createBXITCD2() {
        return new BXITCD2();
    }

    /**
     * Create an instance of {@link  CD2}
     * 
     */
    public CD2 createCD2() {
        return new CD2();
    }

    /**
     * Create an instance of {@link  CR2}
     * 
     */
    public CR2 createCR2() {
        return new CR2();
    }

    /**
     * Create an instance of {@link  CV2}
     * 
     */
    public CV2 createCV2() {
        return new CV2();
    }

    /**
     * Create an instance of {@link  CE2}
     * 
     */
    public CE2 createCE2() {
        return new CE2();
    }

    /**
     * Create an instance of {@link  BXITIVLPQ2}
     * 
     */
    public BXITIVLPQ2 createBXITIVLPQ2() {
        return new BXITIVLPQ2();
    }

    /**
     * Create an instance of {@link  IVLPQ2}
     * 
     */
    public IVLPQ2 createIVLPQ2() {
        return new IVLPQ2();
    }

    /**
     * Create an instance of {@link  SXCMPQ2}
     * 
     */
    public SXCMPQ2 createSXCMPQ2() {
        return new SXCMPQ2();
    }

    /**
     * Create an instance of {@link  PQ2}
     * 
     */
    public PQ2 createPQ2() {
        return new PQ2();
    }

    /**
     * Create an instance of {@link  PQR2}
     * 
     */
    public PQR2 createPQR2() {
        return new PQR2();
    }

    /**
     * Create an instance of {@link  IVXBPQ2}
     * 
     */
    public IVXBPQ2 createIVXBPQ2() {
        return new IVXBPQ2();
    }

    /**
     * Create an instance of {@link  CO2}
     * 
     */
    public CO2 createCO2() {
        return new CO2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVAgency2}
     * 
     */
    public COCTMT230100UVAgency2 createCOCTMT230100UVAgency2() {
        return new COCTMT230100UVAgency2();
    }

    /**
     * Create an instance of {@link  II2}
     * 
     */
    public II2 createII2() {
        return new II2();
    }

    /**
     * Create an instance of {@link  ON2}
     * 
     */
    public ON2 createON2() {
        return new ON2();
    }

    /**
     * Create an instance of {@link  EN2}
     * 
     */
    public EN2 createEN2() {
        return new EN2();
    }

    /**
     * Create an instance of {@link  EnDelimiter2}
     * 
     */
    public EnDelimiter2 createEnDelimiter2() {
        return new EnDelimiter2();
    }

    /**
     * Create an instance of {@link  ENXP2}
     * 
     */
    public ENXP2 createENXP2() {
        return new ENXP2();
    }

    /**
     * Create an instance of {@link  EnFamily2}
     * 
     */
    public EnFamily2 createEnFamily2() {
        return new EnFamily2();
    }

    /**
     * Create an instance of {@link  EnGiven2}
     * 
     */
    public EnGiven2 createEnGiven2() {
        return new EnGiven2();
    }

    /**
     * Create an instance of {@link  EnPrefix2}
     * 
     */
    public EnPrefix2 createEnPrefix2() {
        return new EnPrefix2();
    }

    /**
     * Create an instance of {@link  EnSuffix2}
     * 
     */
    public EnSuffix2 createEnSuffix2() {
        return new EnSuffix2();
    }

    /**
     * Create an instance of {@link  IVLTS2}
     * 
     */
    public IVLTS2 createIVLTS2() {
        return new IVLTS2();
    }

    /**
     * Create an instance of {@link  IVXBTS2}
     * 
     */
    public IVXBTS2 createIVXBTS2() {
        return new IVXBTS2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVApproval2}
     * 
     */
    public COCTMT230100UVApproval2 createCOCTMT230100UVApproval2() {
        return new COCTMT230100UVApproval2();
    }

    /**
     * Create an instance of {@link  CS12}
     * 
     */
    public CS12 createCS12() {
        return new CS12();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVHolder2}
     * 
     */
    public COCTMT230100UVHolder2 createCOCTMT230100UVHolder2() {
        return new COCTMT230100UVHolder2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVRole2}
     * 
     */
    public COCTMT230100UVRole2 createCOCTMT230100UVRole2() {
        return new COCTMT230100UVRole2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVManufacturer2}
     * 
     */
    public COCTMT230100UVManufacturer2 createCOCTMT230100UVManufacturer2() {
        return new COCTMT230100UVManufacturer2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVRelatedManufacturer2}
     * 
     */
    public COCTMT230100UVRelatedManufacturer2 createCOCTMT230100UVRelatedManufacturer2() {
        return new COCTMT230100UVRelatedManufacturer2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVAuthor2}
     * 
     */
    public COCTMT230100UVAuthor2 createCOCTMT230100UVAuthor2() {
        return new COCTMT230100UVAuthor2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVTerritorialAuthority2}
     * 
     */
    public COCTMT230100UVTerritorialAuthority2 createCOCTMT230100UVTerritorialAuthority2() {
        return new COCTMT230100UVTerritorialAuthority2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVCountry2}
     * 
     */
    public COCTMT230100UVCountry2 createCOCTMT230100UVCountry2() {
        return new COCTMT230100UVCountry2();
    }

    /**
     * Create an instance of {@link  TN2}
     * 
     */
    public TN2 createTN2() {
        return new TN2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVCharacteristic2}
     * 
     */
    public COCTMT230100UVCharacteristic2 createCOCTMT230100UVCharacteristic2() {
        return new COCTMT230100UVCharacteristic2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVContent2}
     * 
     */
    public COCTMT230100UVContent2 createCOCTMT230100UVContent2() {
        return new COCTMT230100UVContent2();
    }

    /**
     * Create an instance of {@link  RTOQTYQTY2}
     * 
     */
    public RTOQTYQTY2 createRTOQTYQTY2() {
        return new RTOQTYQTY2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVPackagedMedicine2}
     * 
     */
    public COCTMT230100UVPackagedMedicine2 createCOCTMT230100UVPackagedMedicine2() {
        return new COCTMT230100UVPackagedMedicine2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVManufacturedProduct2}
     * 
     */
    public COCTMT230100UVManufacturedProduct2 createCOCTMT230100UVManufacturedProduct2() {
        return new COCTMT230100UVManufacturedProduct2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject252}
     * 
     */
    public COCTMT230100UVSubject252 createCOCTMT230100UVSubject252() {
        return new COCTMT230100UVSubject252();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject152}
     * 
     */
    public COCTMT230100UVSubject152 createCOCTMT230100UVSubject152() {
        return new COCTMT230100UVSubject152();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVPolicy2}
     * 
     */
    public COCTMT230100UVPolicy2 createCOCTMT230100UVPolicy2() {
        return new COCTMT230100UVPolicy2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject162}
     * 
     */
    public COCTMT230100UVSubject162 createCOCTMT230100UVSubject162() {
        return new COCTMT230100UVSubject162();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSuperContent2}
     * 
     */
    public COCTMT230100UVSuperContent2 createCOCTMT230100UVSuperContent2() {
        return new COCTMT230100UVSuperContent2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubContent2}
     * 
     */
    public COCTMT230100UVSubContent2 createCOCTMT230100UVSubContent2() {
        return new COCTMT230100UVSubContent2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject142}
     * 
     */
    public COCTMT230100UVSubject142 createCOCTMT230100UVSubject142() {
        return new COCTMT230100UVSubject142();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject112}
     * 
     */
    public COCTMT230100UVSubject112 createCOCTMT230100UVSubject112() {
        return new COCTMT230100UVSubject112();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVDistributedProduct2}
     * 
     */
    public COCTMT230100UVDistributedProduct2 createCOCTMT230100UVDistributedProduct2() {
        return new COCTMT230100UVDistributedProduct2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVIngredient2}
     * 
     */
    public COCTMT230100UVIngredient2 createCOCTMT230100UVIngredient2() {
        return new COCTMT230100UVIngredient2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubstance2}
     * 
     */
    public COCTMT230100UVSubstance2 createCOCTMT230100UVSubstance2() {
        return new COCTMT230100UVSubstance2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubstanceManufacturer2}
     * 
     */
    public COCTMT230100UVSubstanceManufacturer2 createCOCTMT230100UVSubstanceManufacturer2() {
        return new COCTMT230100UVSubstanceManufacturer2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubIngredient2}
     * 
     */
    public COCTMT230100UVSubIngredient2 createCOCTMT230100UVSubIngredient2() {
        return new COCTMT230100UVSubIngredient2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVMedication2}
     * 
     */
    public COCTMT230100UVMedication2 createCOCTMT230100UVMedication2() {
        return new COCTMT230100UVMedication2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVMedicine2}
     * 
     */
    public COCTMT230100UVMedicine2 createCOCTMT230100UVMedicine2() {
        return new COCTMT230100UVMedicine2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVMedicineManufacturer2}
     * 
     */
    public COCTMT230100UVMedicineManufacturer2 createCOCTMT230100UVMedicineManufacturer2() {
        return new COCTMT230100UVMedicineManufacturer2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSpecializedKind2}
     * 
     */
    public COCTMT230100UVSpecializedKind2 createCOCTMT230100UVSpecializedKind2() {
        return new COCTMT230100UVSpecializedKind2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVMedicineClass2}
     * 
     */
    public COCTMT230100UVMedicineClass2 createCOCTMT230100UVMedicineClass2() {
        return new COCTMT230100UVMedicineClass2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVPart2}
     * 
     */
    public COCTMT230100UVPart2 createCOCTMT230100UVPart2() {
        return new COCTMT230100UVPart2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject42}
     * 
     */
    public COCTMT230100UVSubject42 createCOCTMT230100UVSubject42() {
        return new COCTMT230100UVSubject42();
    }



    /**
     * Create an instance of {@link  COCTMT230100UVSubject12}
     * 
     */
    public COCTMT230100UVSubject12 createCOCTMT230100UVSubject12() {
        return new COCTMT230100UVSubject12();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject222}
     * 
     */
    public COCTMT230100UVSubject222 createCOCTMT230100UVSubject222() {
        return new COCTMT230100UVSubject222();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject32}
     * 
     */
    public COCTMT230100UVSubject32 createCOCTMT230100UVSubject32() {
        return new COCTMT230100UVSubject32();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVObservationGoal2}
     * 
     */
    public COCTMT230100UVObservationGoal2 createCOCTMT230100UVObservationGoal2() {
        return new COCTMT230100UVObservationGoal2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject72}
     * 
     */
    public COCTMT230100UVSubject72 createCOCTMT230100UVSubject72() {
        return new COCTMT230100UVSubject72();
    }

    /**
     * Create an instance of {@link  COCTMT440001UVValuedItem2}
     * 
     */
    public COCTMT440001UVValuedItem2 createCOCTMT440001UVValuedItem2() {
        return new COCTMT440001UVValuedItem2();
    }

    /**
     * Create an instance of {@link  RTOPQPQ2}
     * 
     */
    public RTOPQPQ2 createRTOPQPQ2() {
        return new RTOPQPQ2();
    }

    /**
     * Create an instance of {@link  RTOMOPQ2}
     * 
     */
    public RTOMOPQ2 createRTOMOPQ2() {
        return new RTOMOPQ2();
    }

    /**
     * Create an instance of {@link  MO2}
     * 
     */
    public MO2 createMO2() {
        return new MO2();
    }

    /**
     * Create an instance of {@link  DocumentRoot2}
     * 
     */
    public DocumentRoot2 createDocumentRoot2() {
        return new DocumentRoot2();
    }

    /**
     * Create an instance of {@link  EIVLEvent2}
     * 
     */
    public EIVLEvent2 createEIVLEvent2() {
        return new EIVLEvent2();
    }

    /**
     * Create an instance of {@link  EIVLPPDTS2}
     * 
     */
    public EIVLPPDTS2 createEIVLPPDTS2() {
        return new EIVLPPDTS2();
    }

    /**
     * Create an instance of {@link  SXCMPPDTS2}
     * 
     */
    public SXCMPPDTS2 createSXCMPPDTS2() {
        return new SXCMPPDTS2();
    }

    /**
     * Create an instance of {@link  PPDTS2}
     * 
     */
    public PPDTS2 createPPDTS2() {
        return new PPDTS2();
    }

    /**
     * Create an instance of {@link  IVLPPDPQ2}
     * 
     */
    public IVLPPDPQ2 createIVLPPDPQ2() {
        return new IVLPPDPQ2();
    }

    /**
     * Create an instance of {@link  SXCMPPDPQ2}
     * 
     */
    public SXCMPPDPQ2 createSXCMPPDPQ2() {
        return new SXCMPPDPQ2();
    }

    /**
     * Create an instance of {@link  PPDPQ2}
     * 
     */
    public PPDPQ2 createPPDPQ2() {
        return new PPDPQ2();
    }

    /**
     * Create an instance of {@link  IVXBPPDPQ2}
     * 
     */
    public IVXBPPDPQ2 createIVXBPPDPQ2() {
        return new IVXBPPDPQ2();
    }

    /**
     * Create an instance of {@link  EIVLTS2}
     * 
     */
    public EIVLTS2 createEIVLTS2() {
        return new EIVLTS2();
    }

    /**
     * Create an instance of {@link  GLISTPQ2}
     * 
     */
    public GLISTPQ2 createGLISTPQ2() {
        return new GLISTPQ2();
    }

    /**
     * Create an instance of {@link  GLISTTS2}
     * 
     */
    public GLISTTS2 createGLISTTS2() {
        return new GLISTTS2();
    }

    /**
     * Create an instance of {@link  HXITCE2}
     * 
     */
    public HXITCE2 createHXITCE2() {
        return new HXITCE2();
    }

    /**
     * Create an instance of {@link  HXITPQ2}
     * 
     */
    public HXITPQ2 createHXITPQ2() {
        return new HXITPQ2();
    }

    /**
     * Create an instance of {@link  INT12}
     * 
     */
    public INT12 createINT12() {
        return new INT12();
    }

    /**
     * Create an instance of {@link  IVLINT2}
     * 
     */
    public IVLINT2 createIVLINT2() {
        return new IVLINT2();
    }

    /**
     * Create an instance of {@link  SXCMINT2}
     * 
     */
    public SXCMINT2 createSXCMINT2() {
        return new SXCMINT2();
    }

    /**
     * Create an instance of {@link  IVXBINT2}
     * 
     */
    public IVXBINT2 createIVXBINT2() {
        return new IVXBINT2();
    }

    /**
     * Create an instance of {@link  IVLMO2}
     * 
     */
    public IVLMO2 createIVLMO2() {
        return new IVLMO2();
    }

    /**
     * Create an instance of {@link  SXCMMO2}
     * 
     */
    public SXCMMO2 createSXCMMO2() {
        return new SXCMMO2();
    }

    /**
     * Create an instance of {@link  IVXBMO2}
     * 
     */
    public IVXBMO2 createIVXBMO2() {
        return new IVXBMO2();
    }

    /**
     * Create an instance of {@link  IVLPPDTS2}
     * 
     */
    public IVLPPDTS2 createIVLPPDTS2() {
        return new IVLPPDTS2();
    }

    /**
     * Create an instance of {@link  IVXBPPDTS2}
     * 
     */
    public IVXBPPDTS2 createIVXBPPDTS2() {
        return new IVXBPPDTS2();
    }

    /**
     * Create an instance of {@link  IVLREAL2}
     * 
     */
    public IVLREAL2 createIVLREAL2() {
        return new IVLREAL2();
    }

    /**
     * Create an instance of {@link  SXCMREAL2}
     * 
     */
    public SXCMREAL2 createSXCMREAL2() {
        return new SXCMREAL2();
    }

    /**
     * Create an instance of {@link  REAL12}
     * 
     */
    public REAL12 createREAL12() {
        return new REAL12();
    }

    /**
     * Create an instance of {@link  IVXBREAL2}
     * 
     */
    public IVXBREAL2 createIVXBREAL2() {
        return new IVXBREAL2();
    }

    /**
     * Create an instance of {@link  PIVLPPDTS2}
     * 
     */
    public PIVLPPDTS2 createPIVLPPDTS2() {
        return new PIVLPPDTS2();
    }

    /**
     * Create an instance of {@link  PIVLTS2}
     * 
     */
    public PIVLTS2 createPIVLTS2() {
        return new PIVLTS2();
    }

    /**
     * Create an instance of {@link  PN2}
     * 
     */
    public PN2 createPN2() {
        return new PN2();
    }

    /**
     * Create an instance of {@link  RTO2}
     * 
     */
    public RTO2 createRTO2() {
        return new RTO2();
    }

    /**
     * Create an instance of {@link  SC2}
     * 
     */
    public SC2 createSC2() {
        return new SC2();
    }

    /**
     * Create an instance of {@link  SLISTPQ2}
     * 
     */
    public SLISTPQ2 createSLISTPQ2() {
        return new SLISTPQ2();
    }

    /**
     * Create an instance of {@link  SLISTTS2}
     * 
     */
    public SLISTTS2 createSLISTTS2() {
        return new SLISTTS2();
    }

    /**
     * Create an instance of {@link  SXCMCD2}
     * 
     */
    public SXCMCD2 createSXCMCD2() {
        return new SXCMCD2();
    }

    /**
     * Create an instance of {@link  SXPRTS2}
     * 
     */
    public SXPRTS2 createSXPRTS2() {
        return new SXPRTS2();
    }

    /**
     * Create an instance of {@link  UVPTS2}
     * 
     */
    public UVPTS2 createUVPTS2() {
        return new UVPTS2();
    }



	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDelimiter2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "delimiter", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpDelimiter2> createAD2Delimiter(net.ihe.gazelle.medication2.AdxpDelimiter2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpDelimiter2>(_AD2Delimiter_QNAME, net.ihe.gazelle.medication2.AdxpDelimiter2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpCountry2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "country", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpCountry2> createAD2Country(net.ihe.gazelle.medication2.AdxpCountry2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpCountry2>(_AD2Country_QNAME, net.ihe.gazelle.medication2.AdxpCountry2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpState2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "state", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpState2> createAD2State(net.ihe.gazelle.medication2.AdxpState2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpState2>(_AD2State_QNAME, net.ihe.gazelle.medication2.AdxpState2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpCounty2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "county", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpCounty2> createAD2County(net.ihe.gazelle.medication2.AdxpCounty2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpCounty2>(_AD2County_QNAME, net.ihe.gazelle.medication2.AdxpCounty2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpCity2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "city", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpCity2> createAD2City(net.ihe.gazelle.medication2.AdxpCity2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpCity2>(_AD2City_QNAME, net.ihe.gazelle.medication2.AdxpCity2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpPostalCode2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "postalCode", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpPostalCode2> createAD2PostalCode(net.ihe.gazelle.medication2.AdxpPostalCode2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpPostalCode2>(_AD2PostalCode_QNAME, net.ihe.gazelle.medication2.AdxpPostalCode2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpStreetAddressLine2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "streetAddressLine", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpStreetAddressLine2> createAD2StreetAddressLine(net.ihe.gazelle.medication2.AdxpStreetAddressLine2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpStreetAddressLine2>(_AD2StreetAddressLine_QNAME, net.ihe.gazelle.medication2.AdxpStreetAddressLine2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpHouseNumber2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "houseNumber", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpHouseNumber2> createAD2HouseNumber(net.ihe.gazelle.medication2.AdxpHouseNumber2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpHouseNumber2>(_AD2HouseNumber_QNAME, net.ihe.gazelle.medication2.AdxpHouseNumber2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpHouseNumberNumeric2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "houseNumberNumeric", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpHouseNumberNumeric2> createAD2HouseNumberNumeric(net.ihe.gazelle.medication2.AdxpHouseNumberNumeric2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpHouseNumberNumeric2>(_AD2HouseNumberNumeric_QNAME, net.ihe.gazelle.medication2.AdxpHouseNumberNumeric2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDirection2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "direction", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpDirection2> createAD2Direction(net.ihe.gazelle.medication2.AdxpDirection2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpDirection2>(_AD2Direction_QNAME, net.ihe.gazelle.medication2.AdxpDirection2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpStreetName2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "streetName", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpStreetName2> createAD2StreetName(net.ihe.gazelle.medication2.AdxpStreetName2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpStreetName2>(_AD2StreetName_QNAME, net.ihe.gazelle.medication2.AdxpStreetName2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpStreetNameBase2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "streetNameBase", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpStreetNameBase2> createAD2StreetNameBase(net.ihe.gazelle.medication2.AdxpStreetNameBase2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpStreetNameBase2>(_AD2StreetNameBase_QNAME, net.ihe.gazelle.medication2.AdxpStreetNameBase2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpStreetNameType2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "streetNameType", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpStreetNameType2> createAD2StreetNameType(net.ihe.gazelle.medication2.AdxpStreetNameType2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpStreetNameType2>(_AD2StreetNameType_QNAME, net.ihe.gazelle.medication2.AdxpStreetNameType2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpAdditionalLocator2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "additionalLocator", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpAdditionalLocator2> createAD2AdditionalLocator(net.ihe.gazelle.medication2.AdxpAdditionalLocator2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpAdditionalLocator2>(_AD2AdditionalLocator_QNAME, net.ihe.gazelle.medication2.AdxpAdditionalLocator2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpUnitID2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "unitID", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpUnitID2> createAD2UnitID(net.ihe.gazelle.medication2.AdxpUnitID2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpUnitID2>(_AD2UnitID_QNAME, net.ihe.gazelle.medication2.AdxpUnitID2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpUnitType2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "unitType", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpUnitType2> createAD2UnitType(net.ihe.gazelle.medication2.AdxpUnitType2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpUnitType2>(_AD2UnitType_QNAME, net.ihe.gazelle.medication2.AdxpUnitType2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpCareOf2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "careOf", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpCareOf2> createAD2CareOf(net.ihe.gazelle.medication2.AdxpCareOf2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpCareOf2>(_AD2CareOf_QNAME, net.ihe.gazelle.medication2.AdxpCareOf2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpCensusTract2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "censusTract", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpCensusTract2> createAD2CensusTract(net.ihe.gazelle.medication2.AdxpCensusTract2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpCensusTract2>(_AD2CensusTract_QNAME, net.ihe.gazelle.medication2.AdxpCensusTract2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryAddressLine2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "deliveryAddressLine", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpDeliveryAddressLine2> createAD2DeliveryAddressLine(net.ihe.gazelle.medication2.AdxpDeliveryAddressLine2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpDeliveryAddressLine2>(_AD2DeliveryAddressLine_QNAME, net.ihe.gazelle.medication2.AdxpDeliveryAddressLine2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryInstallationType2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "deliveryInstallationType", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpDeliveryInstallationType2> createAD2DeliveryInstallationType(net.ihe.gazelle.medication2.AdxpDeliveryInstallationType2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpDeliveryInstallationType2>(_AD2DeliveryInstallationType_QNAME, net.ihe.gazelle.medication2.AdxpDeliveryInstallationType2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryInstallationArea2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "deliveryInstallationArea", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpDeliveryInstallationArea2> createAD2DeliveryInstallationArea(net.ihe.gazelle.medication2.AdxpDeliveryInstallationArea2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpDeliveryInstallationArea2>(_AD2DeliveryInstallationArea_QNAME, net.ihe.gazelle.medication2.AdxpDeliveryInstallationArea2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryInstallationQualifier2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "deliveryInstallationQualifier", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpDeliveryInstallationQualifier2> createAD2DeliveryInstallationQualifier(net.ihe.gazelle.medication2.AdxpDeliveryInstallationQualifier2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpDeliveryInstallationQualifier2>(_AD2DeliveryInstallationQualifier_QNAME, net.ihe.gazelle.medication2.AdxpDeliveryInstallationQualifier2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryMode2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "deliveryMode", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpDeliveryMode2> createAD2DeliveryMode(net.ihe.gazelle.medication2.AdxpDeliveryMode2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpDeliveryMode2>(_AD2DeliveryMode_QNAME, net.ihe.gazelle.medication2.AdxpDeliveryMode2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryModeIdentifier2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "deliveryModeIdentifier", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpDeliveryModeIdentifier2> createAD2DeliveryModeIdentifier(net.ihe.gazelle.medication2.AdxpDeliveryModeIdentifier2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpDeliveryModeIdentifier2>(_AD2DeliveryModeIdentifier_QNAME, net.ihe.gazelle.medication2.AdxpDeliveryModeIdentifier2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpBuildingNumberSuffix2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "buildingNumberSuffix", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpBuildingNumberSuffix2> createAD2BuildingNumberSuffix(net.ihe.gazelle.medication2.AdxpBuildingNumberSuffix2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpBuildingNumberSuffix2>(_AD2BuildingNumberSuffix_QNAME, net.ihe.gazelle.medication2.AdxpBuildingNumberSuffix2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpPostBox2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "postBox", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpPostBox2> createAD2PostBox(net.ihe.gazelle.medication2.AdxpPostBox2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpPostBox2>(_AD2PostBox_QNAME, net.ihe.gazelle.medication2.AdxpPostBox2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpPrecinct2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "precinct", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.AdxpPrecinct2> createAD2Precinct(net.ihe.gazelle.medication2.AdxpPrecinct2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.AdxpPrecinct2>(_AD2Precinct_QNAME, net.ihe.gazelle.medication2.AdxpPrecinct2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link SXCMTS2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "useablePeriod", scope = AD2.class)
    public JAXBElement<net.ihe.gazelle.medication2.SXCMTS2> createAD2UseablePeriod(net.ihe.gazelle.medication2.SXCMTS2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.SXCMTS2>(_AD2UseablePeriod_QNAME, net.ihe.gazelle.medication2.SXCMTS2.class, AD2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link TEL2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "reference", scope = ED2.class)
    public JAXBElement<net.ihe.gazelle.medication2.TEL2> createED2Reference(net.ihe.gazelle.medication2.TEL2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.TEL2>(_ED2Reference_QNAME, net.ihe.gazelle.medication2.TEL2.class, ED2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link Thumbnail2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "thumbnail", scope = ED2.class)
    public JAXBElement<net.ihe.gazelle.medication2.Thumbnail2> createED2Thumbnail(net.ihe.gazelle.medication2.Thumbnail2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.Thumbnail2>(_ED2Thumbnail_QNAME, net.ihe.gazelle.medication2.Thumbnail2.class, ED2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnDelimiter2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "delimiter", scope = EN2.class)
    public JAXBElement<net.ihe.gazelle.medication2.EnDelimiter2> createEN2Delimiter(net.ihe.gazelle.medication2.EnDelimiter2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.EnDelimiter2>(_EN2Delimiter_QNAME, net.ihe.gazelle.medication2.EnDelimiter2.class, EN2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnFamily2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "family", scope = EN2.class)
    public JAXBElement<net.ihe.gazelle.medication2.EnFamily2> createEN2Family(net.ihe.gazelle.medication2.EnFamily2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.EnFamily2>(_EN2Family_QNAME, net.ihe.gazelle.medication2.EnFamily2.class, EN2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnGiven2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "given", scope = EN2.class)
    public JAXBElement<net.ihe.gazelle.medication2.EnGiven2> createEN2Given(net.ihe.gazelle.medication2.EnGiven2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.EnGiven2>(_EN2Given_QNAME, net.ihe.gazelle.medication2.EnGiven2.class, EN2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnPrefix2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "prefix", scope = EN2.class)
    public JAXBElement<net.ihe.gazelle.medication2.EnPrefix2> createEN2Prefix(net.ihe.gazelle.medication2.EnPrefix2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.EnPrefix2>(_EN2Prefix_QNAME, net.ihe.gazelle.medication2.EnPrefix2.class, EN2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnSuffix2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "suffix", scope = EN2.class)
    public JAXBElement<net.ihe.gazelle.medication2.EnSuffix2> createEN2Suffix(net.ihe.gazelle.medication2.EnSuffix2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.EnSuffix2>(_EN2Suffix_QNAME, net.ihe.gazelle.medication2.EnSuffix2.class, EN2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link IVLTS2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "validTime", scope = EN2.class)
    public JAXBElement<net.ihe.gazelle.medication2.IVLTS2> createEN2ValidTime(net.ihe.gazelle.medication2.IVLTS2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.IVLTS2>(_EN2ValidTime_QNAME, net.ihe.gazelle.medication2.IVLTS2.class, EN2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "xmlns:prefix", scope = DocumentRoot2.class)
    public JAXBElement<String> createDocumentRoot2XMLNSPrefixMap(String value) {
        return new JAXBElement<String>(_DocumentRoot2XMLNSPrefixMap_QNAME, String.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "xsi:schemaLocation", scope = DocumentRoot2.class)
    public JAXBElement<String> createDocumentRoot2XSISchemaLocation(String value) {
        return new JAXBElement<String>(_DocumentRoot2XSISchemaLocation_QNAME, String.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVContent2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "asContent", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVContent2> createDocumentRoot2AsContent(net.ihe.gazelle.medication2.COCTMT230100UVContent2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVContent2>(_DocumentRoot2AsContent_QNAME, net.ihe.gazelle.medication2.COCTMT230100UVContent2.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVDistributedProduct2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "asDistributedProduct", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVDistributedProduct2> createDocumentRoot2AsDistributedProduct(net.ihe.gazelle.medication2.COCTMT230100UVDistributedProduct2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVDistributedProduct2>(_DocumentRoot2AsDistributedProduct_QNAME, net.ihe.gazelle.medication2.COCTMT230100UVDistributedProduct2.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVMedicineManufacturer2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "asMedicineManufacturer", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVMedicineManufacturer2> createDocumentRoot2AsMedicineManufacturer(net.ihe.gazelle.medication2.COCTMT230100UVMedicineManufacturer2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVMedicineManufacturer2>(_DocumentRoot2AsMedicineManufacturer_QNAME, net.ihe.gazelle.medication2.COCTMT230100UVMedicineManufacturer2.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVSpecializedKind2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "asSpecializedKind", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVSpecializedKind2> createDocumentRoot2AsSpecializedKind(net.ihe.gazelle.medication2.COCTMT230100UVSpecializedKind2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVSpecializedKind2>(_DocumentRoot2AsSpecializedKind_QNAME, net.ihe.gazelle.medication2.COCTMT230100UVSpecializedKind2.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link ED2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "desc", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.ED2> createDocumentRoot2Desc(net.ihe.gazelle.medication2.ED2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.ED2>(_DocumentRoot2Desc_QNAME, net.ihe.gazelle.medication2.ED2.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link IVLTS2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "expirationTime", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.IVLTS2> createDocumentRoot2ExpirationTime(net.ihe.gazelle.medication2.IVLTS2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.IVLTS2>(_DocumentRoot2ExpirationTime_QNAME, net.ihe.gazelle.medication2.IVLTS2.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link CE2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "formCode", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.CE2> createDocumentRoot2FormCode(net.ihe.gazelle.medication2.CE2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.CE2>(_DocumentRoot2FormCode_QNAME, net.ihe.gazelle.medication2.CE2.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link CE2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "handlingCode", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.CE2> createDocumentRoot2HandlingCode(net.ihe.gazelle.medication2.CE2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.CE2>(_DocumentRoot2HandlingCode_QNAME, net.ihe.gazelle.medication2.CE2.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link II2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "id", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.II2> createDocumentRoot2Id(net.ihe.gazelle.medication2.II2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.II2>(_DocumentRoot2Id_QNAME, net.ihe.gazelle.medication2.II2.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVIngredient2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "ingredient", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVIngredient2> createDocumentRoot2Ingredient(net.ihe.gazelle.medication2.COCTMT230100UVIngredient2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVIngredient2>(_DocumentRoot2Ingredient_QNAME, net.ihe.gazelle.medication2.COCTMT230100UVIngredient2.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVMedication2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "medication", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVMedication2> createDocumentRoot2Medication(net.ihe.gazelle.medication2.COCTMT230100UVMedication2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVMedication2>(_DocumentRoot2Medication_QNAME, net.ihe.gazelle.medication2.COCTMT230100UVMedication2.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVPart2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "part", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVPart2> createDocumentRoot2Part(net.ihe.gazelle.medication2.COCTMT230100UVPart2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVPart2>(_DocumentRoot2Part_QNAME, net.ihe.gazelle.medication2.COCTMT230100UVPart2.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link CE2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "riskCode", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.CE2> createDocumentRoot2RiskCode(net.ihe.gazelle.medication2.CE2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.CE2>(_DocumentRoot2RiskCode_QNAME, net.ihe.gazelle.medication2.CE2.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link IVLTS2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "stabilityTime", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.IVLTS2> createDocumentRoot2StabilityTime(net.ihe.gazelle.medication2.IVLTS2 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.IVLTS2>(_DocumentRoot2StabilityTime_QNAME, net.ihe.gazelle.medication2.IVLTS2.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVSubject222 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "subjectOf1", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVSubject222> createDocumentRoot2SubjectOf1(net.ihe.gazelle.medication2.COCTMT230100UVSubject222 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVSubject222>(_DocumentRoot2SubjectOf1_QNAME, net.ihe.gazelle.medication2.COCTMT230100UVSubject222.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVSubject12 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "subjectOf2", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVSubject12> createDocumentRoot2SubjectOf2(net.ihe.gazelle.medication2.COCTMT230100UVSubject12 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVSubject12>(_DocumentRoot2SubjectOf2_QNAME, net.ihe.gazelle.medication2.COCTMT230100UVSubject12.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVSubject222 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "subjectOf3", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVSubject222> createDocumentRoot2SubjectOf3(net.ihe.gazelle.medication2.COCTMT230100UVSubject222 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVSubject222>(_DocumentRoot2SubjectOf3_QNAME, net.ihe.gazelle.medication2.COCTMT230100UVSubject222.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVSubject32 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "subjectOf4", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVSubject32> createDocumentRoot2SubjectOf4(net.ihe.gazelle.medication2.COCTMT230100UVSubject32 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVSubject32>(_DocumentRoot2SubjectOf4_QNAME, net.ihe.gazelle.medication2.COCTMT230100UVSubject32.class, DocumentRoot2.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVSubject72 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "subjectOf5", scope = DocumentRoot2.class)
    public JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVSubject72> createDocumentRoot2SubjectOf5(net.ihe.gazelle.medication2.COCTMT230100UVSubject72 value) {
        return new JAXBElement<net.ihe.gazelle.medication2.COCTMT230100UVSubject72>(_DocumentRoot2SubjectOf5_QNAME, net.ihe.gazelle.medication2.COCTMT230100UVSubject72.class, DocumentRoot2.class, value);
    }

}

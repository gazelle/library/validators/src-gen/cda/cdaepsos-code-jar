/**
 * ParticipationType2.java
 *
 * File generated from the medication2::ParticipationType2 uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration ParticipationType2.
 *
 * 
 */

@XmlType(name = "ParticipationType")
@XmlEnum
@XmlRootElement(name = "ParticipationType2")
public enum ParticipationType2 {
	@XmlEnumValue("CST")
	CST("CST"),
	@XmlEnumValue("RESP")
	RESP("RESP"),
	@XmlEnumValue("ADM")
	ADM("ADM"),
	@XmlEnumValue("ATND")
	ATND("ATND"),
	@XmlEnumValue("CALLBCK")
	CALLBCK("CALLBCK"),
	@XmlEnumValue("CON")
	CON("CON"),
	@XmlEnumValue("DIS")
	DIS("DIS"),
	@XmlEnumValue("ESC")
	ESC("ESC"),
	@XmlEnumValue("REF")
	REF("REF"),
	@XmlEnumValue("IND")
	IND("IND"),
	@XmlEnumValue("BEN")
	BEN("BEN"),
	@XmlEnumValue("COV")
	COV("COV"),
	@XmlEnumValue("HLD")
	HLD("HLD"),
	@XmlEnumValue("RCT")
	RCT("RCT"),
	@XmlEnumValue("RCV")
	RCV("RCV"),
	@XmlEnumValue("AUT")
	AUT("AUT"),
	@XmlEnumValue("ENT")
	ENT("ENT"),
	@XmlEnumValue("INF")
	INF("INF"),
	@XmlEnumValue("WIT")
	WIT("WIT"),
	@XmlEnumValue("IRCP")
	IRCP("IRCP"),
	@XmlEnumValue("NOT")
	NOT("NOT"),
	@XmlEnumValue("PRCP")
	PRCP("PRCP"),
	@XmlEnumValue("REFB")
	REFB("REFB"),
	@XmlEnumValue("REFT")
	REFT("REFT"),
	@XmlEnumValue("TRC")
	TRC("TRC"),
	@XmlEnumValue("PRF")
	PRF("PRF"),
	@XmlEnumValue("DIST")
	DIST("DIST"),
	@XmlEnumValue("PPRF")
	PPRF("PPRF"),
	@XmlEnumValue("SPRF")
	SPRF("SPRF"),
	@XmlEnumValue("DIR")
	DIR("DIR"),
	@XmlEnumValue("BBY")
	BBY("BBY"),
	@XmlEnumValue("CSM")
	CSM("CSM"),
	@XmlEnumValue("DON")
	DON("DON"),
	@XmlEnumValue("PRD")
	PRD("PRD"),
	@XmlEnumValue("DEV")
	DEV("DEV"),
	@XmlEnumValue("NRD")
	NRD("NRD"),
	@XmlEnumValue("RDV")
	RDV("RDV"),
	@XmlEnumValue("SBJ")
	SBJ("SBJ"),
	@XmlEnumValue("SPC")
	SPC("SPC"),
	@XmlEnumValue("LOC")
	LOC("LOC"),
	@XmlEnumValue("DST")
	DST("DST"),
	@XmlEnumValue("ELOC")
	ELOC("ELOC"),
	@XmlEnumValue("ORG")
	ORG("ORG"),
	@XmlEnumValue("RML")
	RML("RML"),
	@XmlEnumValue("VIA")
	VIA("VIA"),
	@XmlEnumValue("VRF")
	VRF("VRF"),
	@XmlEnumValue("AUTHEN")
	AUTHEN("AUTHEN"),
	@XmlEnumValue("LA")
	LA("LA");
	
	private final String value;

    ParticipationType2(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static ParticipationType2 fromValue(String v) {
        for (ParticipationType2 c: ParticipationType2.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}

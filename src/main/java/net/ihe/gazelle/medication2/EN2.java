/**
 * EN2.java
 *
 * File generated from the medication2::EN2 uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import java.util.ArrayList;
import java.util.List;

// End of user code
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


/**
 * Description of the class EN2.
 *
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EN", propOrder = {
	"mixed",
	"use"
})
@XmlRootElement(name = "EN2")
public class EN2 extends net.ihe.gazelle.medication2.ANY2 implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@XmlElementRefs({
			@XmlElementRef(name = "delimiter", namespace = "urn:epsos-org:ep:medication", type = JAXBElement.class),
			@XmlElementRef(name = "family", namespace = "urn:epsos-org:ep:medication", type = JAXBElement.class),
			@XmlElementRef(name = "given", namespace = "urn:epsos-org:ep:medication", type = JAXBElement.class),
			@XmlElementRef(name = "prefix", namespace = "urn:epsos-org:ep:medication", type = JAXBElement.class),
			@XmlElementRef(name = "suffix", namespace = "urn:epsos-org:ep:medication", type = JAXBElement.class),
			@XmlElementRef(name = "validTime", namespace = "urn:epsos-org:ep:medication", type = JAXBElement.class)
		})
		@XmlMixed
	public List<java.io.Serializable> mixed;
	/**
	 *                       A set of codes advising a system or
	                            user which name                      in a set of like names to select
	                            for a given purpose.                      A name without specific use
	                            code might be a default                      name useful for any
	                            purpose, but a name with a specific                      use code would
	                            be preferred for that respective purpose.
	                        .
	 */
	@XmlAttribute(name = "use")
	public java.lang.String use;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private org.w3c.dom.Node _xmlNodePresentation;
	
	
	/**
	 * Return mixed.
	 * @return mixed
	 */
	public List<java.io.Serializable> getMixed() {
		if (mixed == null) {
	        mixed = new ArrayList<java.io.Serializable>();
	    }
	    return mixed;
	}
	
	/**
	 * Set a value to attribute mixed.
	 * @param mixed.
	 */
	public void setMixed(List<java.io.Serializable> mixed) {
	    this.mixed = mixed;
	}
	
	
	
	/**
	 * Add a mixed to the mixed collection.
	 * @param mixed_elt Element to add.
	 */
	public void addMixed(java.io.Serializable mixed_elt) {
	    this.getMixed().add(mixed_elt);
	}
	
	/**
	 * Remove a mixed to the mixed collection.
	 * @param mixed_elt Element to remove
	 */
	public void removeMixed(java.io.Serializable mixed_elt) {
	    this.getMixed().remove(mixed_elt);
	}
	
	
	/**
	 * Return delimiter.
	 * @return delimiter
	 */
	public List<net.ihe.gazelle.medication2.EnDelimiter2> getDelimiter() {
		List<net.ihe.gazelle.medication2.EnDelimiter2> delimiter_el = new ArrayList<net.ihe.gazelle.medication2.EnDelimiter2>();
			for(java.io.Serializable obj : this.getMixed()){
			if (obj instanceof javax.xml.bind.JAXBElement){
				if (((javax.xml.bind.JAXBElement<?>)obj).getName().getLocalPart().equals("delimiter")){
					delimiter_el.add( (net.ihe.gazelle.medication2.EnDelimiter2) ((javax.xml.bind.JAXBElement<?>)obj).getValue() );
				}
			}
		} 

	    return delimiter_el;
	}
	
	public void addDelimiter(net.ihe.gazelle.medication2.EnDelimiter2 obj){
		ObjectFactory of = new ObjectFactory();
		JAXBElement<net.ihe.gazelle.medication2.EnDelimiter2> jobj = of.createEN2Delimiter(obj);
		this.getMixed().add(jobj);
	}
	
	/**
	 * Return family.
	 * @return family
	 */
	public List<net.ihe.gazelle.medication2.EnFamily2> getFamily() {
		List<net.ihe.gazelle.medication2.EnFamily2> family_el = new ArrayList<net.ihe.gazelle.medication2.EnFamily2>();
			for(java.io.Serializable obj : this.getMixed()){
			if (obj instanceof javax.xml.bind.JAXBElement){
				if (((javax.xml.bind.JAXBElement<?>)obj).getName().getLocalPart().equals("family")){
					family_el.add( (net.ihe.gazelle.medication2.EnFamily2) ((javax.xml.bind.JAXBElement<?>)obj).getValue() );
				}
			}
		} 

	    return family_el;
	}
	
	public void addFamily(net.ihe.gazelle.medication2.EnFamily2 obj){
		ObjectFactory of = new ObjectFactory();
		JAXBElement<net.ihe.gazelle.medication2.EnFamily2> jobj = of.createEN2Family(obj);
		this.getMixed().add(jobj);
	}
	
	/**
	 * Return given.
	 * @return given
	 */
	public List<net.ihe.gazelle.medication2.EnGiven2> getGiven() {
		List<net.ihe.gazelle.medication2.EnGiven2> given_el = new ArrayList<net.ihe.gazelle.medication2.EnGiven2>();
			for(java.io.Serializable obj : this.getMixed()){
			if (obj instanceof javax.xml.bind.JAXBElement){
				if (((javax.xml.bind.JAXBElement<?>)obj).getName().getLocalPart().equals("given")){
					given_el.add( (net.ihe.gazelle.medication2.EnGiven2) ((javax.xml.bind.JAXBElement<?>)obj).getValue() );
				}
			}
		} 

	    return given_el;
	}
	
	public void addGiven(net.ihe.gazelle.medication2.EnGiven2 obj){
		ObjectFactory of = new ObjectFactory();
		JAXBElement<net.ihe.gazelle.medication2.EnGiven2> jobj = of.createEN2Given(obj);
		this.getMixed().add(jobj);
	}
	
	/**
	 * Return prefix.
	 * @return prefix
	 */
	public List<net.ihe.gazelle.medication2.EnPrefix2> getPrefix() {
		List<net.ihe.gazelle.medication2.EnPrefix2> prefix_el = new ArrayList<net.ihe.gazelle.medication2.EnPrefix2>();
			for(java.io.Serializable obj : this.getMixed()){
			if (obj instanceof javax.xml.bind.JAXBElement){
				if (((javax.xml.bind.JAXBElement<?>)obj).getName().getLocalPart().equals("prefix")){
					prefix_el.add( (net.ihe.gazelle.medication2.EnPrefix2) ((javax.xml.bind.JAXBElement<?>)obj).getValue() );
				}
			}
		} 

	    return prefix_el;
	}
	
	public void addPrefix(net.ihe.gazelle.medication2.EnPrefix2 obj){
		ObjectFactory of = new ObjectFactory();
		JAXBElement<net.ihe.gazelle.medication2.EnPrefix2> jobj = of.createEN2Prefix(obj);
		this.getMixed().add(jobj);
	}
	
	/**
	 * Return suffix.
	 * @return suffix
	 */
	public List<net.ihe.gazelle.medication2.EnSuffix2> getSuffix() {
		List<net.ihe.gazelle.medication2.EnSuffix2> suffix_el = new ArrayList<net.ihe.gazelle.medication2.EnSuffix2>();
			for(java.io.Serializable obj : this.getMixed()){
			if (obj instanceof javax.xml.bind.JAXBElement){
				if (((javax.xml.bind.JAXBElement<?>)obj).getName().getLocalPart().equals("suffix")){
					suffix_el.add( (net.ihe.gazelle.medication2.EnSuffix2) ((javax.xml.bind.JAXBElement<?>)obj).getValue() );
				}
			}
		} 

	    return suffix_el;
	}
	
	public void addSuffix(net.ihe.gazelle.medication2.EnSuffix2 obj){
		ObjectFactory of = new ObjectFactory();
		JAXBElement<net.ihe.gazelle.medication2.EnSuffix2> jobj = of.createEN2Suffix(obj);
		this.getMixed().add(jobj);
	}
	
	/**
	 * Return validTime.
	 * @return validTime
	 */
	public net.ihe.gazelle.medication2.IVLTS2 getValidTime() {
		net.ihe.gazelle.medication2.IVLTS2 validTime_el = null;
			for(java.io.Serializable obj : this.getMixed()){
			if (obj instanceof javax.xml.bind.JAXBElement){
				if (((javax.xml.bind.JAXBElement<?>)obj).getName().getLocalPart().equals("validTime")){
					validTime_el = (net.ihe.gazelle.medication2.IVLTS2) ((javax.xml.bind.JAXBElement<?>)obj).getValue();
					return validTime_el;
				}
			}
		} 

	    return validTime_el;
	}
	
	public void addValidTime(net.ihe.gazelle.medication2.IVLTS2 obj){
		ObjectFactory of = new ObjectFactory();
		JAXBElement<net.ihe.gazelle.medication2.IVLTS2> jobj = of.createEN2ValidTime(obj);
		this.getMixed().add(jobj);
	}
	
	/**
	 * Return use.
	 * @return use :                       A set of codes advising a system or
	                            user which name                      in a set of like names to select
	                            for a given purpose.                      A name without specific use
	                            code might be a default                      name useful for any
	                            purpose, but a name with a specific                      use code would
	                            be preferred for that respective purpose.
	                        
	 */
	public java.lang.String getUse() {
	    return use;
	}
	
	/**
	 * Set a value to attribute use.
	 * @param use :                       A set of codes advising a system or
	                            user which name                      in a set of like names to select
	                            for a given purpose.                      A name without specific use
	                            code might be a default                      name useful for any
	                            purpose, but a name with a specific                      use code would
	                            be preferred for that respective purpose.
	                        .
	 */
	public void setUse(java.lang.String use) {
	    this.use = use;
	}
	
	
	
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.medication2");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:epsos-org:ep:medication", "EN2").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	public List<String> getListStringValues(){
		List<String> res = new ArrayList<String>();
		for(java.io.Serializable obj : this.getMixed()){
			if (obj instanceof String){
				res.add((String)obj);
			}
		} 
		return res;
	}
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(EN2 eN2, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (eN2 != null){
   			net.ihe.gazelle.medication2.ANY2.validateByModule(eN2, _location, cvm, diagnostic);
			{
				int i = 0;
				for (net.ihe.gazelle.medication2.EnDelimiter2 delimiter: eN2.getDelimiter()){
					net.ihe.gazelle.medication2.EnDelimiter2.validateByModule(delimiter, _location + "/delimiter[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			{
				int i = 0;
				for (net.ihe.gazelle.medication2.EnFamily2 family: eN2.getFamily()){
					net.ihe.gazelle.medication2.EnFamily2.validateByModule(family, _location + "/family[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			{
				int i = 0;
				for (net.ihe.gazelle.medication2.EnGiven2 given: eN2.getGiven()){
					net.ihe.gazelle.medication2.EnGiven2.validateByModule(given, _location + "/given[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			{
				int i = 0;
				for (net.ihe.gazelle.medication2.EnPrefix2 prefix: eN2.getPrefix()){
					net.ihe.gazelle.medication2.EnPrefix2.validateByModule(prefix, _location + "/prefix[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			{
				int i = 0;
				for (net.ihe.gazelle.medication2.EnSuffix2 suffix: eN2.getSuffix()){
					net.ihe.gazelle.medication2.EnSuffix2.validateByModule(suffix, _location + "/suffix[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			net.ihe.gazelle.medication2.IVLTS2.validateByModule(eN2.getValidTime(), _location + "/validTime", cvm, diagnostic);
    	}
    }

}

/**
 * ActRelationshipObjective2.java
 *
 * File generated from the medication2::ActRelationshipObjective2 uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration ActRelationshipObjective2.
 *
 * 
 */

@XmlType(name = "ActRelationshipObjective")
@XmlEnum
@XmlRootElement(name = "ActRelationshipObjective2")
public enum ActRelationshipObjective2 {
	@XmlEnumValue("OBJC")
	OBJC("OBJC"),
	@XmlEnumValue("OBJF")
	OBJF("OBJF");
	
	private final String value;

    ActRelationshipObjective2(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static ActRelationshipObjective2 fromValue(String v) {
        for (ActRelationshipObjective2 c: ActRelationshipObjective2.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}

/**
 * PQ2.java
 *
 * File generated from the medication2::PQ2 uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import java.util.ArrayList;
import java.util.List;

// End of user code
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


/**
 * Description of the class PQ2.
 *
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PQ", propOrder = {
	"translation",
	"unit",
	"value"
})
@XmlRootElement(name = "PQ2")
public class PQ2 extends net.ihe.gazelle.medication2.QTY2 implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	/**
	 *                          An alternative representation
	                                of the same physical                         quantity expressed in a
	                                different unit, of a different                         unit code
	                                system and possibly with a different value.
	                            .
	 */
	@XmlElement(name = "translation", namespace = "urn:epsos-org:ep:medication")
	public List<net.ihe.gazelle.medication2.PQR2> translation;
	/**
	 *                       The unit of measure specified in the
	                            Unified Code for                      Units of Measure (UCUM)
	                            [http://aurora.rg.iupui.edu/UCUM].                   .
	 */
	@XmlAttribute(name = "unit")
	public String unit;
	/**
	 *                       The magnitude of the quantity
	                            measured in terms of                      the unit.
	                        .
	 */
	@XmlAttribute(name = "value")
	public java.lang.String value;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private org.w3c.dom.Node _xmlNodePresentation;
	
	
	/**
	 * Return translation.
	 * @return translation :                          An alternative representation
	                                of the same physical                         quantity expressed in a
	                                different unit, of a different                         unit code
	                                system and possibly with a different value.
	                            
	 */
	public List<net.ihe.gazelle.medication2.PQR2> getTranslation() {
		if (translation == null) {
	        translation = new ArrayList<net.ihe.gazelle.medication2.PQR2>();
	    }
	    return translation;
	}
	
	/**
	 * Set a value to attribute translation.
	 * @param translation :                          An alternative representation
	                                of the same physical                         quantity expressed in a
	                                different unit, of a different                         unit code
	                                system and possibly with a different value.
	                            .
	 */
	public void setTranslation(List<net.ihe.gazelle.medication2.PQR2> translation) {
	    this.translation = translation;
	}
	
	
	
	/**
	 * Add a translation to the translation collection.
	 * @param translation_elt Element to add.
	 */
	public void addTranslation(net.ihe.gazelle.medication2.PQR2 translation_elt) {
	    this.getTranslation().add(translation_elt);
	}
	
	/**
	 * Remove a translation to the translation collection.
	 * @param translation_elt Element to remove
	 */
	public void removeTranslation(net.ihe.gazelle.medication2.PQR2 translation_elt) {
	    this.getTranslation().remove(translation_elt);
	}
	
	/**
	 * Return unit.
	 * @return unit :                       The unit of measure specified in the
	                            Unified Code for                      Units of Measure (UCUM)
	                            [http://aurora.rg.iupui.edu/UCUM].                   
	 */
	public String getUnit() {
	    return unit;
	}
	
	/**
	 * Set a value to attribute unit.
	 * @param unit :                       The unit of measure specified in the
	                            Unified Code for                      Units of Measure (UCUM)
	                            [http://aurora.rg.iupui.edu/UCUM].                   .
	 */
	public void setUnit(String unit) {
	    this.unit = unit;
	}
	
	
	
	
	/**
	 * Return value.
	 * @return value :                       The magnitude of the quantity
	                            measured in terms of                      the unit.
	                        
	 */
	public java.lang.String getValue() {
	    return value;
	}
	
	/**
	 * Set a value to attribute value.
	 * @param value :                       The magnitude of the quantity
	                            measured in terms of                      the unit.
	                        .
	 */
	public void setValue(java.lang.String value) {
	    this.value = value;
	}
	
	
	
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.medication2");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:epsos-org:ep:medication", "PQ2").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(PQ2 pQ2, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (pQ2 != null){
   			net.ihe.gazelle.medication2.QTY2.validateByModule(pQ2, _location, cvm, diagnostic);
			{
				int i = 0;
				for (net.ihe.gazelle.medication2.PQR2 translation: pQ2.getTranslation()){
					net.ihe.gazelle.medication2.PQR2.validateByModule(translation, _location + "/translation[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
    	}
    }

}

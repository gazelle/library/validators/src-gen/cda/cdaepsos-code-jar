/**
 * RoleClassIsSpeciesEntity2.java
 *
 * File generated from the medication2::RoleClassIsSpeciesEntity2 uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration RoleClassIsSpeciesEntity2.
 *
 * 
 */

@XmlType(name = "RoleClassIsSpeciesEntity")
@XmlEnum
@XmlRootElement(name = "RoleClassIsSpeciesEntity2")
public enum RoleClassIsSpeciesEntity2 {
	@XmlEnumValue("GEN")
	GEN("GEN"),
	@XmlEnumValue("GRIC")
	GRIC("GRIC");
	
	private final String value;

    RoleClassIsSpeciesEntity2(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static RoleClassIsSpeciesEntity2 fromValue(String v) {
        for (RoleClassIsSpeciesEntity2 c: RoleClassIsSpeciesEntity2.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}

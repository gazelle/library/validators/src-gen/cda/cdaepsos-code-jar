/**
 * ActClassEntry2.java
 *
 * File generated from the medication2::ActClassEntry2 uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration ActClassEntry2.
 *
 * 
 */

@XmlType(name = "ActClassEntry")
@XmlEnum
@XmlRootElement(name = "ActClassEntry2")
public enum ActClassEntry2 {
	@XmlEnumValue("ENTRY")
	ENTRY("ENTRY"),
	@XmlEnumValue("BATTERY")
	BATTERY("BATTERY"),
	@XmlEnumValue("CLUSTER")
	CLUSTER("CLUSTER");
	
	private final String value;

    ActClassEntry2(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static ActClassEntry2 fromValue(String v) {
        for (ActClassEntry2 c: ActClassEntry2.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}

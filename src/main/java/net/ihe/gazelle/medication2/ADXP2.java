/**
 * ADXP2.java
 *
 * File generated from the medication2::ADXP2 uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import java.util.List;

// End of user code
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


/**
 * Description of the class ADXP2.
 *
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ADXP", propOrder = {
	"partType"
})
@XmlRootElement(name = "ADXP2")
public class ADXP2 extends net.ihe.gazelle.medication2.ST12 implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	/**
	 *                       Specifies whether an address part
	                            names the street,                      city, country, postal code, post
	                            box, etc. If the type                      is NULL the address part is
	                            unclassified and would                      simply appear on an address
	                            label as is.                   .
	 */
	@XmlAttribute(name = "partType")
	public net.ihe.gazelle.medication2.AddressPartType2 partType;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private org.w3c.dom.Node _xmlNodePresentation;
	
	
	/**
	 * Return partType.
	 * @return partType :                       Specifies whether an address part
	                            names the street,                      city, country, postal code, post
	                            box, etc. If the type                      is NULL the address part is
	                            unclassified and would                      simply appear on an address
	                            label as is.                   
	 */
	public net.ihe.gazelle.medication2.AddressPartType2 getPartType() {
	    return partType;
	}
	
	/**
	 * Set a value to attribute partType.
	 * @param partType :                       Specifies whether an address part
	                            names the street,                      city, country, postal code, post
	                            box, etc. If the type                      is NULL the address part is
	                            unclassified and would                      simply appear on an address
	                            label as is.                   .
	 */
	public void setPartType(net.ihe.gazelle.medication2.AddressPartType2 partType) {
	    this.partType = partType;
	}
	
	
	
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.medication2");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:epsos-org:ep:medication", "ADXP2").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(ADXP2 aDXP2, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (aDXP2 != null){
   			net.ihe.gazelle.medication2.ST12.validateByModule(aDXP2, _location, cvm, diagnostic);
    	}
    }

}

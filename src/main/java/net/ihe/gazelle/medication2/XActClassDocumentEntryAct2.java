/**
 * XActClassDocumentEntryAct2.java
 *
 * File generated from the medication2::XActClassDocumentEntryAct2 uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration XActClassDocumentEntryAct2.
 *
 * 
 */

@XmlType(name = "XActClassDocumentEntryAct")
@XmlEnum
@XmlRootElement(name = "XActClassDocumentEntryAct2")
public enum XActClassDocumentEntryAct2 {
	@XmlEnumValue("ACT")
	ACT("ACT"),
	@XmlEnumValue("ACCM")
	ACCM("ACCM"),
	@XmlEnumValue("CONS")
	CONS("CONS"),
	@XmlEnumValue("CTTEVENT")
	CTTEVENT("CTTEVENT"),
	@XmlEnumValue("INC")
	INC("INC"),
	@XmlEnumValue("INFRM")
	INFRM("INFRM"),
	@XmlEnumValue("PCPR")
	PCPR("PCPR"),
	@XmlEnumValue("REG")
	REG("REG"),
	@XmlEnumValue("SPCTRT")
	SPCTRT("SPCTRT");
	
	private final String value;

    XActClassDocumentEntryAct2(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static XActClassDocumentEntryAct2 fromValue(String v) {
        for (XActClassDocumentEntryAct2 c: XActClassDocumentEntryAct2.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}

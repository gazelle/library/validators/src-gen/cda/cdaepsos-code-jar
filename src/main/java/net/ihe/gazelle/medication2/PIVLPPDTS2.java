/**
 * PIVLPPDTS2.java
 *
 * File generated from the medication2::PIVLPPDTS2 uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import java.util.List;

// End of user code
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


/**
 * Description of the class PIVLPPDTS2.
 *
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PIVL_PPD_TS", propOrder = {
	"phase",
	"period",
	"alignment",
	"institutionSpecified"
})
@XmlRootElement(name = "PIVL_PPD_TS")
public class PIVLPPDTS2 extends net.ihe.gazelle.medication2.SXCMPPDTS2 implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	/**
	 *                          A prototype of the repeating
	                                interval specifying the                         duration of each
	                                occurrence and anchors the periodic                         interval
	                                sequence at a certain point in time.
	                            .
	 */
	@XmlElement(name = "phase", namespace = "urn:epsos-org:ep:medication")
	public net.ihe.gazelle.medication2.IVLPPDTS2 phase;
	/**
	 *                          A time duration specifying a
	                                reciprocal measure of                         the frequency at which
	                                the periodic interval repeats.
	                            .
	 */
	@XmlElement(name = "period", namespace = "urn:epsos-org:ep:medication")
	public net.ihe.gazelle.medication2.PPDPQ2 period;
	/**
	 *                       Specifies if and how the repetitions
	                            are aligned to                      the cycles of the underlying
	                            calendar (e.g., to                      distinguish every 30 days from
	                            "the 5th of every                      month".) A non-aligned periodic
	                            interval recurs                      independently from the calendar. An
	                            aligned periodic                      interval is synchronized with the
	                            calendar.                   .
	 */
	@XmlAttribute(name = "alignment")
	public net.ihe.gazelle.medication2.CalendarCycle2 alignment;
	/**
	 *                       Indicates whether the exact timing
	                            is up to the party                      executing the schedule (e.g., to
	                            distinguish "every 8                      hours" from "3 times a day".)
	                        .
	 */
	@XmlAttribute(name = "institutionSpecified")
	public java.lang.Boolean institutionSpecified;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private org.w3c.dom.Node _xmlNodePresentation;
	
	
	/**
	 * Return phase.
	 * @return phase :                          A prototype of the repeating
	                                interval specifying the                         duration of each
	                                occurrence and anchors the periodic                         interval
	                                sequence at a certain point in time.
	                            
	 */
	public net.ihe.gazelle.medication2.IVLPPDTS2 getPhase() {
	    return phase;
	}
	
	/**
	 * Set a value to attribute phase.
	 * @param phase :                          A prototype of the repeating
	                                interval specifying the                         duration of each
	                                occurrence and anchors the periodic                         interval
	                                sequence at a certain point in time.
	                            .
	 */
	public void setPhase(net.ihe.gazelle.medication2.IVLPPDTS2 phase) {
	    this.phase = phase;
	}
	
	
	
	
	/**
	 * Return period.
	 * @return period :                          A time duration specifying a
	                                reciprocal measure of                         the frequency at which
	                                the periodic interval repeats.
	                            
	 */
	public net.ihe.gazelle.medication2.PPDPQ2 getPeriod() {
	    return period;
	}
	
	/**
	 * Set a value to attribute period.
	 * @param period :                          A time duration specifying a
	                                reciprocal measure of                         the frequency at which
	                                the periodic interval repeats.
	                            .
	 */
	public void setPeriod(net.ihe.gazelle.medication2.PPDPQ2 period) {
	    this.period = period;
	}
	
	
	
	
	/**
	 * Return alignment.
	 * @return alignment :                       Specifies if and how the repetitions
	                            are aligned to                      the cycles of the underlying
	                            calendar (e.g., to                      distinguish every 30 days from
	                            "the 5th of every                      month".) A non-aligned periodic
	                            interval recurs                      independently from the calendar. An
	                            aligned periodic                      interval is synchronized with the
	                            calendar.                   
	 */
	public net.ihe.gazelle.medication2.CalendarCycle2 getAlignment() {
	    return alignment;
	}
	
	/**
	 * Set a value to attribute alignment.
	 * @param alignment :                       Specifies if and how the repetitions
	                            are aligned to                      the cycles of the underlying
	                            calendar (e.g., to                      distinguish every 30 days from
	                            "the 5th of every                      month".) A non-aligned periodic
	                            interval recurs                      independently from the calendar. An
	                            aligned periodic                      interval is synchronized with the
	                            calendar.                   .
	 */
	public void setAlignment(net.ihe.gazelle.medication2.CalendarCycle2 alignment) {
	    this.alignment = alignment;
	}
	
	
	
	
	/**
	 * Return institutionSpecified.
	 * @return institutionSpecified :                       Indicates whether the exact timing
	                            is up to the party                      executing the schedule (e.g., to
	                            distinguish "every 8                      hours" from "3 times a day".)
	                        
	 */
	public java.lang.Boolean getInstitutionSpecified() {
	    return institutionSpecified;
	}
	
	/**
	 * Set a value to attribute institutionSpecified.
	 * @param institutionSpecified :                       Indicates whether the exact timing
	                            is up to the party                      executing the schedule (e.g., to
	                            distinguish "every 8                      hours" from "3 times a day".)
	                        .
	 */
	public void setInstitutionSpecified(java.lang.Boolean institutionSpecified) {
	    this.institutionSpecified = institutionSpecified;
	}
	
	
	
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.medication2");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:epsos-org:ep:medication", "PIVL_PPD_TS").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(PIVLPPDTS2 pIVLPPDTS2, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (pIVLPPDTS2 != null){
   			net.ihe.gazelle.medication2.SXCMPPDTS2.validateByModule(pIVLPPDTS2, _location, cvm, diagnostic);
			net.ihe.gazelle.medication2.IVLPPDTS2.validateByModule(pIVLPPDTS2.getPhase(), _location + "/phase", cvm, diagnostic);
			net.ihe.gazelle.medication2.PPDPQ2.validateByModule(pIVLPPDTS2.getPeriod(), _location + "/period", cvm, diagnostic);
    	}
    }

}

/**
 * TemporallyPertains2.java
 *
 * File generated from the medication2::TemporallyPertains2 uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration TemporallyPertains2.
 *
 * 
 */

@XmlType(name = "TemporallyPertains")
@XmlEnum
@XmlRootElement(name = "TemporallyPertains2")
public enum TemporallyPertains2 {
	@XmlEnumValue("SAS")
	SAS("SAS");
	
	private final String value;

    TemporallyPertains2(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static TemporallyPertains2 fromValue(String v) {
        for (TemporallyPertains2 c: TemporallyPertains2.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}

/**
 * ED2.java
 *
 * File generated from the medication2::ED2 uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import java.util.List;

// End of user code
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


/**
 * Description of the class ED2.
 *
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ED", propOrder = {
	"compression",
	"integrityCheck",
	"integrityCheckAlgorithm",
	"language",
	"mediaType"
})
@XmlRootElement(name = "ED2")
public class ED2 extends net.ihe.gazelle.medication2.BIN12 implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	/**
	 *                       Indicates whether the raw byte data
	                            is compressed,                      and what compression algorithm was
	                            used.                   .
	 */
	@XmlAttribute(name = "compression")
	public net.ihe.gazelle.medication2.CompressionAlgorithm2 compression;
	/**
	 *                       The integrity check is a short
	                            binary value representing                      a cryptographically
	                            strong checksum that is calculated                      over the binary
	                            data. The purpose of this property, when
	                            communicated with a reference is for anyone to validate
	                            later whether the reference still resolved to the same
	                            data that the reference resolved to when the encapsulated
	                            data value with reference was created.
	                        .
	 */
	@XmlAttribute(name = "integrityCheck")
	public java.lang.String integrityCheck;
	/**
	 *                       Specifies the algorithm used to
	                            compute the                      integrityCheck value.
	                        .
	 */
	@XmlAttribute(name = "integrityCheckAlgorithm")
	public net.ihe.gazelle.medication2.IntegrityCheckAlgorithm2 integrityCheckAlgorithm;
	/**
	 *                       For character based information the
	                            language property                      specifies the human language of
	                            the text.                   .
	 */
	@XmlAttribute(name = "language")
	public String language;
	/**
	 *                       Identifies the type of the
	                            encapsulated data and                      identifies a method to
	                            interpret or render the data.                   .
	 */
	@XmlAttribute(name = "mediaType")
	public String mediaType;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private org.w3c.dom.Node _xmlNodePresentation;
	
	
	/**
	 * Return reference.
	 * @return reference
	 */
	public net.ihe.gazelle.medication2.TEL2 getReference() {
		net.ihe.gazelle.medication2.TEL2 reference_el = null;
		
	    return reference_el;
	}
	
	
	/**
	 * Return thumbnail.
	 * @return thumbnail
	 */
	public net.ihe.gazelle.medication2.Thumbnail2 getThumbnail() {
		net.ihe.gazelle.medication2.Thumbnail2 thumbnail_el = null;
		
	    return thumbnail_el;
	}
	
	
	/**
	 * Return compression.
	 * @return compression :                       Indicates whether the raw byte data
	                            is compressed,                      and what compression algorithm was
	                            used.                   
	 */
	public net.ihe.gazelle.medication2.CompressionAlgorithm2 getCompression() {
	    return compression;
	}
	
	/**
	 * Set a value to attribute compression.
	 * @param compression :                       Indicates whether the raw byte data
	                            is compressed,                      and what compression algorithm was
	                            used.                   .
	 */
	public void setCompression(net.ihe.gazelle.medication2.CompressionAlgorithm2 compression) {
	    this.compression = compression;
	}
	
	
	
	
	/**
	 * Return integrityCheck.
	 * @return integrityCheck :                       The integrity check is a short
	                            binary value representing                      a cryptographically
	                            strong checksum that is calculated                      over the binary
	                            data. The purpose of this property, when
	                            communicated with a reference is for anyone to validate
	                            later whether the reference still resolved to the same
	                            data that the reference resolved to when the encapsulated
	                            data value with reference was created.
	                        
	 */
	public java.lang.String getIntegrityCheck() {
	    return integrityCheck;
	}
	
	/**
	 * Set a value to attribute integrityCheck.
	 * @param integrityCheck :                       The integrity check is a short
	                            binary value representing                      a cryptographically
	                            strong checksum that is calculated                      over the binary
	                            data. The purpose of this property, when
	                            communicated with a reference is for anyone to validate
	                            later whether the reference still resolved to the same
	                            data that the reference resolved to when the encapsulated
	                            data value with reference was created.
	                        .
	 */
	public void setIntegrityCheck(java.lang.String integrityCheck) {
	    this.integrityCheck = integrityCheck;
	}
	
	
	
	
	/**
	 * Return integrityCheckAlgorithm.
	 * @return integrityCheckAlgorithm :                       Specifies the algorithm used to
	                            compute the                      integrityCheck value.
	                        
	 */
	public net.ihe.gazelle.medication2.IntegrityCheckAlgorithm2 getIntegrityCheckAlgorithm() {
	    return integrityCheckAlgorithm;
	}
	
	/**
	 * Set a value to attribute integrityCheckAlgorithm.
	 * @param integrityCheckAlgorithm :                       Specifies the algorithm used to
	                            compute the                      integrityCheck value.
	                        .
	 */
	public void setIntegrityCheckAlgorithm(net.ihe.gazelle.medication2.IntegrityCheckAlgorithm2 integrityCheckAlgorithm) {
	    this.integrityCheckAlgorithm = integrityCheckAlgorithm;
	}
	
	
	
	
	/**
	 * Return language.
	 * @return language :                       For character based information the
	                            language property                      specifies the human language of
	                            the text.                   
	 */
	public String getLanguage() {
	    return language;
	}
	
	/**
	 * Set a value to attribute language.
	 * @param language :                       For character based information the
	                            language property                      specifies the human language of
	                            the text.                   .
	 */
	public void setLanguage(String language) {
	    this.language = language;
	}
	
	
	
	
	/**
	 * Return mediaType.
	 * @return mediaType :                       Identifies the type of the
	                            encapsulated data and                      identifies a method to
	                            interpret or render the data.                   
	 */
	public String getMediaType() {
	    return mediaType;
	}
	
	/**
	 * Set a value to attribute mediaType.
	 * @param mediaType :                       Identifies the type of the
	                            encapsulated data and                      identifies a method to
	                            interpret or render the data.                   .
	 */
	public void setMediaType(String mediaType) {
	    this.mediaType = mediaType;
	}
	
	
	
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.medication2");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:epsos-org:ep:medication", "ED2").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(ED2 eD2, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (eD2 != null){
   			net.ihe.gazelle.medication2.BIN12.validateByModule(eD2, _location, cvm, diagnostic);
			net.ihe.gazelle.medication2.TEL2.validateByModule(eD2.getReference(), _location + "/reference", cvm, diagnostic);
			net.ihe.gazelle.medication2.Thumbnail2.validateByModule(eD2.getThumbnail(), _location + "/thumbnail", cvm, diagnostic);
    	}
    }

}

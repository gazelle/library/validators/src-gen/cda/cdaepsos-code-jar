/**
 * RoleClassAgent2.java
 *
 * File generated from the medication2::RoleClassAgent2 uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration RoleClassAgent2.
 *
 * 
 */

@XmlType(name = "RoleClassAgent")
@XmlEnum
@XmlRootElement(name = "RoleClassAgent2")
public enum RoleClassAgent2 {
	@XmlEnumValue("AGNT")
	AGNT("AGNT"),
	@XmlEnumValue("GUARD")
	GUARD("GUARD"),
	@XmlEnumValue("ASSIGNED")
	ASSIGNED("ASSIGNED"),
	@XmlEnumValue("COMPAR")
	COMPAR("COMPAR"),
	@XmlEnumValue("SGNOFF")
	SGNOFF("SGNOFF"),
	@XmlEnumValue("CON")
	CON("CON"),
	@XmlEnumValue("ECON")
	ECON("ECON"),
	@XmlEnumValue("NOK")
	NOK("NOK");
	
	private final String value;

    RoleClassAgent2(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static RoleClassAgent2 fromValue(String v) {
        for (RoleClassAgent2 c: RoleClassAgent2.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}

/**
 * RoleClassTerritoryOfAuthority2.java
 *
 * File generated from the medication2::RoleClassTerritoryOfAuthority2 uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration RoleClassTerritoryOfAuthority2.
 *
 */

@XmlType(name = "RoleClassTerritoryOfAuthority")
@XmlEnum
@XmlRootElement(name = "RoleClassTerritoryOfAuthority2")
public enum RoleClassTerritoryOfAuthority2 {
	@XmlEnumValue("TERR")
	TERR("TERR");
	
	private final String value;

    RoleClassTerritoryOfAuthority2(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static RoleClassTerritoryOfAuthority2 fromValue(String v) {
        for (RoleClassTerritoryOfAuthority2 c: RoleClassTerritoryOfAuthority2.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}

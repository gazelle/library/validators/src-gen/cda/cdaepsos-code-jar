/**
 * TelecommunicationAddressUse2.java
 *
 * File generated from the medication2::TelecommunicationAddressUse2 uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration TelecommunicationAddressUse2.
 *
 * 
 */

@XmlType(name = "TelecommunicationAddressUse")
@XmlEnum
@XmlRootElement(name = "TelecommunicationAddressUse2")
public enum TelecommunicationAddressUse2 {
	@XmlEnumValue("AS")
	AS("AS"),
	@XmlEnumValue("EC")
	EC("EC"),
	@XmlEnumValue("MC")
	MC("MC"),
	@XmlEnumValue("PG")
	PG("PG"),
	@XmlEnumValue("BAD")
	BAD("BAD"),
	@XmlEnumValue("TMP")
	TMP("TMP"),
	@XmlEnumValue("H")
	H("H"),
	@XmlEnumValue("HP")
	HP("HP"),
	@XmlEnumValue("HV")
	HV("HV"),
	@XmlEnumValue("WP")
	WP("WP"),
	@XmlEnumValue("DIR")
	DIR("DIR"),
	@XmlEnumValue("PUB")
	PUB("PUB");
	
	private final String value;

    TelecommunicationAddressUse2(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static TelecommunicationAddressUse2 fromValue(String v) {
        for (TelecommunicationAddressUse2 c: TelecommunicationAddressUse2.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}

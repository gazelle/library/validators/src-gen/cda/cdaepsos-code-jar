/**
 * ContextControlOverriding2.java
 *
 * File generated from the medication2::ContextControlOverriding2 uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration ContextControlOverriding2.
 *
 * 
 */

@XmlType(name = "ContextControlOverriding")
@XmlEnum
@XmlRootElement(name = "ContextControlOverriding2")
public enum ContextControlOverriding2 {
	@XmlEnumValue("ON")
	ON2("ON"),
	@XmlEnumValue("OP")
	OP("OP");
	
	private final String value;

    ContextControlOverriding2(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static ContextControlOverriding2 fromValue(String v) {
        for (ContextControlOverriding2 c: ContextControlOverriding2.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}

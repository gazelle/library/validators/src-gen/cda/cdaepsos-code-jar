/**
 * VideoMediaType2.java
 *
 * File generated from the medication2::VideoMediaType2 uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration VideoMediaType2.
 *
 * 
 */

@XmlType(name = "VideoMediaType")
@XmlEnum
@XmlRootElement(name = "VideoMediaType2")
public enum VideoMediaType2 {
	@XmlEnumValue("video/mpeg")
	VIDEOMPEG("video/mpeg"),
	@XmlEnumValue("video/x-avi")
	VIDEOXAVI("video/x-avi");
	
	private final String value;

    VideoMediaType2(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static VideoMediaType2 fromValue(String v) {
        for (VideoMediaType2 c: VideoMediaType2.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}

/**
 * XEncounterParticipant2.java
 *
 * File generated from the medication2::XEncounterParticipant2 uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration XEncounterParticipant2.
 *
 * 
 */

@XmlType(name = "XEncounterParticipant")
@XmlEnum
@XmlRootElement(name = "XEncounterParticipant2")
public enum XEncounterParticipant2 {
	@XmlEnumValue("ADM")
	ADM("ADM"),
	@XmlEnumValue("ATND")
	ATND("ATND"),
	@XmlEnumValue("CON")
	CON("CON"),
	@XmlEnumValue("DIS")
	DIS("DIS"),
	@XmlEnumValue("REF")
	REF("REF");
	
	private final String value;

    XEncounterParticipant2(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static XEncounterParticipant2 fromValue(String v) {
        for (XEncounterParticipant2 c: XEncounterParticipant2.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}

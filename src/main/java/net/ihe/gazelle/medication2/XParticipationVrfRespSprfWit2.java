/**
 * XParticipationVrfRespSprfWit2.java
 *
 * File generated from the medication2::XParticipationVrfRespSprfWit2 uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration XParticipationVrfRespSprfWit2.
 *
 * 
 */

@XmlType(name = "XParticipationVrfRespSprfWit")
@XmlEnum
@XmlRootElement(name = "XParticipationVrfRespSprfWit2")
public enum XParticipationVrfRespSprfWit2 {
	@XmlEnumValue("VRF")
	VRF("VRF"),
	@XmlEnumValue("RESP")
	RESP("RESP"),
	@XmlEnumValue("SPRF")
	SPRF("SPRF"),
	@XmlEnumValue("WIT")
	WIT("WIT");
	
	private final String value;

    XParticipationVrfRespSprfWit2(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static XParticipationVrfRespSprfWit2 fromValue(String v) {
        for (XParticipationVrfRespSprfWit2 c: XParticipationVrfRespSprfWit2.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}

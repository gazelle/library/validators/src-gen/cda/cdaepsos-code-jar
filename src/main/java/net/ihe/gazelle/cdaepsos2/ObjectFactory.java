package net.ihe.gazelle.cdaepsos2;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {}
    
    /**
     * Create an instance of {@link  POCDMT000040ClinicalDocument2}
     * 
     */
    public POCDMT000040ClinicalDocument2 createPOCDMT000040ClinicalDocument2() {
        return new POCDMT000040ClinicalDocument2();
    }

    /**
     * Create an instance of {@link  POCDMT000040Component22}
     * 
     */
    public POCDMT000040Component22 createPOCDMT000040Component22() {
        return new POCDMT000040Component22();
    }

    /**
     * Create an instance of {@link  POCDMT000040StructuredBody2}
     * 
     */
    public POCDMT000040StructuredBody2 createPOCDMT000040StructuredBody2() {
        return new POCDMT000040StructuredBody2();
    }

    /**
     * Create an instance of {@link  POCDMT000040Component32}
     * 
     */
    public POCDMT000040Component32 createPOCDMT000040Component32() {
        return new POCDMT000040Component32();
    }

    /**
     * Create an instance of {@link  POCDMT000040Section2}
     * 
     */
    public POCDMT000040Section2 createPOCDMT000040Section2() {
        return new POCDMT000040Section2();
    }

    /**
     * Create an instance of {@link  POCDMT000040Entry2}
     * 
     */
    public POCDMT000040Entry2 createPOCDMT000040Entry2() {
        return new POCDMT000040Entry2();
    }

    /**
     * Create an instance of {@link  POCDMT000040Act2}
     * 
     */
    public POCDMT000040Act2 createPOCDMT000040Act2() {
        return new POCDMT000040Act2();
    }

    /**
     * Create an instance of {@link  POCDMT000040EntryRelationship2}
     * 
     */
    public POCDMT000040EntryRelationship2 createPOCDMT000040EntryRelationship2() {
        return new POCDMT000040EntryRelationship2();
    }

    /**
     * Create an instance of {@link  POCDMT000040Encounter2}
     * 
     */
    public POCDMT000040Encounter2 createPOCDMT000040Encounter2() {
        return new POCDMT000040Encounter2();
    }

    /**
     * Create an instance of {@link  POCDMT000040Observation2}
     * 
     */
    public POCDMT000040Observation2 createPOCDMT000040Observation2() {
        return new POCDMT000040Observation2();
    }

    /**
     * Create an instance of {@link  POCDMT000040ObservationMedia2}
     * 
     */
    public POCDMT000040ObservationMedia2 createPOCDMT000040ObservationMedia2() {
        return new POCDMT000040ObservationMedia2();
    }

    /**
     * Create an instance of {@link  POCDMT000040Procedure2}
     * 
     */
    public POCDMT000040Procedure2 createPOCDMT000040Procedure2() {
        return new POCDMT000040Procedure2();
    }

    /**
     * Create an instance of {@link  POCDMT000040RegionOfInterest2}
     * 
     */
    public POCDMT000040RegionOfInterest2 createPOCDMT000040RegionOfInterest2() {
        return new POCDMT000040RegionOfInterest2();
    }

    /**
     * Create an instance of {@link  POCDMT000040SubstanceAdministration2}
     * 
     */
    public POCDMT000040SubstanceAdministration2 createPOCDMT000040SubstanceAdministration2() {
        return new POCDMT000040SubstanceAdministration2();
    }

    /**
     * Create an instance of {@link  POCDMT000040Consumable2}
     * 
     */
    public POCDMT000040Consumable2 createPOCDMT000040Consumable2() {
        return new POCDMT000040Consumable2();
    }

    /**
     * Create an instance of {@link  POCDMT000040ManufacturedProduct2}
     * 
     */
    public POCDMT000040ManufacturedProduct2 createPOCDMT000040ManufacturedProduct2() {
        return new POCDMT000040ManufacturedProduct2();
    }

    /**
     * Create an instance of {@link  POCDMT000040Material2}
     * 
     */
    public POCDMT000040Material2 createPOCDMT000040Material2() {
        return new POCDMT000040Material2();
    }

    /**
     * Create an instance of {@link  POCDMT000040Supply2}
     * 
     */
    public POCDMT000040Supply2 createPOCDMT000040Supply2() {
        return new POCDMT000040Supply2();
    }

    /**
     * Create an instance of {@link  POCDMT000040Product2}
     * 
     */
    public POCDMT000040Product2 createPOCDMT000040Product2() {
        return new POCDMT000040Product2();
    }

    /**
     * Create an instance of {@link  POCDMT000040Component52}
     * 
     */
    public POCDMT000040Component52 createPOCDMT000040Component52() {
        return new POCDMT000040Component52();
    }

    /**
     * Create an instance of {@link  POCDMT000040Component42}
     * 
     */
    public POCDMT000040Component42 createPOCDMT000040Component42() {
        return new POCDMT000040Component42();
    }

    /**
     * Create an instance of {@link  POCDMT000040Organizer2}
     * 
     */
    public POCDMT000040Organizer2 createPOCDMT000040Organizer2() {
        return new POCDMT000040Organizer2();
    }




}
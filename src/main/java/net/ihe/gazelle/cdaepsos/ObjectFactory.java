package net.ihe.gazelle.cdaepsos;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {

	private final static QName _DocumentRootXMLNSPrefixMap_QNAME = new QName("", "xmlns:prefix");
	private final static QName _DocumentRootXSISchemaLocation_QNAME = new QName("", "xsi:schemaLocation");
	private final static QName _DocumentRootClinicalDocument_QNAME = new QName("urn:hl7-org:v3", "ClinicalDocument");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {}
    
    /**
     * Create an instance of {@link  DocumentRoot}
     * 
     */
    public DocumentRoot createDocumentRoot() {
        return new DocumentRoot();
    }

    /**
     * Create an instance of {@link  POCDMT000040ClinicalDocument}
     * 
     */
    public POCDMT000040ClinicalDocument createPOCDMT000040ClinicalDocument() {
        return new POCDMT000040ClinicalDocument();
    }

    /**
     * Create an instance of {@link  POCDMT000040Component2}
     * 
     */
    public POCDMT000040Component2 createPOCDMT000040Component2() {
        return new POCDMT000040Component2();
    }

    /**
     * Create an instance of {@link  POCDMT000040StructuredBody}
     * 
     */
    public POCDMT000040StructuredBody createPOCDMT000040StructuredBody() {
        return new POCDMT000040StructuredBody();
    }

    /**
     * Create an instance of {@link  POCDMT000040Component3}
     * 
     */
    public POCDMT000040Component3 createPOCDMT000040Component3() {
        return new POCDMT000040Component3();
    }

    /**
     * Create an instance of {@link  POCDMT000040Section}
     * 
     */
    public POCDMT000040Section createPOCDMT000040Section() {
        return new POCDMT000040Section();
    }

    /**
     * Create an instance of {@link  POCDMT000040Entry}
     * 
     */
    public POCDMT000040Entry createPOCDMT000040Entry() {
        return new POCDMT000040Entry();
    }

    /**
     * Create an instance of {@link  POCDMT000040Act}
     * 
     */
    public POCDMT000040Act createPOCDMT000040Act() {
        return new POCDMT000040Act();
    }

    /**
     * Create an instance of {@link  POCDMT000040EntryRelationship}
     * 
     */
    public POCDMT000040EntryRelationship createPOCDMT000040EntryRelationship() {
        return new POCDMT000040EntryRelationship();
    }

    /**
     * Create an instance of {@link  POCDMT000040Encounter}
     * 
     */
    public POCDMT000040Encounter createPOCDMT000040Encounter() {
        return new POCDMT000040Encounter();
    }

    /**
     * Create an instance of {@link  POCDMT000040Observation}
     * 
     */
    public POCDMT000040Observation createPOCDMT000040Observation() {
        return new POCDMT000040Observation();
    }

    /**
     * Create an instance of {@link  POCDMT000040ObservationMedia}
     * 
     */
    public POCDMT000040ObservationMedia createPOCDMT000040ObservationMedia() {
        return new POCDMT000040ObservationMedia();
    }

    /**
     * Create an instance of {@link  POCDMT000040Procedure}
     * 
     */
    public POCDMT000040Procedure createPOCDMT000040Procedure() {
        return new POCDMT000040Procedure();
    }

    /**
     * Create an instance of {@link  POCDMT000040RegionOfInterest}
     * 
     */
    public POCDMT000040RegionOfInterest createPOCDMT000040RegionOfInterest() {
        return new POCDMT000040RegionOfInterest();
    }

    /**
     * Create an instance of {@link  POCDMT000040SubstanceAdministration}
     * 
     */
    public POCDMT000040SubstanceAdministration createPOCDMT000040SubstanceAdministration() {
        return new POCDMT000040SubstanceAdministration();
    }

    /**
     * Create an instance of {@link  POCDMT000040Consumable}
     * 
     */
    public POCDMT000040Consumable createPOCDMT000040Consumable() {
        return new POCDMT000040Consumable();
    }

    /**
     * Create an instance of {@link  POCDMT000040ManufacturedProduct}
     * 
     */
    public POCDMT000040ManufacturedProduct createPOCDMT000040ManufacturedProduct() {
        return new POCDMT000040ManufacturedProduct();
    }

    /**
     * Create an instance of {@link  POCDMT000040Material}
     * 
     */
    public POCDMT000040Material createPOCDMT000040Material() {
        return new POCDMT000040Material();
    }

    /**
     * Create an instance of {@link  POCDMT000040Supply}
     * 
     */
    public POCDMT000040Supply createPOCDMT000040Supply() {
        return new POCDMT000040Supply();
    }

    /**
     * Create an instance of {@link  POCDMT000040Product}
     * 
     */
    public POCDMT000040Product createPOCDMT000040Product() {
        return new POCDMT000040Product();
    }

    /**
     * Create an instance of {@link  POCDMT000040Component5}
     * 
     */
    public POCDMT000040Component5 createPOCDMT000040Component5() {
        return new POCDMT000040Component5();
    }

    /**
     * Create an instance of {@link  POCDMT000040Component4}
     * 
     */
    public POCDMT000040Component4 createPOCDMT000040Component4() {
        return new POCDMT000040Component4();
    }

    /**
     * Create an instance of {@link  POCDMT000040Organizer}
     * 
     */
    public POCDMT000040Organizer createPOCDMT000040Organizer() {
        return new POCDMT000040Organizer();
    }



	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "xmlns:prefix", scope = DocumentRoot.class)
    public JAXBElement<String> createDocumentRootXMLNSPrefixMap(String value) {
        return new JAXBElement<String>(_DocumentRootXMLNSPrefixMap_QNAME, String.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "xsi:schemaLocation", scope = DocumentRoot.class)
    public JAXBElement<String> createDocumentRootXSISchemaLocation(String value) {
        return new JAXBElement<String>(_DocumentRootXSISchemaLocation_QNAME, String.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link POCDMT000040ClinicalDocument }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "ClinicalDocument", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.cdaepsos.POCDMT000040ClinicalDocument> createDocumentRootClinicalDocument(net.ihe.gazelle.cdaepsos.POCDMT000040ClinicalDocument value) {
        return new JAXBElement<net.ihe.gazelle.cdaepsos.POCDMT000040ClinicalDocument>(_DocumentRootClinicalDocument_QNAME, net.ihe.gazelle.cdaepsos.POCDMT000040ClinicalDocument.class, DocumentRoot.class, value);
    }

}
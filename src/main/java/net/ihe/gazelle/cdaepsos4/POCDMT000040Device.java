/**
 * POCDMT000040Device.java
 *
 * File generated from the cdaepsos3::POCDMT000040Device uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.cdaepsos4;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.ArrayList;
import java.util.List;

// End of user code


/**
 * Description of the class POCDMT000040Device.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "POCD_MT000040.Device", propOrder = {
	"realmCode",
	"typeId",
	"templateId",
	"code",
	"manufacturerModelName",
	"softwareName",
	"classCode",
	"determinerCode",
	"nullFlavor"
})
@XmlRootElement(name = "POCD_MT000040.Device")
public class POCDMT000040Device implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@XmlElement(name = "realmCode", namespace = "urn:hl7-org:v3")
	public List<net.ihe.gazelle.datatypes.CS> realmCode;
	@XmlElement(name = "typeId", namespace = "urn:hl7-org:v3")
	public POCDMT000040InfrastructureRootTypeId typeId;
	@XmlElement(name = "templateId", namespace = "urn:hl7-org:v3")
	public List<net.ihe.gazelle.datatypes.II> templateId;
	@XmlElement(name = "code", namespace = "urn:hl7-org:v3")
	public net.ihe.gazelle.datatypes.CE code;
	@XmlElement(name = "manufacturerModelName", namespace = "urn:hl7-org:v3")
	public net.ihe.gazelle.datatypes.SC manufacturerModelName;
	@XmlElement(name = "softwareName", namespace = "urn:hl7-org:v3")
	public net.ihe.gazelle.datatypes.SC softwareName;
	@XmlAttribute(name = "classCode")
	public net.ihe.gazelle.voc.EntityClassDevice classCode;
	@XmlAttribute(name = "determinerCode")
	public net.ihe.gazelle.voc.EntityDeterminer determinerCode;
	@XmlAttribute(name = "nullFlavor")
	public net.ihe.gazelle.voc.NullFlavor nullFlavor;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private Node _xmlNodePresentation;
	
	
	/**
	 * Return realmCode.
	 * @return realmCode
	 */
	public List<net.ihe.gazelle.datatypes.CS> getRealmCode() {
		if (realmCode == null) {
	        realmCode = new ArrayList<net.ihe.gazelle.datatypes.CS>();
	    }
	    return realmCode;
	}
	
	/**
	 * Set a value to attribute realmCode.
	 * @param realmCode.
	 */
	public void setRealmCode(List<net.ihe.gazelle.datatypes.CS> realmCode) {
	    this.realmCode = realmCode;
	}
	
	
	
	/**
	 * Add a realmCode to the realmCode collection.
	 * @param realmCode_elt Element to add.
	 */
	public void addRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
	    this.getRealmCode().add(realmCode_elt);
	}
	
	/**
	 * Remove a realmCode to the realmCode collection.
	 * @param realmCode_elt Element to remove
	 */
	public void removeRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
	    this.getRealmCode().remove(realmCode_elt);
	}
	
	/**
	 * Return typeId.
	 * @return typeId
	 */
	public POCDMT000040InfrastructureRootTypeId getTypeId() {
	    return typeId;
	}
	
	/**
	 * Set a value to attribute typeId.
	 * @param typeId.
	 */
	public void setTypeId(POCDMT000040InfrastructureRootTypeId typeId) {
	    this.typeId = typeId;
	}
	
	
	
	
	/**
	 * Return templateId.
	 * @return templateId
	 */
	public List<net.ihe.gazelle.datatypes.II> getTemplateId() {
		if (templateId == null) {
	        templateId = new ArrayList<net.ihe.gazelle.datatypes.II>();
	    }
	    return templateId;
	}
	
	/**
	 * Set a value to attribute templateId.
	 * @param templateId.
	 */
	public void setTemplateId(List<net.ihe.gazelle.datatypes.II> templateId) {
	    this.templateId = templateId;
	}
	
	
	
	/**
	 * Add a templateId to the templateId collection.
	 * @param templateId_elt Element to add.
	 */
	public void addTemplateId(net.ihe.gazelle.datatypes.II templateId_elt) {
	    this.getTemplateId().add(templateId_elt);
	}
	
	/**
	 * Remove a templateId to the templateId collection.
	 * @param templateId_elt Element to remove
	 */
	public void removeTemplateId(net.ihe.gazelle.datatypes.II templateId_elt) {
	    this.getTemplateId().remove(templateId_elt);
	}
	
	/**
	 * Return code.
	 * @return code
	 */
	public net.ihe.gazelle.datatypes.CE getCode() {
	    return code;
	}
	
	/**
	 * Set a value to attribute code.
	 * @param code.
	 */
	public void setCode(net.ihe.gazelle.datatypes.CE code) {
	    this.code = code;
	}
	
	
	
	
	/**
	 * Return manufacturerModelName.
	 * @return manufacturerModelName
	 */
	public net.ihe.gazelle.datatypes.SC getManufacturerModelName() {
	    return manufacturerModelName;
	}
	
	/**
	 * Set a value to attribute manufacturerModelName.
	 * @param manufacturerModelName.
	 */
	public void setManufacturerModelName(net.ihe.gazelle.datatypes.SC manufacturerModelName) {
	    this.manufacturerModelName = manufacturerModelName;
	}
	
	
	
	
	/**
	 * Return softwareName.
	 * @return softwareName
	 */
	public net.ihe.gazelle.datatypes.SC getSoftwareName() {
	    return softwareName;
	}
	
	/**
	 * Set a value to attribute softwareName.
	 * @param softwareName.
	 */
	public void setSoftwareName(net.ihe.gazelle.datatypes.SC softwareName) {
	    this.softwareName = softwareName;
	}
	
	
	
	
	/**
	 * Return classCode.
	 * @return classCode
	 */
	public net.ihe.gazelle.voc.EntityClassDevice getClassCode() {
	    return classCode;
	}
	
	/**
	 * Set a value to attribute classCode.
	 * @param classCode.
	 */
	public void setClassCode(net.ihe.gazelle.voc.EntityClassDevice classCode) {
	    this.classCode = classCode;
	}
	
	
	
	
	/**
	 * Return determinerCode.
	 * @return determinerCode
	 */
	public net.ihe.gazelle.voc.EntityDeterminer getDeterminerCode() {
	    return determinerCode;
	}
	
	/**
	 * Set a value to attribute determinerCode.
	 * @param determinerCode.
	 */
	public void setDeterminerCode(net.ihe.gazelle.voc.EntityDeterminer determinerCode) {
	    this.determinerCode = determinerCode;
	}
	
	
	
	
	/**
	 * Return nullFlavor.
	 * @return nullFlavor
	 */
	public net.ihe.gazelle.voc.NullFlavor getNullFlavor() {
	    return nullFlavor;
	}
	
	/**
	 * Set a value to attribute nullFlavor.
	 * @param nullFlavor.
	 */
	public void setNullFlavor(net.ihe.gazelle.voc.NullFlavor nullFlavor) {
	    this.nullFlavor = nullFlavor;
	}
	
	
	
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.cdaepsos4");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:hl7-org:v3", "POCD_MT000040.Device").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(POCDMT000040Device pOCDMT000040Device, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (pOCDMT000040Device != null){
   			cvm.validate(pOCDMT000040Device, _location, diagnostic);
			{
				int i = 0;
				for (net.ihe.gazelle.datatypes.CS realmCode: pOCDMT000040Device.getRealmCode()){
					net.ihe.gazelle.datatypes.CS.validateByModule(realmCode, _location + "/realmCode[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			POCDMT000040InfrastructureRootTypeId.validateByModule(pOCDMT000040Device.getTypeId(), _location + "/typeId", cvm, diagnostic);
			{
				int i = 0;
				for (net.ihe.gazelle.datatypes.II templateId: pOCDMT000040Device.getTemplateId()){
					net.ihe.gazelle.datatypes.II.validateByModule(templateId, _location + "/templateId[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			net.ihe.gazelle.datatypes.CE.validateByModule(pOCDMT000040Device.getCode(), _location + "/code", cvm, diagnostic);
			net.ihe.gazelle.datatypes.SC.validateByModule(pOCDMT000040Device.getManufacturerModelName(), _location + "/manufacturerModelName", cvm, diagnostic);
			net.ihe.gazelle.datatypes.SC.validateByModule(pOCDMT000040Device.getSoftwareName(), _location + "/softwareName", cvm, diagnostic);
    	}
    }

}
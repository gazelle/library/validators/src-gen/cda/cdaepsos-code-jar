/**
 * POCDMT000040PlayingEntity.java
 *
 * File generated from the cdaepsos3::POCDMT000040PlayingEntity uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.cdaepsos4;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.ArrayList;
import java.util.List;

// End of user code


/**
 * Description of the class POCDMT000040PlayingEntity.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "POCD_MT000040.PlayingEntity", propOrder = {
	"realmCode",
	"typeId",
	"templateId",
	"code",
	"quantity",
	"name",
	"desc",
	"classCode",
	"determinerCode",
	"nullFlavor"
})
@XmlRootElement(name = "POCD_MT000040.PlayingEntity")
public class POCDMT000040PlayingEntity implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@XmlElement(name = "realmCode", namespace = "urn:hl7-org:v3")
	public List<net.ihe.gazelle.datatypes.CS> realmCode;
	@XmlElement(name = "typeId", namespace = "urn:hl7-org:v3")
	public POCDMT000040InfrastructureRootTypeId typeId;
	@XmlElement(name = "templateId", namespace = "urn:hl7-org:v3")
	public List<net.ihe.gazelle.datatypes.II> templateId;
	@XmlElement(name = "code", namespace = "urn:hl7-org:v3")
	public net.ihe.gazelle.datatypes.CE code;
	@XmlElement(name = "quantity", namespace = "urn:hl7-org:v3")
	public List<net.ihe.gazelle.datatypes.PQ> quantity;
	@XmlElement(name = "name", namespace = "urn:hl7-org:v3")
	public List<net.ihe.gazelle.datatypes.PN> name;
	@XmlElement(name = "desc", namespace = "urn:hl7-org:v3")
	public net.ihe.gazelle.datatypes.ED desc;
	@XmlAttribute(name = "classCode")
	public net.ihe.gazelle.voc.EntityClassRoot classCode;
	@XmlAttribute(name = "determinerCode")
	public net.ihe.gazelle.voc.EntityDeterminer determinerCode;
	@XmlAttribute(name = "nullFlavor")
	public net.ihe.gazelle.voc.NullFlavor nullFlavor;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private Node _xmlNodePresentation;
	
	
	/**
	 * Return realmCode.
	 * @return realmCode
	 */
	public List<net.ihe.gazelle.datatypes.CS> getRealmCode() {
		if (realmCode == null) {
	        realmCode = new ArrayList<net.ihe.gazelle.datatypes.CS>();
	    }
	    return realmCode;
	}
	
	/**
	 * Set a value to attribute realmCode.
	 * @param realmCode.
	 */
	public void setRealmCode(List<net.ihe.gazelle.datatypes.CS> realmCode) {
	    this.realmCode = realmCode;
	}
	
	
	
	/**
	 * Add a realmCode to the realmCode collection.
	 * @param realmCode_elt Element to add.
	 */
	public void addRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
	    this.getRealmCode().add(realmCode_elt);
	}
	
	/**
	 * Remove a realmCode to the realmCode collection.
	 * @param realmCode_elt Element to remove
	 */
	public void removeRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
	    this.getRealmCode().remove(realmCode_elt);
	}
	
	/**
	 * Return typeId.
	 * @return typeId
	 */
	public POCDMT000040InfrastructureRootTypeId getTypeId() {
	    return typeId;
	}
	
	/**
	 * Set a value to attribute typeId.
	 * @param typeId.
	 */
	public void setTypeId(POCDMT000040InfrastructureRootTypeId typeId) {
	    this.typeId = typeId;
	}
	
	
	
	
	/**
	 * Return templateId.
	 * @return templateId
	 */
	public List<net.ihe.gazelle.datatypes.II> getTemplateId() {
		if (templateId == null) {
	        templateId = new ArrayList<net.ihe.gazelle.datatypes.II>();
	    }
	    return templateId;
	}
	
	/**
	 * Set a value to attribute templateId.
	 * @param templateId.
	 */
	public void setTemplateId(List<net.ihe.gazelle.datatypes.II> templateId) {
	    this.templateId = templateId;
	}
	
	
	
	/**
	 * Add a templateId to the templateId collection.
	 * @param templateId_elt Element to add.
	 */
	public void addTemplateId(net.ihe.gazelle.datatypes.II templateId_elt) {
	    this.getTemplateId().add(templateId_elt);
	}
	
	/**
	 * Remove a templateId to the templateId collection.
	 * @param templateId_elt Element to remove
	 */
	public void removeTemplateId(net.ihe.gazelle.datatypes.II templateId_elt) {
	    this.getTemplateId().remove(templateId_elt);
	}
	
	/**
	 * Return code.
	 * @return code
	 */
	public net.ihe.gazelle.datatypes.CE getCode() {
	    return code;
	}
	
	/**
	 * Set a value to attribute code.
	 * @param code.
	 */
	public void setCode(net.ihe.gazelle.datatypes.CE code) {
	    this.code = code;
	}
	
	
	
	
	/**
	 * Return quantity.
	 * @return quantity
	 */
	public List<net.ihe.gazelle.datatypes.PQ> getQuantity() {
		if (quantity == null) {
	        quantity = new ArrayList<net.ihe.gazelle.datatypes.PQ>();
	    }
	    return quantity;
	}
	
	/**
	 * Set a value to attribute quantity.
	 * @param quantity.
	 */
	public void setQuantity(List<net.ihe.gazelle.datatypes.PQ> quantity) {
	    this.quantity = quantity;
	}
	
	
	
	/**
	 * Add a quantity to the quantity collection.
	 * @param quantity_elt Element to add.
	 */
	public void addQuantity(net.ihe.gazelle.datatypes.PQ quantity_elt) {
	    this.getQuantity().add(quantity_elt);
	}
	
	/**
	 * Remove a quantity to the quantity collection.
	 * @param quantity_elt Element to remove
	 */
	public void removeQuantity(net.ihe.gazelle.datatypes.PQ quantity_elt) {
	    this.getQuantity().remove(quantity_elt);
	}
	
	/**
	 * Return name.
	 * @return name
	 */
	public List<net.ihe.gazelle.datatypes.PN> getName() {
		if (name == null) {
	        name = new ArrayList<net.ihe.gazelle.datatypes.PN>();
	    }
	    return name;
	}
	
	/**
	 * Set a value to attribute name.
	 * @param name.
	 */
	public void setName(List<net.ihe.gazelle.datatypes.PN> name) {
	    this.name = name;
	}
	
	
	
	/**
	 * Add a name to the name collection.
	 * @param name_elt Element to add.
	 */
	public void addName(net.ihe.gazelle.datatypes.PN name_elt) {
	    this.getName().add(name_elt);
	}
	
	/**
	 * Remove a name to the name collection.
	 * @param name_elt Element to remove
	 */
	public void removeName(net.ihe.gazelle.datatypes.PN name_elt) {
	    this.getName().remove(name_elt);
	}
	
	/**
	 * Return desc.
	 * @return desc
	 */
	public net.ihe.gazelle.datatypes.ED getDesc() {
	    return desc;
	}
	
	/**
	 * Set a value to attribute desc.
	 * @param desc.
	 */
	public void setDesc(net.ihe.gazelle.datatypes.ED desc) {
	    this.desc = desc;
	}
	
	
	
	
	/**
	 * Return classCode.
	 * @return classCode
	 */
	public net.ihe.gazelle.voc.EntityClassRoot getClassCode() {
	    return classCode;
	}
	
	/**
	 * Set a value to attribute classCode.
	 * @param classCode.
	 */
	public void setClassCode(net.ihe.gazelle.voc.EntityClassRoot classCode) {
	    this.classCode = classCode;
	}
	
	
	
	
	/**
	 * Return determinerCode.
	 * @return determinerCode
	 */
	public net.ihe.gazelle.voc.EntityDeterminer getDeterminerCode() {
	    return determinerCode;
	}
	
	/**
	 * Set a value to attribute determinerCode.
	 * @param determinerCode.
	 */
	public void setDeterminerCode(net.ihe.gazelle.voc.EntityDeterminer determinerCode) {
	    this.determinerCode = determinerCode;
	}
	
	
	
	
	/**
	 * Return nullFlavor.
	 * @return nullFlavor
	 */
	public net.ihe.gazelle.voc.NullFlavor getNullFlavor() {
	    return nullFlavor;
	}
	
	/**
	 * Set a value to attribute nullFlavor.
	 * @param nullFlavor.
	 */
	public void setNullFlavor(net.ihe.gazelle.voc.NullFlavor nullFlavor) {
	    this.nullFlavor = nullFlavor;
	}
	
	
	
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.cdaepsos4");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:hl7-org:v3", "POCD_MT000040.PlayingEntity").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(POCDMT000040PlayingEntity pOCDMT000040PlayingEntity, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (pOCDMT000040PlayingEntity != null){
   			cvm.validate(pOCDMT000040PlayingEntity, _location, diagnostic);
			{
				int i = 0;
				for (net.ihe.gazelle.datatypes.CS realmCode: pOCDMT000040PlayingEntity.getRealmCode()){
					net.ihe.gazelle.datatypes.CS.validateByModule(realmCode, _location + "/realmCode[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			POCDMT000040InfrastructureRootTypeId.validateByModule(pOCDMT000040PlayingEntity.getTypeId(), _location + "/typeId", cvm, diagnostic);
			{
				int i = 0;
				for (net.ihe.gazelle.datatypes.II templateId: pOCDMT000040PlayingEntity.getTemplateId()){
					net.ihe.gazelle.datatypes.II.validateByModule(templateId, _location + "/templateId[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			net.ihe.gazelle.datatypes.CE.validateByModule(pOCDMT000040PlayingEntity.getCode(), _location + "/code", cvm, diagnostic);
			{
				int i = 0;
				for (net.ihe.gazelle.datatypes.PQ quantity: pOCDMT000040PlayingEntity.getQuantity()){
					net.ihe.gazelle.datatypes.PQ.validateByModule(quantity, _location + "/quantity[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			{
				int i = 0;
				for (net.ihe.gazelle.datatypes.PN name: pOCDMT000040PlayingEntity.getName()){
					net.ihe.gazelle.datatypes.PN.validateByModule(name, _location + "/name[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			net.ihe.gazelle.datatypes.ED.validateByModule(pOCDMT000040PlayingEntity.getDesc(), _location + "/desc", cvm, diagnostic);
    	}
    }

}
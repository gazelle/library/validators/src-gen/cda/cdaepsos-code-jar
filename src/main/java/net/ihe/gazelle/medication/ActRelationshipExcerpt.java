/**
 * ActRelationshipExcerpt.java
 *
 * File generated from the medication::ActRelationshipExcerpt uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration ActRelationshipExcerpt.
 *
 * 
 */

@XmlType(name = "ActRelationshipExcerpt")
@XmlEnum
@XmlRootElement(name = "ActRelationshipExcerpt")
public enum ActRelationshipExcerpt {
	@XmlEnumValue("XCRPT")
	XCRPT("XCRPT"),
	@XmlEnumValue("VRXCRPT")
	VRXCRPT("VRXCRPT");
	
	private final String value;

    ActRelationshipExcerpt(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static ActRelationshipExcerpt fromValue(String v) {
        for (ActRelationshipExcerpt c: ActRelationshipExcerpt.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
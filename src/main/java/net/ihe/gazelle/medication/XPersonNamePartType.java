/**
 * XPersonNamePartType.java
 *
 * File generated from the medication::XPersonNamePartType uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration XPersonNamePartType.
 *
 * 
 */

@XmlType(name = "XPersonNamePartType")
@XmlEnum
@XmlRootElement(name = "XPersonNamePartType")
public enum XPersonNamePartType {
	@XmlEnumValue("DEL")
	DEL("DEL"),
	@XmlEnumValue("FAM")
	FAM("FAM"),
	@XmlEnumValue("GIV")
	GIV("GIV"),
	@XmlEnumValue("PFX")
	PFX("PFX"),
	@XmlEnumValue("SFX")
	SFX("SFX");
	
	private final String value;

    XPersonNamePartType(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static XPersonNamePartType fromValue(String v) {
        for (XPersonNamePartType c: XPersonNamePartType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
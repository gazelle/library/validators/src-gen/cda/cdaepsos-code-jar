/**
 * XInformationRecipient.java
 *
 * File generated from the medication::XInformationRecipient uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration XInformationRecipient.
 *
 * 
 */

@XmlType(name = "XInformationRecipient")
@XmlEnum
@XmlRootElement(name = "XInformationRecipient")
public enum XInformationRecipient {
	@XmlEnumValue("PRCP")
	PRCP("PRCP"),
	@XmlEnumValue("TRC")
	TRC("TRC");
	
	private final String value;

    XInformationRecipient(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static XInformationRecipient fromValue(String v) {
        for (XInformationRecipient c: XInformationRecipient.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
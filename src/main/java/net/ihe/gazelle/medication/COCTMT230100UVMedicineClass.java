/**
 * COCTMT230100UVMedicineClass.java
 *
 * File generated from the medication::COCTMT230100UVMedicineClass uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


/**
 * Description of the class COCTMT230100UVMedicineClass.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "COCT_MT230100UV.MedicineClass", propOrder = {
	"realmCode",
	"typeId",
	"templateId",
	"code",
	"name",
	"desc",
	"formCode",
	"classCode",
	"determinerCode",
	"nullFlavor"
})
@XmlRootElement(name = "COCT_MT230100UV.MedicineClass")
public class COCTMT230100UVMedicineClass implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@XmlElement(name = "realmCode", namespace = "urn:hl7-org:v3")
	public List<net.ihe.gazelle.datatypes.CS> realmCode;
	@XmlElement(name = "typeId", namespace = "urn:hl7-org:v3")
	public net.ihe.gazelle.infr.AllInfrastructureRootTypeId typeId;
	@XmlElement(name = "templateId", namespace = "urn:hl7-org:v3")
	public List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> templateId;
	@XmlElement(name = "code", required = true, namespace = "urn:epsos-org:ep:medication")
	public net.ihe.gazelle.medication.CE code;
	@XmlElement(name = "name", namespace = "urn:epsos-org:ep:medication")
	public List<net.ihe.gazelle.medication.TN> name;
	@XmlElement(name = "desc", namespace = "urn:epsos-org:ep:medication")
	public net.ihe.gazelle.medication.ED desc;
	@XmlElement(name = "formCode", namespace = "urn:epsos-org:ep:medication")
	public net.ihe.gazelle.medication.CE formCode;
	@XmlAttribute(name = "classCode", required = true)
	public net.ihe.gazelle.medication.EntityClassManufacturedMaterial classCode;
	@XmlAttribute(name = "determinerCode")
	public net.ihe.gazelle.medication.EntityDeterminer determinerCode;
	@XmlAttribute(name = "nullFlavor")
	public net.ihe.gazelle.medication.NullFlavor nullFlavor;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private org.w3c.dom.Node _xmlNodePresentation;
	
	
	/**
	 * Return realmCode.
	 * @return realmCode
	 */
	public List<net.ihe.gazelle.datatypes.CS> getRealmCode() {
		if (realmCode == null) {
	        realmCode = new ArrayList<net.ihe.gazelle.datatypes.CS>();
	    }
	    return realmCode;
	}
	
	/**
	 * Set a value to attribute realmCode.
	 * @param realmCode.
	 */
	public void setRealmCode(List<net.ihe.gazelle.datatypes.CS> realmCode) {
	    this.realmCode = realmCode;
	}
	
	
	
	/**
	 * Add a realmCode to the realmCode collection.
	 * @param realmCode_elt Element to add.
	 */
	public void addRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
	    this.realmCode.add(realmCode_elt);
	}
	
	/**
	 * Remove a realmCode to the realmCode collection.
	 * @param realmCode_elt Element to remove
	 */
	public void removeRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
	    this.realmCode.remove(realmCode_elt);
	}
	
	/**
	 * Return typeId.
	 * @return typeId
	 */
	public net.ihe.gazelle.infr.AllInfrastructureRootTypeId getTypeId() {
	    return typeId;
	}
	
	/**
	 * Set a value to attribute typeId.
	 * @param typeId.
	 */
	public void setTypeId(net.ihe.gazelle.infr.AllInfrastructureRootTypeId typeId) {
	    this.typeId = typeId;
	}
	
	
	
	
	/**
	 * Return templateId.
	 * @return templateId
	 */
	public List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> getTemplateId() {
		if (templateId == null) {
	        templateId = new ArrayList<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId>();
	    }
	    return templateId;
	}
	
	/**
	 * Set a value to attribute templateId.
	 * @param templateId.
	 */
	public void setTemplateId(List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> templateId) {
	    this.templateId = templateId;
	}
	
	
	
	/**
	 * Add a templateId to the templateId collection.
	 * @param templateId_elt Element to add.
	 */
	public void addTemplateId(net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId_elt) {
	    this.templateId.add(templateId_elt);
	}
	
	/**
	 * Remove a templateId to the templateId collection.
	 * @param templateId_elt Element to remove
	 */
	public void removeTemplateId(net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId_elt) {
	    this.templateId.remove(templateId_elt);
	}
	
	/**
	 * Return code.
	 * @return code
	 */
	public net.ihe.gazelle.medication.CE getCode() {
	    return code;
	}
	
	/**
	 * Set a value to attribute code.
	 * @param code.
	 */
	public void setCode(net.ihe.gazelle.medication.CE code) {
	    this.code = code;
	}
	
	
	
	
	/**
	 * Return name.
	 * @return name
	 */
	public List<net.ihe.gazelle.medication.TN> getName() {
		if (name == null) {
	        name = new ArrayList<net.ihe.gazelle.medication.TN>();
	    }
	    return name;
	}
	
	/**
	 * Set a value to attribute name.
	 * @param name.
	 */
	public void setName(List<net.ihe.gazelle.medication.TN> name) {
	    this.name = name;
	}
	
	
	
	/**
	 * Add a name to the name collection.
	 * @param name_elt Element to add.
	 */
	public void addName(net.ihe.gazelle.medication.TN name_elt) {
	    this.name.add(name_elt);
	}
	
	/**
	 * Remove a name to the name collection.
	 * @param name_elt Element to remove
	 */
	public void removeName(net.ihe.gazelle.medication.TN name_elt) {
	    this.name.remove(name_elt);
	}
	
	/**
	 * Return desc.
	 * @return desc
	 */
	public net.ihe.gazelle.medication.ED getDesc() {
	    return desc;
	}
	
	/**
	 * Set a value to attribute desc.
	 * @param desc.
	 */
	public void setDesc(net.ihe.gazelle.medication.ED desc) {
	    this.desc = desc;
	}
	
	
	
	
	/**
	 * Return formCode.
	 * @return formCode
	 */
	public net.ihe.gazelle.medication.CE getFormCode() {
	    return formCode;
	}
	
	/**
	 * Set a value to attribute formCode.
	 * @param formCode.
	 */
	public void setFormCode(net.ihe.gazelle.medication.CE formCode) {
	    this.formCode = formCode;
	}
	
	
	
	
	/**
	 * Return classCode.
	 * @return classCode
	 */
	public net.ihe.gazelle.medication.EntityClassManufacturedMaterial getClassCode() {
	    return classCode;
	}
	
	/**
	 * Set a value to attribute classCode.
	 * @param classCode.
	 */
	public void setClassCode(net.ihe.gazelle.medication.EntityClassManufacturedMaterial classCode) {
	    this.classCode = classCode;
	}
	
	
	
	
	/**
	 * Return determinerCode.
	 * @return determinerCode
	 */
	public net.ihe.gazelle.medication.EntityDeterminer getDeterminerCode() {
	    return determinerCode;
	}
	
	/**
	 * Set a value to attribute determinerCode.
	 * @param determinerCode.
	 */
	public void setDeterminerCode(net.ihe.gazelle.medication.EntityDeterminer determinerCode) {
	    this.determinerCode = determinerCode;
	}
	
	
	
	
	/**
	 * Return nullFlavor.
	 * @return nullFlavor
	 */
	public net.ihe.gazelle.medication.NullFlavor getNullFlavor() {
	    return nullFlavor;
	}
	
	/**
	 * Set a value to attribute nullFlavor.
	 * @param nullFlavor.
	 */
	public void setNullFlavor(net.ihe.gazelle.medication.NullFlavor nullFlavor) {
	    this.nullFlavor = nullFlavor;
	}
	
	
	
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.medication");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:epsos-org:ep:medication", "COCT_MT230100UV.MedicineClass").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(COCTMT230100UVMedicineClass cOCTMT230100UVMedicineClass, String location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (cOCTMT230100UVMedicineClass != null){
   			cvm.validate(cOCTMT230100UVMedicineClass, location, diagnostic);
			{
				int i = 0;
				for (net.ihe.gazelle.datatypes.CS realmCode: cOCTMT230100UVMedicineClass.getRealmCode()){
					net.ihe.gazelle.datatypes.CS.validateByModule(realmCode, location + "/realmCode[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			net.ihe.gazelle.infr.AllInfrastructureRootTypeId.validateByModule(cOCTMT230100UVMedicineClass.getTypeId(), location + "/typeId", cvm, diagnostic);
			{
				int i = 0;
				for (net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId: cOCTMT230100UVMedicineClass.getTemplateId()){
					net.ihe.gazelle.infr.AllInfrastructureRootTemplateId.validateByModule(templateId, location + "/templateId[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			net.ihe.gazelle.medication.CE.validateByModule(cOCTMT230100UVMedicineClass.getCode(), location + "/code", cvm, diagnostic);
			{
				int i = 0;
				for (net.ihe.gazelle.medication.TN name: cOCTMT230100UVMedicineClass.getName()){
					net.ihe.gazelle.medication.TN.validateByModule(name, location + "/name[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			net.ihe.gazelle.medication.ED.validateByModule(cOCTMT230100UVMedicineClass.getDesc(), location + "/desc", cvm, diagnostic);
			net.ihe.gazelle.medication.CE.validateByModule(cOCTMT230100UVMedicineClass.getFormCode(), location + "/formCode", cvm, diagnostic);
    	}
    }

}
/**
 * ActClassInvoiceElement.java
 *
 * File generated from the medication::ActClassInvoiceElement uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration ActClassInvoiceElement.
 *
 */

@XmlType(name = "ActClassInvoiceElement")
@XmlEnum
@XmlRootElement(name = "ActClassInvoiceElement")
public enum ActClassInvoiceElement {
	@XmlEnumValue("INVE")
	INVE("INVE");
	
	private final String value;

    ActClassInvoiceElement(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static ActClassInvoiceElement fromValue(String v) {
        for (ActClassInvoiceElement c: ActClassInvoiceElement.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
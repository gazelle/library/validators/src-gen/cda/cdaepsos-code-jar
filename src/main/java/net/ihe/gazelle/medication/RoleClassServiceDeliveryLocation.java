/**
 * RoleClassServiceDeliveryLocation.java
 *
 * File generated from the medication::RoleClassServiceDeliveryLocation uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration RoleClassServiceDeliveryLocation.
 *
 * 
 */

@XmlType(name = "RoleClassServiceDeliveryLocation")
@XmlEnum
@XmlRootElement(name = "RoleClassServiceDeliveryLocation")
public enum RoleClassServiceDeliveryLocation {
	@XmlEnumValue("SDLOC")
	SDLOC("SDLOC"),
	@XmlEnumValue("DSDLOC")
	DSDLOC("DSDLOC"),
	@XmlEnumValue("ISDLOC")
	ISDLOC("ISDLOC");
	
	private final String value;

    RoleClassServiceDeliveryLocation(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static RoleClassServiceDeliveryLocation fromValue(String v) {
        for (RoleClassServiceDeliveryLocation c: RoleClassServiceDeliveryLocation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
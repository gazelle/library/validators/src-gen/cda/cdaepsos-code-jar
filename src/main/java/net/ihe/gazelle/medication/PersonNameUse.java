/**
 * PersonNameUse.java
 *
 * File generated from the medication::PersonNameUse uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration PersonNameUse.
 *
 * 
 */

@XmlType(name = "PersonNameUse")
@XmlEnum
@XmlRootElement(name = "PersonNameUse")
public enum PersonNameUse {
	@XmlEnumValue("A")
	A("A"),
	@XmlEnumValue("ASGN")
	ASGN("ASGN"),
	@XmlEnumValue("C")
	C("C"),
	@XmlEnumValue("I")
	I("I"),
	@XmlEnumValue("L")
	L("L"),
	@XmlEnumValue("R")
	R("R"),
	@XmlEnumValue("SRCH")
	SRCH("SRCH"),
	@XmlEnumValue("PHON")
	PHON("PHON"),
	@XmlEnumValue("SNDX")
	SNDX("SNDX"),
	@XmlEnumValue("P")
	P("P"),
	@XmlEnumValue("ABC")
	ABC("ABC"),
	@XmlEnumValue("IDE")
	IDE("IDE"),
	@XmlEnumValue("SYL")
	SYL("SYL");
	
	private final String value;

    PersonNameUse(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static PersonNameUse fromValue(String v) {
        for (PersonNameUse c: PersonNameUse.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
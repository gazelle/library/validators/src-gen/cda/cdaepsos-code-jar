/**
 * Other.java
 *
 * File generated from the medication::Other uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration Other.
 *
 * 
 */

@XmlType(name = "Other")
@XmlEnum
@XmlRootElement(name = "Other")
public enum Other {
	@XmlEnumValue("OTH")
	OTH("OTH"),
	@XmlEnumValue("NINF")
	NINF("NINF"),
	@XmlEnumValue("PINF")
	PINF("PINF");
	
	private final String value;

    Other(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static Other fromValue(String v) {
        for (Other c: Other.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
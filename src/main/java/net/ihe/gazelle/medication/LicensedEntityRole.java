/**
 * LicensedEntityRole.java
 *
 * File generated from the medication::LicensedEntityRole uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration LicensedEntityRole.
 *
 * 
 */

@XmlType(name = "LicensedEntityRole")
@XmlEnum
@XmlRootElement(name = "LicensedEntityRole")
public enum LicensedEntityRole {
	@XmlEnumValue("LIC")
	LIC("LIC"),
	@XmlEnumValue("NOT")
	NOT("NOT"),
	@XmlEnumValue("PROV")
	PROV("PROV");
	
	private final String value;

    LicensedEntityRole(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static LicensedEntityRole fromValue(String v) {
        for (LicensedEntityRole c: LicensedEntityRole.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
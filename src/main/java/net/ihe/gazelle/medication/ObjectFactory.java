package net.ihe.gazelle.medication;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {

	private final static QName _ADGroup_QNAME = new QName("", "group:2");
	private final static QName _ADDelimiter_QNAME = new QName("urn:epsos-org:ep:medication", "delimiter");
	private final static QName _ADCountry_QNAME = new QName("urn:epsos-org:ep:medication", "country");
	private final static QName _ADState_QNAME = new QName("urn:epsos-org:ep:medication", "state");
	private final static QName _ADCounty_QNAME = new QName("urn:epsos-org:ep:medication", "county");
	private final static QName _ADCity_QNAME = new QName("urn:epsos-org:ep:medication", "city");
	private final static QName _ADPostalCode_QNAME = new QName("urn:epsos-org:ep:medication", "postalCode");
	private final static QName _ADStreetAddressLine_QNAME = new QName("urn:epsos-org:ep:medication", "streetAddressLine");
	private final static QName _ADHouseNumber_QNAME = new QName("urn:epsos-org:ep:medication", "houseNumber");
	private final static QName _ADHouseNumberNumeric_QNAME = new QName("urn:epsos-org:ep:medication", "houseNumberNumeric");
	private final static QName _ADDirection_QNAME = new QName("urn:epsos-org:ep:medication", "direction");
	private final static QName _ADStreetName_QNAME = new QName("urn:epsos-org:ep:medication", "streetName");
	private final static QName _ADStreetNameBase_QNAME = new QName("urn:epsos-org:ep:medication", "streetNameBase");
	private final static QName _ADStreetNameType_QNAME = new QName("urn:epsos-org:ep:medication", "streetNameType");
	private final static QName _ADAdditionalLocator_QNAME = new QName("urn:epsos-org:ep:medication", "additionalLocator");
	private final static QName _ADUnitID_QNAME = new QName("urn:epsos-org:ep:medication", "unitID");
	private final static QName _ADUnitType_QNAME = new QName("urn:epsos-org:ep:medication", "unitType");
	private final static QName _ADCareOf_QNAME = new QName("urn:epsos-org:ep:medication", "careOf");
	private final static QName _ADCensusTract_QNAME = new QName("urn:epsos-org:ep:medication", "censusTract");
	private final static QName _ADDeliveryAddressLine_QNAME = new QName("urn:epsos-org:ep:medication", "deliveryAddressLine");
	private final static QName _ADDeliveryInstallationType_QNAME = new QName("urn:epsos-org:ep:medication", "deliveryInstallationType");
	private final static QName _ADDeliveryInstallationArea_QNAME = new QName("urn:epsos-org:ep:medication", "deliveryInstallationArea");
	private final static QName _ADDeliveryInstallationQualifier_QNAME = new QName("urn:epsos-org:ep:medication", "deliveryInstallationQualifier");
	private final static QName _ADDeliveryMode_QNAME = new QName("urn:epsos-org:ep:medication", "deliveryMode");
	private final static QName _ADDeliveryModeIdentifier_QNAME = new QName("urn:epsos-org:ep:medication", "deliveryModeIdentifier");
	private final static QName _ADBuildingNumberSuffix_QNAME = new QName("urn:epsos-org:ep:medication", "buildingNumberSuffix");
	private final static QName _ADPostBox_QNAME = new QName("urn:epsos-org:ep:medication", "postBox");
	private final static QName _ADPrecinct_QNAME = new QName("urn:epsos-org:ep:medication", "precinct");
	private final static QName _ADUseablePeriod_QNAME = new QName("urn:epsos-org:ep:medication", "useablePeriod");
	private final static QName _EDReference_QNAME = new QName("urn:epsos-org:ep:medication", "reference");
	private final static QName _EDThumbnail_QNAME = new QName("urn:epsos-org:ep:medication", "thumbnail");
	private final static QName _ENGroup_QNAME = new QName("", "group:2");
	private final static QName _ENDelimiter_QNAME = new QName("urn:epsos-org:ep:medication", "delimiter");
	private final static QName _ENFamily_QNAME = new QName("urn:epsos-org:ep:medication", "family");
	private final static QName _ENGiven_QNAME = new QName("urn:epsos-org:ep:medication", "given");
	private final static QName _ENPrefix_QNAME = new QName("urn:epsos-org:ep:medication", "prefix");
	private final static QName _ENSuffix_QNAME = new QName("urn:epsos-org:ep:medication", "suffix");
	private final static QName _ENValidTime_QNAME = new QName("urn:epsos-org:ep:medication", "validTime");
	private final static QName _DocumentRootXMLNSPrefixMap_QNAME = new QName("", "xmlns:prefix");
	private final static QName _DocumentRootXSISchemaLocation_QNAME = new QName("", "xsi:schemaLocation");
	private final static QName _DocumentRootAsContent_QNAME = new QName("urn:epsos-org:ep:medication", "asContent");
	private final static QName _DocumentRootAsDistributedProduct_QNAME = new QName("urn:epsos-org:ep:medication", "asDistributedProduct");
	private final static QName _DocumentRootAsMedicineManufacturer_QNAME = new QName("urn:epsos-org:ep:medication", "asMedicineManufacturer");
	private final static QName _DocumentRootAsSpecializedKind_QNAME = new QName("urn:epsos-org:ep:medication", "asSpecializedKind");
	private final static QName _DocumentRootDesc_QNAME = new QName("urn:epsos-org:ep:medication", "desc");
	private final static QName _DocumentRootExpirationTime_QNAME = new QName("urn:epsos-org:ep:medication", "expirationTime");
	private final static QName _DocumentRootFormCode_QNAME = new QName("urn:epsos-org:ep:medication", "formCode");
	private final static QName _DocumentRootHandlingCode_QNAME = new QName("urn:epsos-org:ep:medication", "handlingCode");
	private final static QName _DocumentRootId_QNAME = new QName("urn:epsos-org:ep:medication", "id");
	private final static QName _DocumentRootIngredient_QNAME = new QName("urn:epsos-org:ep:medication", "ingredient");
	private final static QName _DocumentRootMedication_QNAME = new QName("urn:epsos-org:ep:medication", "medication");
	private final static QName _DocumentRootPart_QNAME = new QName("urn:epsos-org:ep:medication", "part");
	private final static QName _DocumentRootRiskCode_QNAME = new QName("urn:epsos-org:ep:medication", "riskCode");
	private final static QName _DocumentRootStabilityTime_QNAME = new QName("urn:epsos-org:ep:medication", "stabilityTime");
	private final static QName _DocumentRootSubjectOf1_QNAME = new QName("urn:epsos-org:ep:medication", "subjectOf1");
	private final static QName _DocumentRootSubjectOf2_QNAME = new QName("urn:epsos-org:ep:medication", "subjectOf2");
	private final static QName _DocumentRootSubjectOf3_QNAME = new QName("urn:epsos-org:ep:medication", "subjectOf3");
	private final static QName _DocumentRootSubjectOf4_QNAME = new QName("urn:epsos-org:ep:medication", "subjectOf4");
	private final static QName _DocumentRootSubjectOf5_QNAME = new QName("urn:epsos-org:ep:medication", "subjectOf5");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {}
    
    /**
     * Create an instance of {@link  AD}
     * 
     */
    public AD createAD() {
        return new AD();
    }

    /**
     * Create an instance of {@link  AdxpDelimiter}
     * 
     */
    public AdxpDelimiter createAdxpDelimiter() {
        return new AdxpDelimiter();
    }

    /**
     * Create an instance of {@link  ADXP}
     * 
     */
    public ADXP createADXP() {
        return new ADXP();
    }

    /**
     * Create an instance of {@link  ST1}
     * 
     */
    public ST1 createST1() {
        return new ST1();
    }

    /**
     * Create an instance of {@link  ED}
     * 
     */
    public ED createED() {
        return new ED();
    }

    /**
     * Create an instance of {@link  TEL}
     * 
     */
    public TEL createTEL() {
        return new TEL();
    }

    /**
     * Create an instance of {@link  SXCMTS}
     * 
     */
    public SXCMTS createSXCMTS() {
        return new SXCMTS();
    }

    /**
     * Create an instance of {@link  TS1}
     * 
     */
    public TS1 createTS1() {
        return new TS1();
    }

    /**
     * Create an instance of {@link  Thumbnail}
     * 
     */
    public Thumbnail createThumbnail() {
        return new Thumbnail();
    }

    /**
     * Create an instance of {@link  AdxpCountry}
     * 
     */
    public AdxpCountry createAdxpCountry() {
        return new AdxpCountry();
    }

    /**
     * Create an instance of {@link  AdxpState}
     * 
     */
    public AdxpState createAdxpState() {
        return new AdxpState();
    }

    /**
     * Create an instance of {@link  AdxpCounty}
     * 
     */
    public AdxpCounty createAdxpCounty() {
        return new AdxpCounty();
    }

    /**
     * Create an instance of {@link  AdxpCity}
     * 
     */
    public AdxpCity createAdxpCity() {
        return new AdxpCity();
    }

    /**
     * Create an instance of {@link  AdxpPostalCode}
     * 
     */
    public AdxpPostalCode createAdxpPostalCode() {
        return new AdxpPostalCode();
    }

    /**
     * Create an instance of {@link  AdxpStreetAddressLine}
     * 
     */
    public AdxpStreetAddressLine createAdxpStreetAddressLine() {
        return new AdxpStreetAddressLine();
    }

    /**
     * Create an instance of {@link  AdxpHouseNumber}
     * 
     */
    public AdxpHouseNumber createAdxpHouseNumber() {
        return new AdxpHouseNumber();
    }

    /**
     * Create an instance of {@link  AdxpHouseNumberNumeric}
     * 
     */
    public AdxpHouseNumberNumeric createAdxpHouseNumberNumeric() {
        return new AdxpHouseNumberNumeric();
    }

    /**
     * Create an instance of {@link  AdxpDirection}
     * 
     */
    public AdxpDirection createAdxpDirection() {
        return new AdxpDirection();
    }

    /**
     * Create an instance of {@link  AdxpStreetName}
     * 
     */
    public AdxpStreetName createAdxpStreetName() {
        return new AdxpStreetName();
    }

    /**
     * Create an instance of {@link  AdxpStreetNameBase}
     * 
     */
    public AdxpStreetNameBase createAdxpStreetNameBase() {
        return new AdxpStreetNameBase();
    }

    /**
     * Create an instance of {@link  AdxpStreetNameType}
     * 
     */
    public AdxpStreetNameType createAdxpStreetNameType() {
        return new AdxpStreetNameType();
    }

    /**
     * Create an instance of {@link  AdxpAdditionalLocator}
     * 
     */
    public AdxpAdditionalLocator createAdxpAdditionalLocator() {
        return new AdxpAdditionalLocator();
    }

    /**
     * Create an instance of {@link  AdxpUnitID}
     * 
     */
    public AdxpUnitID createAdxpUnitID() {
        return new AdxpUnitID();
    }

    /**
     * Create an instance of {@link  AdxpUnitType}
     * 
     */
    public AdxpUnitType createAdxpUnitType() {
        return new AdxpUnitType();
    }

    /**
     * Create an instance of {@link  AdxpCareOf}
     * 
     */
    public AdxpCareOf createAdxpCareOf() {
        return new AdxpCareOf();
    }

    /**
     * Create an instance of {@link  AdxpCensusTract}
     * 
     */
    public AdxpCensusTract createAdxpCensusTract() {
        return new AdxpCensusTract();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryAddressLine}
     * 
     */
    public AdxpDeliveryAddressLine createAdxpDeliveryAddressLine() {
        return new AdxpDeliveryAddressLine();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryInstallationType}
     * 
     */
    public AdxpDeliveryInstallationType createAdxpDeliveryInstallationType() {
        return new AdxpDeliveryInstallationType();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryInstallationArea}
     * 
     */
    public AdxpDeliveryInstallationArea createAdxpDeliveryInstallationArea() {
        return new AdxpDeliveryInstallationArea();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryInstallationQualifier}
     * 
     */
    public AdxpDeliveryInstallationQualifier createAdxpDeliveryInstallationQualifier() {
        return new AdxpDeliveryInstallationQualifier();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryMode}
     * 
     */
    public AdxpDeliveryMode createAdxpDeliveryMode() {
        return new AdxpDeliveryMode();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryModeIdentifier}
     * 
     */
    public AdxpDeliveryModeIdentifier createAdxpDeliveryModeIdentifier() {
        return new AdxpDeliveryModeIdentifier();
    }

    /**
     * Create an instance of {@link  AdxpBuildingNumberSuffix}
     * 
     */
    public AdxpBuildingNumberSuffix createAdxpBuildingNumberSuffix() {
        return new AdxpBuildingNumberSuffix();
    }

    /**
     * Create an instance of {@link  AdxpPostBox}
     * 
     */
    public AdxpPostBox createAdxpPostBox() {
        return new AdxpPostBox();
    }

    /**
     * Create an instance of {@link  AdxpPrecinct}
     * 
     */
    public AdxpPrecinct createAdxpPrecinct() {
        return new AdxpPrecinct();
    }

    /**
     * Create an instance of {@link  ANYNonNull}
     * 
     */
    public ANYNonNull createANYNonNull() {
        return new ANYNonNull();
    }

    /**
     * Create an instance of {@link  BL1}
     * 
     */
    public BL1 createBL1() {
        return new BL1();
    }

    /**
     * Create an instance of {@link  BN1}
     * 
     */
    public BN1 createBN1() {
        return new BN1();
    }

    /**
     * Create an instance of {@link  BXITCD}
     * 
     */
    public BXITCD createBXITCD() {
        return new BXITCD();
    }

    /**
     * Create an instance of {@link  CD}
     * 
     */
    public CD createCD() {
        return new CD();
    }

    /**
     * Create an instance of {@link  CR}
     * 
     */
    public CR createCR() {
        return new CR();
    }

    /**
     * Create an instance of {@link  CV}
     * 
     */
    public CV createCV() {
        return new CV();
    }

    /**
     * Create an instance of {@link  CE}
     * 
     */
    public CE createCE() {
        return new CE();
    }

    /**
     * Create an instance of {@link  BXITIVLPQ}
     * 
     */
    public BXITIVLPQ createBXITIVLPQ() {
        return new BXITIVLPQ();
    }

    /**
     * Create an instance of {@link  IVLPQ}
     * 
     */
    public IVLPQ createIVLPQ() {
        return new IVLPQ();
    }

    /**
     * Create an instance of {@link  SXCMPQ}
     * 
     */
    public SXCMPQ createSXCMPQ() {
        return new SXCMPQ();
    }

    /**
     * Create an instance of {@link  PQ}
     * 
     */
    public PQ createPQ() {
        return new PQ();
    }

    /**
     * Create an instance of {@link  PQR}
     * 
     */
    public PQR createPQR() {
        return new PQR();
    }

    /**
     * Create an instance of {@link  IVXBPQ}
     * 
     */
    public IVXBPQ createIVXBPQ() {
        return new IVXBPQ();
    }

    /**
     * Create an instance of {@link  CO}
     * 
     */
    public CO createCO() {
        return new CO();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVAgency}
     * 
     */
    public COCTMT230100UVAgency createCOCTMT230100UVAgency() {
        return new COCTMT230100UVAgency();
    }

    /**
     * Create an instance of {@link  II}
     * 
     */
    public II createII() {
        return new II();
    }

    /**
     * Create an instance of {@link  ON}
     * 
     */
    public ON createON() {
        return new ON();
    }

    /**
     * Create an instance of {@link  EN}
     * 
     */
    public EN createEN() {
        return new EN();
    }

    /**
     * Create an instance of {@link  EnDelimiter}
     * 
     */
    public EnDelimiter createEnDelimiter() {
        return new EnDelimiter();
    }

    /**
     * Create an instance of {@link  ENXP}
     * 
     */
    public ENXP createENXP() {
        return new ENXP();
    }

    /**
     * Create an instance of {@link  EnFamily}
     * 
     */
    public EnFamily createEnFamily() {
        return new EnFamily();
    }

    /**
     * Create an instance of {@link  EnGiven}
     * 
     */
    public EnGiven createEnGiven() {
        return new EnGiven();
    }

    /**
     * Create an instance of {@link  EnPrefix}
     * 
     */
    public EnPrefix createEnPrefix() {
        return new EnPrefix();
    }

    /**
     * Create an instance of {@link  EnSuffix}
     * 
     */
    public EnSuffix createEnSuffix() {
        return new EnSuffix();
    }

    /**
     * Create an instance of {@link  IVLTS}
     * 
     */
    public IVLTS createIVLTS() {
        return new IVLTS();
    }

    /**
     * Create an instance of {@link  IVXBTS}
     * 
     */
    public IVXBTS createIVXBTS() {
        return new IVXBTS();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVApproval}
     * 
     */
    public COCTMT230100UVApproval createCOCTMT230100UVApproval() {
        return new COCTMT230100UVApproval();
    }

    /**
     * Create an instance of {@link  CS1}
     * 
     */
    public CS1 createCS1() {
        return new CS1();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVHolder}
     * 
     */
    public COCTMT230100UVHolder createCOCTMT230100UVHolder() {
        return new COCTMT230100UVHolder();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVRole}
     * 
     */
    public COCTMT230100UVRole createCOCTMT230100UVRole() {
        return new COCTMT230100UVRole();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVManufacturer}
     * 
     */
    public COCTMT230100UVManufacturer createCOCTMT230100UVManufacturer() {
        return new COCTMT230100UVManufacturer();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVRelatedManufacturer}
     * 
     */
    public COCTMT230100UVRelatedManufacturer createCOCTMT230100UVRelatedManufacturer() {
        return new COCTMT230100UVRelatedManufacturer();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVAuthor}
     * 
     */
    public COCTMT230100UVAuthor createCOCTMT230100UVAuthor() {
        return new COCTMT230100UVAuthor();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVTerritorialAuthority}
     * 
     */
    public COCTMT230100UVTerritorialAuthority createCOCTMT230100UVTerritorialAuthority() {
        return new COCTMT230100UVTerritorialAuthority();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVCountry}
     * 
     */
    public COCTMT230100UVCountry createCOCTMT230100UVCountry() {
        return new COCTMT230100UVCountry();
    }

    /**
     * Create an instance of {@link  TN}
     * 
     */
    public TN createTN() {
        return new TN();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVCharacteristic}
     * 
     */
    public COCTMT230100UVCharacteristic createCOCTMT230100UVCharacteristic() {
        return new COCTMT230100UVCharacteristic();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVContent}
     * 
     */
    public COCTMT230100UVContent createCOCTMT230100UVContent() {
        return new COCTMT230100UVContent();
    }

    /**
     * Create an instance of {@link  RTOQTYQTY}
     * 
     */
    public RTOQTYQTY createRTOQTYQTY() {
        return new RTOQTYQTY();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVPackagedMedicine}
     * 
     */
    public COCTMT230100UVPackagedMedicine createCOCTMT230100UVPackagedMedicine() {
        return new COCTMT230100UVPackagedMedicine();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVManufacturedProduct}
     * 
     */
    public COCTMT230100UVManufacturedProduct createCOCTMT230100UVManufacturedProduct() {
        return new COCTMT230100UVManufacturedProduct();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject25}
     * 
     */
    public COCTMT230100UVSubject25 createCOCTMT230100UVSubject25() {
        return new COCTMT230100UVSubject25();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject15}
     * 
     */
    public COCTMT230100UVSubject15 createCOCTMT230100UVSubject15() {
        return new COCTMT230100UVSubject15();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVPolicy}
     * 
     */
    public COCTMT230100UVPolicy createCOCTMT230100UVPolicy() {
        return new COCTMT230100UVPolicy();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject16}
     * 
     */
    public COCTMT230100UVSubject16 createCOCTMT230100UVSubject16() {
        return new COCTMT230100UVSubject16();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSuperContent}
     * 
     */
    public COCTMT230100UVSuperContent createCOCTMT230100UVSuperContent() {
        return new COCTMT230100UVSuperContent();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubContent}
     * 
     */
    public COCTMT230100UVSubContent createCOCTMT230100UVSubContent() {
        return new COCTMT230100UVSubContent();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject14}
     * 
     */
    public COCTMT230100UVSubject14 createCOCTMT230100UVSubject14() {
        return new COCTMT230100UVSubject14();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject11}
     * 
     */
    public COCTMT230100UVSubject11 createCOCTMT230100UVSubject11() {
        return new COCTMT230100UVSubject11();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVDistributedProduct}
     * 
     */
    public COCTMT230100UVDistributedProduct createCOCTMT230100UVDistributedProduct() {
        return new COCTMT230100UVDistributedProduct();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVIngredient}
     * 
     */
    public COCTMT230100UVIngredient createCOCTMT230100UVIngredient() {
        return new COCTMT230100UVIngredient();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubstance}
     * 
     */
    public COCTMT230100UVSubstance createCOCTMT230100UVSubstance() {
        return new COCTMT230100UVSubstance();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubstanceManufacturer}
     * 
     */
    public COCTMT230100UVSubstanceManufacturer createCOCTMT230100UVSubstanceManufacturer() {
        return new COCTMT230100UVSubstanceManufacturer();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubIngredient}
     * 
     */
    public COCTMT230100UVSubIngredient createCOCTMT230100UVSubIngredient() {
        return new COCTMT230100UVSubIngredient();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVMedication}
     * 
     */
    public COCTMT230100UVMedication createCOCTMT230100UVMedication() {
        return new COCTMT230100UVMedication();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVMedicine}
     * 
     */
    public COCTMT230100UVMedicine createCOCTMT230100UVMedicine() {
        return new COCTMT230100UVMedicine();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVMedicineManufacturer}
     * 
     */
    public COCTMT230100UVMedicineManufacturer createCOCTMT230100UVMedicineManufacturer() {
        return new COCTMT230100UVMedicineManufacturer();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSpecializedKind}
     * 
     */
    public COCTMT230100UVSpecializedKind createCOCTMT230100UVSpecializedKind() {
        return new COCTMT230100UVSpecializedKind();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVMedicineClass}
     * 
     */
    public COCTMT230100UVMedicineClass createCOCTMT230100UVMedicineClass() {
        return new COCTMT230100UVMedicineClass();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVPart}
     * 
     */
    public COCTMT230100UVPart createCOCTMT230100UVPart() {
        return new COCTMT230100UVPart();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject4}
     * 
     */
    public COCTMT230100UVSubject4 createCOCTMT230100UVSubject4() {
        return new COCTMT230100UVSubject4();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject2}
     * 
     */
    public COCTMT230100UVSubject2 createCOCTMT230100UVSubject2() {
        return new COCTMT230100UVSubject2();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject1}
     * 
     */
    public COCTMT230100UVSubject1 createCOCTMT230100UVSubject1() {
        return new COCTMT230100UVSubject1();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject22}
     * 
     */
    public COCTMT230100UVSubject22 createCOCTMT230100UVSubject22() {
        return new COCTMT230100UVSubject22();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject3}
     * 
     */
    public COCTMT230100UVSubject3 createCOCTMT230100UVSubject3() {
        return new COCTMT230100UVSubject3();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVObservationGoal}
     * 
     */
    public COCTMT230100UVObservationGoal createCOCTMT230100UVObservationGoal() {
        return new COCTMT230100UVObservationGoal();
    }

    /**
     * Create an instance of {@link  COCTMT230100UVSubject7}
     * 
     */
    public COCTMT230100UVSubject7 createCOCTMT230100UVSubject7() {
        return new COCTMT230100UVSubject7();
    }

    /**
     * Create an instance of {@link  COCTMT440001UVValuedItem}
     * 
     */
    public COCTMT440001UVValuedItem createCOCTMT440001UVValuedItem() {
        return new COCTMT440001UVValuedItem();
    }

    /**
     * Create an instance of {@link  RTOPQPQ}
     * 
     */
    public RTOPQPQ createRTOPQPQ() {
        return new RTOPQPQ();
    }

    /**
     * Create an instance of {@link  RTOMOPQ}
     * 
     */
    public RTOMOPQ createRTOMOPQ() {
        return new RTOMOPQ();
    }

    /**
     * Create an instance of {@link  MO}
     * 
     */
    public MO createMO() {
        return new MO();
    }

    /**
     * Create an instance of {@link  DocumentRoot}
     * 
     */
    public DocumentRoot createDocumentRoot() {
        return new DocumentRoot();
    }

    /**
     * Create an instance of {@link  EIVLEvent}
     * 
     */
    public EIVLEvent createEIVLEvent() {
        return new EIVLEvent();
    }

    /**
     * Create an instance of {@link  EIVLPPDTS}
     * 
     */
    public EIVLPPDTS createEIVLPPDTS() {
        return new EIVLPPDTS();
    }

    /**
     * Create an instance of {@link  SXCMPPDTS}
     * 
     */
    public SXCMPPDTS createSXCMPPDTS() {
        return new SXCMPPDTS();
    }

    /**
     * Create an instance of {@link  PPDTS}
     * 
     */
    public PPDTS createPPDTS() {
        return new PPDTS();
    }

    /**
     * Create an instance of {@link  IVLPPDPQ}
     * 
     */
    public IVLPPDPQ createIVLPPDPQ() {
        return new IVLPPDPQ();
    }

    /**
     * Create an instance of {@link  SXCMPPDPQ}
     * 
     */
    public SXCMPPDPQ createSXCMPPDPQ() {
        return new SXCMPPDPQ();
    }

    /**
     * Create an instance of {@link  PPDPQ}
     * 
     */
    public PPDPQ createPPDPQ() {
        return new PPDPQ();
    }

    /**
     * Create an instance of {@link  IVXBPPDPQ}
     * 
     */
    public IVXBPPDPQ createIVXBPPDPQ() {
        return new IVXBPPDPQ();
    }

    /**
     * Create an instance of {@link  EIVLTS}
     * 
     */
    public EIVLTS createEIVLTS() {
        return new EIVLTS();
    }

    /**
     * Create an instance of {@link  GLISTPQ}
     * 
     */
    public GLISTPQ createGLISTPQ() {
        return new GLISTPQ();
    }

    /**
     * Create an instance of {@link  GLISTTS}
     * 
     */
    public GLISTTS createGLISTTS() {
        return new GLISTTS();
    }

    /**
     * Create an instance of {@link  HXITCE}
     * 
     */
    public HXITCE createHXITCE() {
        return new HXITCE();
    }

    /**
     * Create an instance of {@link  HXITPQ}
     * 
     */
    public HXITPQ createHXITPQ() {
        return new HXITPQ();
    }

    /**
     * Create an instance of {@link  INT1}
     * 
     */
    public INT1 createINT1() {
        return new INT1();
    }

    /**
     * Create an instance of {@link  IVLINT}
     * 
     */
    public IVLINT createIVLINT() {
        return new IVLINT();
    }

    /**
     * Create an instance of {@link  SXCMINT}
     * 
     */
    public SXCMINT createSXCMINT() {
        return new SXCMINT();
    }

    /**
     * Create an instance of {@link  IVXBINT}
     * 
     */
    public IVXBINT createIVXBINT() {
        return new IVXBINT();
    }

    /**
     * Create an instance of {@link  IVLMO}
     * 
     */
    public IVLMO createIVLMO() {
        return new IVLMO();
    }

    /**
     * Create an instance of {@link  SXCMMO}
     * 
     */
    public SXCMMO createSXCMMO() {
        return new SXCMMO();
    }

    /**
     * Create an instance of {@link  IVXBMO}
     * 
     */
    public IVXBMO createIVXBMO() {
        return new IVXBMO();
    }

    /**
     * Create an instance of {@link  IVLPPDTS}
     * 
     */
    public IVLPPDTS createIVLPPDTS() {
        return new IVLPPDTS();
    }

    /**
     * Create an instance of {@link  IVXBPPDTS}
     * 
     */
    public IVXBPPDTS createIVXBPPDTS() {
        return new IVXBPPDTS();
    }

    /**
     * Create an instance of {@link  IVLREAL}
     * 
     */
    public IVLREAL createIVLREAL() {
        return new IVLREAL();
    }

    /**
     * Create an instance of {@link  SXCMREAL}
     * 
     */
    public SXCMREAL createSXCMREAL() {
        return new SXCMREAL();
    }

    /**
     * Create an instance of {@link  REAL1}
     * 
     */
    public REAL1 createREAL1() {
        return new REAL1();
    }

    /**
     * Create an instance of {@link  IVXBREAL}
     * 
     */
    public IVXBREAL createIVXBREAL() {
        return new IVXBREAL();
    }

    /**
     * Create an instance of {@link  PIVLPPDTS}
     * 
     */
    public PIVLPPDTS createPIVLPPDTS() {
        return new PIVLPPDTS();
    }

    /**
     * Create an instance of {@link  PIVLTS}
     * 
     */
    public PIVLTS createPIVLTS() {
        return new PIVLTS();
    }

    /**
     * Create an instance of {@link  PN}
     * 
     */
    public PN createPN() {
        return new PN();
    }

    /**
     * Create an instance of {@link  RTO}
     * 
     */
    public RTO createRTO() {
        return new RTO();
    }

    /**
     * Create an instance of {@link  SC}
     * 
     */
    public SC createSC() {
        return new SC();
    }

    /**
     * Create an instance of {@link  SLISTPQ}
     * 
     */
    public SLISTPQ createSLISTPQ() {
        return new SLISTPQ();
    }

    /**
     * Create an instance of {@link  SLISTTS}
     * 
     */
    public SLISTTS createSLISTTS() {
        return new SLISTTS();
    }

    /**
     * Create an instance of {@link  SXCMCD}
     * 
     */
    public SXCMCD createSXCMCD() {
        return new SXCMCD();
    }

    /**
     * Create an instance of {@link  SXPRTS}
     * 
     */
    public SXPRTS createSXPRTS() {
        return new SXPRTS();
    }

    /**
     * Create an instance of {@link  UVPTS}
     * 
     */
    public UVPTS createUVPTS() {
        return new UVPTS();
    }



	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDelimiter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "delimiter", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpDelimiter> createADDelimiter(net.ihe.gazelle.medication.AdxpDelimiter value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpDelimiter>(_ADDelimiter_QNAME, net.ihe.gazelle.medication.AdxpDelimiter.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpCountry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "country", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpCountry> createADCountry(net.ihe.gazelle.medication.AdxpCountry value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpCountry>(_ADCountry_QNAME, net.ihe.gazelle.medication.AdxpCountry.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpState }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "state", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpState> createADState(net.ihe.gazelle.medication.AdxpState value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpState>(_ADState_QNAME, net.ihe.gazelle.medication.AdxpState.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpCounty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "county", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpCounty> createADCounty(net.ihe.gazelle.medication.AdxpCounty value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpCounty>(_ADCounty_QNAME, net.ihe.gazelle.medication.AdxpCounty.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpCity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "city", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpCity> createADCity(net.ihe.gazelle.medication.AdxpCity value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpCity>(_ADCity_QNAME, net.ihe.gazelle.medication.AdxpCity.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpPostalCode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "postalCode", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpPostalCode> createADPostalCode(net.ihe.gazelle.medication.AdxpPostalCode value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpPostalCode>(_ADPostalCode_QNAME, net.ihe.gazelle.medication.AdxpPostalCode.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpStreetAddressLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "streetAddressLine", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpStreetAddressLine> createADStreetAddressLine(net.ihe.gazelle.medication.AdxpStreetAddressLine value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpStreetAddressLine>(_ADStreetAddressLine_QNAME, net.ihe.gazelle.medication.AdxpStreetAddressLine.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpHouseNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "houseNumber", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpHouseNumber> createADHouseNumber(net.ihe.gazelle.medication.AdxpHouseNumber value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpHouseNumber>(_ADHouseNumber_QNAME, net.ihe.gazelle.medication.AdxpHouseNumber.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpHouseNumberNumeric }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "houseNumberNumeric", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpHouseNumberNumeric> createADHouseNumberNumeric(net.ihe.gazelle.medication.AdxpHouseNumberNumeric value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpHouseNumberNumeric>(_ADHouseNumberNumeric_QNAME, net.ihe.gazelle.medication.AdxpHouseNumberNumeric.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDirection }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "direction", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpDirection> createADDirection(net.ihe.gazelle.medication.AdxpDirection value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpDirection>(_ADDirection_QNAME, net.ihe.gazelle.medication.AdxpDirection.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpStreetName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "streetName", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpStreetName> createADStreetName(net.ihe.gazelle.medication.AdxpStreetName value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpStreetName>(_ADStreetName_QNAME, net.ihe.gazelle.medication.AdxpStreetName.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpStreetNameBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "streetNameBase", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpStreetNameBase> createADStreetNameBase(net.ihe.gazelle.medication.AdxpStreetNameBase value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpStreetNameBase>(_ADStreetNameBase_QNAME, net.ihe.gazelle.medication.AdxpStreetNameBase.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpStreetNameType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "streetNameType", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpStreetNameType> createADStreetNameType(net.ihe.gazelle.medication.AdxpStreetNameType value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpStreetNameType>(_ADStreetNameType_QNAME, net.ihe.gazelle.medication.AdxpStreetNameType.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpAdditionalLocator }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "additionalLocator", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpAdditionalLocator> createADAdditionalLocator(net.ihe.gazelle.medication.AdxpAdditionalLocator value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpAdditionalLocator>(_ADAdditionalLocator_QNAME, net.ihe.gazelle.medication.AdxpAdditionalLocator.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpUnitID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "unitID", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpUnitID> createADUnitID(net.ihe.gazelle.medication.AdxpUnitID value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpUnitID>(_ADUnitID_QNAME, net.ihe.gazelle.medication.AdxpUnitID.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpUnitType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "unitType", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpUnitType> createADUnitType(net.ihe.gazelle.medication.AdxpUnitType value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpUnitType>(_ADUnitType_QNAME, net.ihe.gazelle.medication.AdxpUnitType.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpCareOf }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "careOf", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpCareOf> createADCareOf(net.ihe.gazelle.medication.AdxpCareOf value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpCareOf>(_ADCareOf_QNAME, net.ihe.gazelle.medication.AdxpCareOf.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpCensusTract }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "censusTract", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpCensusTract> createADCensusTract(net.ihe.gazelle.medication.AdxpCensusTract value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpCensusTract>(_ADCensusTract_QNAME, net.ihe.gazelle.medication.AdxpCensusTract.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryAddressLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "deliveryAddressLine", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpDeliveryAddressLine> createADDeliveryAddressLine(net.ihe.gazelle.medication.AdxpDeliveryAddressLine value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpDeliveryAddressLine>(_ADDeliveryAddressLine_QNAME, net.ihe.gazelle.medication.AdxpDeliveryAddressLine.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryInstallationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "deliveryInstallationType", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpDeliveryInstallationType> createADDeliveryInstallationType(net.ihe.gazelle.medication.AdxpDeliveryInstallationType value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpDeliveryInstallationType>(_ADDeliveryInstallationType_QNAME, net.ihe.gazelle.medication.AdxpDeliveryInstallationType.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryInstallationArea }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "deliveryInstallationArea", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpDeliveryInstallationArea> createADDeliveryInstallationArea(net.ihe.gazelle.medication.AdxpDeliveryInstallationArea value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpDeliveryInstallationArea>(_ADDeliveryInstallationArea_QNAME, net.ihe.gazelle.medication.AdxpDeliveryInstallationArea.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryInstallationQualifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "deliveryInstallationQualifier", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpDeliveryInstallationQualifier> createADDeliveryInstallationQualifier(net.ihe.gazelle.medication.AdxpDeliveryInstallationQualifier value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpDeliveryInstallationQualifier>(_ADDeliveryInstallationQualifier_QNAME, net.ihe.gazelle.medication.AdxpDeliveryInstallationQualifier.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryMode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "deliveryMode", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpDeliveryMode> createADDeliveryMode(net.ihe.gazelle.medication.AdxpDeliveryMode value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpDeliveryMode>(_ADDeliveryMode_QNAME, net.ihe.gazelle.medication.AdxpDeliveryMode.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryModeIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "deliveryModeIdentifier", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpDeliveryModeIdentifier> createADDeliveryModeIdentifier(net.ihe.gazelle.medication.AdxpDeliveryModeIdentifier value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpDeliveryModeIdentifier>(_ADDeliveryModeIdentifier_QNAME, net.ihe.gazelle.medication.AdxpDeliveryModeIdentifier.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpBuildingNumberSuffix }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "buildingNumberSuffix", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpBuildingNumberSuffix> createADBuildingNumberSuffix(net.ihe.gazelle.medication.AdxpBuildingNumberSuffix value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpBuildingNumberSuffix>(_ADBuildingNumberSuffix_QNAME, net.ihe.gazelle.medication.AdxpBuildingNumberSuffix.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpPostBox }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "postBox", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpPostBox> createADPostBox(net.ihe.gazelle.medication.AdxpPostBox value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpPostBox>(_ADPostBox_QNAME, net.ihe.gazelle.medication.AdxpPostBox.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpPrecinct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "precinct", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.AdxpPrecinct> createADPrecinct(net.ihe.gazelle.medication.AdxpPrecinct value) {
        return new JAXBElement<net.ihe.gazelle.medication.AdxpPrecinct>(_ADPrecinct_QNAME, net.ihe.gazelle.medication.AdxpPrecinct.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link SXCMTS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "useablePeriod", scope = AD.class)
    public JAXBElement<net.ihe.gazelle.medication.SXCMTS> createADUseablePeriod(net.ihe.gazelle.medication.SXCMTS value) {
        return new JAXBElement<net.ihe.gazelle.medication.SXCMTS>(_ADUseablePeriod_QNAME, net.ihe.gazelle.medication.SXCMTS.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link TEL }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "reference", scope = ED.class)
    public JAXBElement<net.ihe.gazelle.medication.TEL> createEDReference(net.ihe.gazelle.medication.TEL value) {
        return new JAXBElement<net.ihe.gazelle.medication.TEL>(_EDReference_QNAME, net.ihe.gazelle.medication.TEL.class, ED.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link Thumbnail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "thumbnail", scope = ED.class)
    public JAXBElement<net.ihe.gazelle.medication.Thumbnail> createEDThumbnail(net.ihe.gazelle.medication.Thumbnail value) {
        return new JAXBElement<net.ihe.gazelle.medication.Thumbnail>(_EDThumbnail_QNAME, net.ihe.gazelle.medication.Thumbnail.class, ED.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnDelimiter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "delimiter", scope = EN.class)
    public JAXBElement<net.ihe.gazelle.medication.EnDelimiter> createENDelimiter(net.ihe.gazelle.medication.EnDelimiter value) {
        return new JAXBElement<net.ihe.gazelle.medication.EnDelimiter>(_ENDelimiter_QNAME, net.ihe.gazelle.medication.EnDelimiter.class, EN.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnFamily }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "family", scope = EN.class)
    public JAXBElement<net.ihe.gazelle.medication.EnFamily> createENFamily(net.ihe.gazelle.medication.EnFamily value) {
        return new JAXBElement<net.ihe.gazelle.medication.EnFamily>(_ENFamily_QNAME, net.ihe.gazelle.medication.EnFamily.class, EN.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnGiven }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "given", scope = EN.class)
    public JAXBElement<net.ihe.gazelle.medication.EnGiven> createENGiven(net.ihe.gazelle.medication.EnGiven value) {
        return new JAXBElement<net.ihe.gazelle.medication.EnGiven>(_ENGiven_QNAME, net.ihe.gazelle.medication.EnGiven.class, EN.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnPrefix }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "prefix", scope = EN.class)
    public JAXBElement<net.ihe.gazelle.medication.EnPrefix> createENPrefix(net.ihe.gazelle.medication.EnPrefix value) {
        return new JAXBElement<net.ihe.gazelle.medication.EnPrefix>(_ENPrefix_QNAME, net.ihe.gazelle.medication.EnPrefix.class, EN.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnSuffix }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "suffix", scope = EN.class)
    public JAXBElement<net.ihe.gazelle.medication.EnSuffix> createENSuffix(net.ihe.gazelle.medication.EnSuffix value) {
        return new JAXBElement<net.ihe.gazelle.medication.EnSuffix>(_ENSuffix_QNAME, net.ihe.gazelle.medication.EnSuffix.class, EN.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link IVLTS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "validTime", scope = EN.class)
    public JAXBElement<net.ihe.gazelle.medication.IVLTS> createENValidTime(net.ihe.gazelle.medication.IVLTS value) {
        return new JAXBElement<net.ihe.gazelle.medication.IVLTS>(_ENValidTime_QNAME, net.ihe.gazelle.medication.IVLTS.class, EN.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "xmlns:prefix", scope = DocumentRoot.class)
    public JAXBElement<String> createDocumentRootXMLNSPrefixMap(String value) {
        return new JAXBElement<String>(_DocumentRootXMLNSPrefixMap_QNAME, String.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "xsi:schemaLocation", scope = DocumentRoot.class)
    public JAXBElement<String> createDocumentRootXSISchemaLocation(String value) {
        return new JAXBElement<String>(_DocumentRootXSISchemaLocation_QNAME, String.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVContent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "asContent", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVContent> createDocumentRootAsContent(net.ihe.gazelle.medication.COCTMT230100UVContent value) {
        return new JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVContent>(_DocumentRootAsContent_QNAME, net.ihe.gazelle.medication.COCTMT230100UVContent.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVDistributedProduct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "asDistributedProduct", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVDistributedProduct> createDocumentRootAsDistributedProduct(net.ihe.gazelle.medication.COCTMT230100UVDistributedProduct value) {
        return new JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVDistributedProduct>(_DocumentRootAsDistributedProduct_QNAME, net.ihe.gazelle.medication.COCTMT230100UVDistributedProduct.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVMedicineManufacturer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "asMedicineManufacturer", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVMedicineManufacturer> createDocumentRootAsMedicineManufacturer(net.ihe.gazelle.medication.COCTMT230100UVMedicineManufacturer value) {
        return new JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVMedicineManufacturer>(_DocumentRootAsMedicineManufacturer_QNAME, net.ihe.gazelle.medication.COCTMT230100UVMedicineManufacturer.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVSpecializedKind }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "asSpecializedKind", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVSpecializedKind> createDocumentRootAsSpecializedKind(net.ihe.gazelle.medication.COCTMT230100UVSpecializedKind value) {
        return new JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVSpecializedKind>(_DocumentRootAsSpecializedKind_QNAME, net.ihe.gazelle.medication.COCTMT230100UVSpecializedKind.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link ED }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "desc", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.ED> createDocumentRootDesc(net.ihe.gazelle.medication.ED value) {
        return new JAXBElement<net.ihe.gazelle.medication.ED>(_DocumentRootDesc_QNAME, net.ihe.gazelle.medication.ED.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link IVLTS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "expirationTime", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.IVLTS> createDocumentRootExpirationTime(net.ihe.gazelle.medication.IVLTS value) {
        return new JAXBElement<net.ihe.gazelle.medication.IVLTS>(_DocumentRootExpirationTime_QNAME, net.ihe.gazelle.medication.IVLTS.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link CE }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "formCode", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.CE> createDocumentRootFormCode(net.ihe.gazelle.medication.CE value) {
        return new JAXBElement<net.ihe.gazelle.medication.CE>(_DocumentRootFormCode_QNAME, net.ihe.gazelle.medication.CE.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link CE }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "handlingCode", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.CE> createDocumentRootHandlingCode(net.ihe.gazelle.medication.CE value) {
        return new JAXBElement<net.ihe.gazelle.medication.CE>(_DocumentRootHandlingCode_QNAME, net.ihe.gazelle.medication.CE.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link II }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "id", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.II> createDocumentRootId(net.ihe.gazelle.medication.II value) {
        return new JAXBElement<net.ihe.gazelle.medication.II>(_DocumentRootId_QNAME, net.ihe.gazelle.medication.II.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVIngredient }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "ingredient", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVIngredient> createDocumentRootIngredient(net.ihe.gazelle.medication.COCTMT230100UVIngredient value) {
        return new JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVIngredient>(_DocumentRootIngredient_QNAME, net.ihe.gazelle.medication.COCTMT230100UVIngredient.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVMedication }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "medication", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVMedication> createDocumentRootMedication(net.ihe.gazelle.medication.COCTMT230100UVMedication value) {
        return new JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVMedication>(_DocumentRootMedication_QNAME, net.ihe.gazelle.medication.COCTMT230100UVMedication.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVPart }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "part", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVPart> createDocumentRootPart(net.ihe.gazelle.medication.COCTMT230100UVPart value) {
        return new JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVPart>(_DocumentRootPart_QNAME, net.ihe.gazelle.medication.COCTMT230100UVPart.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link CE }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "riskCode", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.CE> createDocumentRootRiskCode(net.ihe.gazelle.medication.CE value) {
        return new JAXBElement<net.ihe.gazelle.medication.CE>(_DocumentRootRiskCode_QNAME, net.ihe.gazelle.medication.CE.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link IVLTS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "stabilityTime", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.IVLTS> createDocumentRootStabilityTime(net.ihe.gazelle.medication.IVLTS value) {
        return new JAXBElement<net.ihe.gazelle.medication.IVLTS>(_DocumentRootStabilityTime_QNAME, net.ihe.gazelle.medication.IVLTS.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVSubject2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "subjectOf1", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVSubject2> createDocumentRootSubjectOf1(net.ihe.gazelle.medication.COCTMT230100UVSubject2 value) {
        return new JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVSubject2>(_DocumentRootSubjectOf1_QNAME, net.ihe.gazelle.medication.COCTMT230100UVSubject2.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVSubject1 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "subjectOf2", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVSubject1> createDocumentRootSubjectOf2(net.ihe.gazelle.medication.COCTMT230100UVSubject1 value) {
        return new JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVSubject1>(_DocumentRootSubjectOf2_QNAME, net.ihe.gazelle.medication.COCTMT230100UVSubject1.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVSubject22 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "subjectOf3", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVSubject22> createDocumentRootSubjectOf3(net.ihe.gazelle.medication.COCTMT230100UVSubject22 value) {
        return new JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVSubject22>(_DocumentRootSubjectOf3_QNAME, net.ihe.gazelle.medication.COCTMT230100UVSubject22.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVSubject3 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "subjectOf4", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVSubject3> createDocumentRootSubjectOf4(net.ihe.gazelle.medication.COCTMT230100UVSubject3 value) {
        return new JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVSubject3>(_DocumentRootSubjectOf4_QNAME, net.ihe.gazelle.medication.COCTMT230100UVSubject3.class, DocumentRoot.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link COCTMT230100UVSubject7 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epsos-org:ep:medication", name = "subjectOf5", scope = DocumentRoot.class)
    public JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVSubject7> createDocumentRootSubjectOf5(net.ihe.gazelle.medication.COCTMT230100UVSubject7 value) {
        return new JAXBElement<net.ihe.gazelle.medication.COCTMT230100UVSubject7>(_DocumentRootSubjectOf5_QNAME, net.ihe.gazelle.medication.COCTMT230100UVSubject7.class, DocumentRoot.class, value);
    }

}
/**
 * XDocumentActMood.java
 *
 * File generated from the medication::XDocumentActMood uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration XDocumentActMood.
 *
 * 
 */

@XmlType(name = "XDocumentActMood")
@XmlEnum
@XmlRootElement(name = "XDocumentActMood")
public enum XDocumentActMood {
	@XmlEnumValue("INT")
	INT("INT"),
	@XmlEnumValue("APT")
	APT("APT"),
	@XmlEnumValue("ARQ")
	ARQ("ARQ"),
	@XmlEnumValue("DEF")
	DEF("DEF"),
	@XmlEnumValue("EVN")
	EVN("EVN"),
	@XmlEnumValue("PRMS")
	PRMS("PRMS"),
	@XmlEnumValue("PRP")
	PRP("PRP"),
	@XmlEnumValue("RQO")
	RQO("RQO");
	
	private final String value;

    XDocumentActMood(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static XDocumentActMood fromValue(String v) {
        for (XDocumentActMood c: XDocumentActMood.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
/**
 * MultipartMediaType.java
 *
 * File generated from the medication::MultipartMediaType uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration MultipartMediaType.
 *
 * 
 */

@XmlType(name = "MultipartMediaType")
@XmlEnum
@XmlRootElement(name = "MultipartMediaType")
public enum MultipartMediaType {
	@XmlEnumValue("multipart/x-hl7-cda-level1")
	MULTIPARTXHL7CDALEVEL1("multipart/x-hl7-cda-level1");
	
	private final String value;

    MultipartMediaType(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static MultipartMediaType fromValue(String v) {
        for (MultipartMediaType c: MultipartMediaType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
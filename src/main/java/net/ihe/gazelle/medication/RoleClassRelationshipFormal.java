/**
 * RoleClassRelationshipFormal.java
 *
 * File generated from the medication::RoleClassRelationshipFormal uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration RoleClassRelationshipFormal.
 *
 * 
 */

@XmlType(name = "RoleClassRelationshipFormal")
@XmlEnum
@XmlRootElement(name = "RoleClassRelationshipFormal")
public enum RoleClassRelationshipFormal {
	@XmlEnumValue("CIT")
	CIT("CIT"),
	@XmlEnumValue("COVPTY")
	COVPTY("COVPTY"),
	@XmlEnumValue("CRINV")
	CRINV("CRINV"),
	@XmlEnumValue("CRSPNSR")
	CRSPNSR("CRSPNSR"),
	@XmlEnumValue("GUAR")
	GUAR("GUAR"),
	@XmlEnumValue("PAT")
	PAT("PAT"),
	@XmlEnumValue("PAYEE")
	PAYEE("PAYEE"),
	@XmlEnumValue("PAYOR")
	PAYOR("PAYOR"),
	@XmlEnumValue("POLHOLD")
	POLHOLD("POLHOLD"),
	@XmlEnumValue("QUAL")
	QUAL("QUAL"),
	@XmlEnumValue("SPNSR")
	SPNSR("SPNSR"),
	@XmlEnumValue("STD")
	STD("STD"),
	@XmlEnumValue("UNDWRT")
	UNDWRT("UNDWRT"),
	@XmlEnumValue("LIC")
	LIC("LIC"),
	@XmlEnumValue("NOT")
	NOT("NOT"),
	@XmlEnumValue("PROV")
	PROV("PROV"),
	@XmlEnumValue("AGNT")
	AGNT("AGNT"),
	@XmlEnumValue("GUARD")
	GUARD("GUARD"),
	@XmlEnumValue("ASSIGNED")
	ASSIGNED("ASSIGNED"),
	@XmlEnumValue("COMPAR")
	COMPAR("COMPAR"),
	@XmlEnumValue("SGNOFF")
	SGNOFF("SGNOFF"),
	@XmlEnumValue("CON")
	CON("CON"),
	@XmlEnumValue("ECON")
	ECON("ECON"),
	@XmlEnumValue("NOK")
	NOK("NOK"),
	@XmlEnumValue("EMP")
	EMP("EMP"),
	@XmlEnumValue("MIL")
	MIL("MIL"),
	@XmlEnumValue("INVSBJ")
	INVSBJ("INVSBJ"),
	@XmlEnumValue("CASESBJ")
	CASESBJ("CASESBJ"),
	@XmlEnumValue("RESBJ")
	RESBJ("RESBJ");
	
	private final String value;

    RoleClassRelationshipFormal(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static RoleClassRelationshipFormal fromValue(String v) {
        for (RoleClassRelationshipFormal c: RoleClassRelationshipFormal.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
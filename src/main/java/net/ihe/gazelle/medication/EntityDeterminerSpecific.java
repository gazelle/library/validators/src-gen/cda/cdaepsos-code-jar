/**
 * EntityDeterminerSpecific.java
 *
 * File generated from the medication::EntityDeterminerSpecific uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration EntityDeterminerSpecific.
 *
 */

@XmlType(name = "EntityDeterminerSpecific")
@XmlEnum
@XmlRootElement(name = "EntityDeterminerSpecific")
public enum EntityDeterminerSpecific {
	@XmlEnumValue("INSTANCE")
	INSTANCE("INSTANCE");
	
	private final String value;

    EntityDeterminerSpecific(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static EntityDeterminerSpecific fromValue(String v) {
        for (EntityDeterminerSpecific c: EntityDeterminerSpecific.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
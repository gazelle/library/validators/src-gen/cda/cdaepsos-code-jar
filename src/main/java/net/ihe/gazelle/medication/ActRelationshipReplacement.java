/**
 * ActRelationshipReplacement.java
 *
 * File generated from the medication::ActRelationshipReplacement uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration ActRelationshipReplacement.
 *
 * 
 */

@XmlType(name = "ActRelationshipReplacement")
@XmlEnum
@XmlRootElement(name = "ActRelationshipReplacement")
public enum ActRelationshipReplacement {
	@XmlEnumValue("RPLC")
	RPLC("RPLC"),
	@XmlEnumValue("SUCC")
	SUCC("SUCC");
	
	private final String value;

    ActRelationshipReplacement(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static ActRelationshipReplacement fromValue(String v) {
        for (ActRelationshipReplacement c: ActRelationshipReplacement.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
/**
 * EntityClassContainer.java
 *
 * File generated from the medication::EntityClassContainer uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration EntityClassContainer.
 *
 * 
 */

@XmlType(name = "EntityClassContainer")
@XmlEnum
@XmlRootElement(name = "EntityClassContainer")
public enum EntityClassContainer {
	@XmlEnumValue("CONT")
	CONT("CONT"),
	@XmlEnumValue("HOLD")
	HOLD("HOLD");
	
	private final String value;

    EntityClassContainer(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static EntityClassContainer fromValue(String v) {
        for (EntityClassContainer c: EntityClassContainer.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
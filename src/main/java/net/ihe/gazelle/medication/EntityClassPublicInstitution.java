/**
 * EntityClassPublicInstitution.java
 *
 * File generated from the medication::EntityClassPublicInstitution uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration EntityClassPublicInstitution.
 *
 */

@XmlType(name = "EntityClassPublicInstitution")
@XmlEnum
@XmlRootElement(name = "EntityClassPublicInstitution")
public enum EntityClassPublicInstitution {
	@XmlEnumValue("PUB")
	PUB("PUB");
	
	private final String value;

    EntityClassPublicInstitution(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static EntityClassPublicInstitution fromValue(String v) {
        for (EntityClassPublicInstitution c: EntityClassPublicInstitution.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
/**
 * COCTMT230100UVSpecializedKind.java
 *
 * File generated from the medication::COCTMT230100UVSpecializedKind uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


/**
 * Description of the class COCTMT230100UVSpecializedKind.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "COCT_MT230100UV.SpecializedKind", propOrder = {
	"realmCode",
	"typeId",
	"templateId",
	"code",
	"generalizedMedicineClass",
	"classCode",
	"nullFlavor"
})
@XmlRootElement(name = "asSpecializedKind")
public class COCTMT230100UVSpecializedKind implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@XmlElement(name = "realmCode", namespace = "urn:hl7-org:v3")
	public List<net.ihe.gazelle.datatypes.CS> realmCode;
	@XmlElement(name = "typeId", namespace = "urn:hl7-org:v3")
	public net.ihe.gazelle.infr.AllInfrastructureRootTypeId typeId;
	@XmlElement(name = "templateId", namespace = "urn:hl7-org:v3")
	public List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> templateId;
	@XmlElement(name = "code", namespace = "urn:epsos-org:ep:medication")
	public net.ihe.gazelle.medication.CE code;
	@XmlElement(name = "generalizedMedicineClass", namespace = "urn:epsos-org:ep:medication")
	public net.ihe.gazelle.medication.COCTMT230100UVMedicineClass generalizedMedicineClass;
	@XmlAttribute(name = "classCode", required = true)
	public net.ihe.gazelle.medication.RoleClassIsSpeciesEntity classCode;
	@XmlAttribute(name = "nullFlavor")
	public net.ihe.gazelle.medication.NullFlavor nullFlavor;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private org.w3c.dom.Node _xmlNodePresentation;
	
	
	/**
	 * Return realmCode.
	 * @return realmCode
	 */
	public List<net.ihe.gazelle.datatypes.CS> getRealmCode() {
		if (realmCode == null) {
	        realmCode = new ArrayList<net.ihe.gazelle.datatypes.CS>();
	    }
	    return realmCode;
	}
	
	/**
	 * Set a value to attribute realmCode.
	 * @param realmCode.
	 */
	public void setRealmCode(List<net.ihe.gazelle.datatypes.CS> realmCode) {
	    this.realmCode = realmCode;
	}
	
	
	
	/**
	 * Add a realmCode to the realmCode collection.
	 * @param realmCode_elt Element to add.
	 */
	public void addRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
	    this.realmCode.add(realmCode_elt);
	}
	
	/**
	 * Remove a realmCode to the realmCode collection.
	 * @param realmCode_elt Element to remove
	 */
	public void removeRealmCode(net.ihe.gazelle.datatypes.CS realmCode_elt) {
	    this.realmCode.remove(realmCode_elt);
	}
	
	/**
	 * Return typeId.
	 * @return typeId
	 */
	public net.ihe.gazelle.infr.AllInfrastructureRootTypeId getTypeId() {
	    return typeId;
	}
	
	/**
	 * Set a value to attribute typeId.
	 * @param typeId.
	 */
	public void setTypeId(net.ihe.gazelle.infr.AllInfrastructureRootTypeId typeId) {
	    this.typeId = typeId;
	}
	
	
	
	
	/**
	 * Return templateId.
	 * @return templateId
	 */
	public List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> getTemplateId() {
		if (templateId == null) {
	        templateId = new ArrayList<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId>();
	    }
	    return templateId;
	}
	
	/**
	 * Set a value to attribute templateId.
	 * @param templateId.
	 */
	public void setTemplateId(List<net.ihe.gazelle.infr.AllInfrastructureRootTemplateId> templateId) {
	    this.templateId = templateId;
	}
	
	
	
	/**
	 * Add a templateId to the templateId collection.
	 * @param templateId_elt Element to add.
	 */
	public void addTemplateId(net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId_elt) {
	    this.templateId.add(templateId_elt);
	}
	
	/**
	 * Remove a templateId to the templateId collection.
	 * @param templateId_elt Element to remove
	 */
	public void removeTemplateId(net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId_elt) {
	    this.templateId.remove(templateId_elt);
	}
	
	/**
	 * Return code.
	 * @return code
	 */
	public net.ihe.gazelle.medication.CE getCode() {
	    return code;
	}
	
	/**
	 * Set a value to attribute code.
	 * @param code.
	 */
	public void setCode(net.ihe.gazelle.medication.CE code) {
	    this.code = code;
	}
	
	
	
	
	/**
	 * Return generalizedMedicineClass.
	 * @return generalizedMedicineClass
	 */
	public net.ihe.gazelle.medication.COCTMT230100UVMedicineClass getGeneralizedMedicineClass() {
	    return generalizedMedicineClass;
	}
	
	/**
	 * Set a value to attribute generalizedMedicineClass.
	 * @param generalizedMedicineClass.
	 */
	public void setGeneralizedMedicineClass(net.ihe.gazelle.medication.COCTMT230100UVMedicineClass generalizedMedicineClass) {
	    this.generalizedMedicineClass = generalizedMedicineClass;
	}
	
	
	
	
	/**
	 * Return classCode.
	 * @return classCode
	 */
	public net.ihe.gazelle.medication.RoleClassIsSpeciesEntity getClassCode() {
	    return classCode;
	}
	
	/**
	 * Set a value to attribute classCode.
	 * @param classCode.
	 */
	public void setClassCode(net.ihe.gazelle.medication.RoleClassIsSpeciesEntity classCode) {
	    this.classCode = classCode;
	}
	
	
	
	
	/**
	 * Return nullFlavor.
	 * @return nullFlavor
	 */
	public net.ihe.gazelle.medication.NullFlavor getNullFlavor() {
	    return nullFlavor;
	}
	
	/**
	 * Set a value to attribute nullFlavor.
	 * @param nullFlavor.
	 */
	public void setNullFlavor(net.ihe.gazelle.medication.NullFlavor nullFlavor) {
	    this.nullFlavor = nullFlavor;
	}
	
	
	
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.medication");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:epsos-org:ep:medication", "asSpecializedKind").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(COCTMT230100UVSpecializedKind cOCTMT230100UVSpecializedKind, String location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (cOCTMT230100UVSpecializedKind != null){
   			cvm.validate(cOCTMT230100UVSpecializedKind, location, diagnostic);
			{
				int i = 0;
				for (net.ihe.gazelle.datatypes.CS realmCode: cOCTMT230100UVSpecializedKind.getRealmCode()){
					net.ihe.gazelle.datatypes.CS.validateByModule(realmCode, location + "/realmCode[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			net.ihe.gazelle.infr.AllInfrastructureRootTypeId.validateByModule(cOCTMT230100UVSpecializedKind.getTypeId(), location + "/typeId", cvm, diagnostic);
			{
				int i = 0;
				for (net.ihe.gazelle.infr.AllInfrastructureRootTemplateId templateId: cOCTMT230100UVSpecializedKind.getTemplateId()){
					net.ihe.gazelle.infr.AllInfrastructureRootTemplateId.validateByModule(templateId, location + "/templateId[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			net.ihe.gazelle.medication.CE.validateByModule(cOCTMT230100UVSpecializedKind.getCode(), location + "/code", cvm, diagnostic);
			net.ihe.gazelle.medication.COCTMT230100UVMedicineClass.validateByModule(cOCTMT230100UVSpecializedKind.getGeneralizedMedicineClass(), location + "/generalizedMedicineClass", cvm, diagnostic);
    	}
    }

}
/**
 * RoleClassInvestigationSubject.java
 *
 * File generated from the medication::RoleClassInvestigationSubject uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration RoleClassInvestigationSubject.
 *
 * 
 */

@XmlType(name = "RoleClassInvestigationSubject")
@XmlEnum
@XmlRootElement(name = "RoleClassInvestigationSubject")
public enum RoleClassInvestigationSubject {
	@XmlEnumValue("INVSBJ")
	INVSBJ("INVSBJ"),
	@XmlEnumValue("CASESBJ")
	CASESBJ("CASESBJ"),
	@XmlEnumValue("RESBJ")
	RESBJ("RESBJ");
	
	private final String value;

    RoleClassInvestigationSubject(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static RoleClassInvestigationSubject fromValue(String v) {
        for (RoleClassInvestigationSubject c: RoleClassInvestigationSubject.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
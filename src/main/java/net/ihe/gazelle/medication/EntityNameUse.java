/**
 * EntityNameUse.java
 *
 * File generated from the medication::EntityNameUse uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/**
 * Description of the enumeration EntityNameUse.
 *
 * 
 */

@XmlType(name = "EntityNameUse")
@XmlEnum
@XmlRootElement(name = "EntityNameUse")
public enum EntityNameUse {
	@XmlEnumValue("C")
	C("C"),
	@XmlEnumValue("SRCH")
	SRCH("SRCH"),
	@XmlEnumValue("PHON")
	PHON("PHON"),
	@XmlEnumValue("SNDX")
	SNDX("SNDX"),
	@XmlEnumValue("ABC")
	ABC("ABC"),
	@XmlEnumValue("IDE")
	IDE("IDE"),
	@XmlEnumValue("SYL")
	SYL("SYL"),
	@XmlEnumValue("L")
	L("L"),
	@XmlEnumValue("A")
	A("A"),
	@XmlEnumValue("ASGN")
	ASGN("ASGN"),
	@XmlEnumValue("I")
	I("I"),
	@XmlEnumValue("R")
	R("R"),
	@XmlEnumValue("P")
	P("P");
	
	private final String value;

    EntityNameUse(String v) {
        value = v;
    }
    
     public String value() {
        return value;
    }

    public static EntityNameUse fromValue(String v) {
        for (EntityNameUse c: EntityNameUse.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
}
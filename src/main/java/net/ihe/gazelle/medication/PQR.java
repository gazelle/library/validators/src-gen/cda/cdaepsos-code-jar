/**
 * PQR.java
 *
 * File generated from the medication::PQR uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.medication;

// End of user code
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


/**
 * Description of the class PQR.
 *
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PQR", propOrder = {
	"value"
})
@XmlRootElement(name = "PQR")
public class PQR extends net.ihe.gazelle.medication.CV implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	/**
	 *                       The magnitude of the measurement
	                            value in terms of                      the unit specified in the code.
	                        .
	 */
	@XmlAttribute(name = "value")
	public java.lang.String value;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private org.w3c.dom.Node _xmlNodePresentation;
	
	
	/**
	 * Return value.
	 * @return value :                       The magnitude of the measurement
	                            value in terms of                      the unit specified in the code.
	                                              The magnitude of the measurement
	                            value in terms of                      the unit specified in the code.
	                                              The magnitude of the measurement
	                            value in terms of                      the unit specified in the code.
	                                              The magnitude of the measurement
	                            value in terms of                      the unit specified in the code.
	                                              The magnitude of the measurement
	                            value in terms of                      the unit specified in the code.
	                                              The magnitude of the measurement
	                            value in terms of                      the unit specified in the code.
	                                              The magnitude of the measurement
	                            value in terms of                      the unit specified in the code.
	                        
	 */
	public java.lang.String getValue() {
	    return value;
	}
	
	/**
	 * Set a value to attribute value.
	 * @param value :                       The magnitude of the measurement
	                            value in terms of                      the unit specified in the code.
	                                              The magnitude of the measurement
	                            value in terms of                      the unit specified in the code.
	                                              The magnitude of the measurement
	                            value in terms of                      the unit specified in the code.
	                                              The magnitude of the measurement
	                            value in terms of                      the unit specified in the code.
	                                              The magnitude of the measurement
	                            value in terms of                      the unit specified in the code.
	                                              The magnitude of the measurement
	                            value in terms of                      the unit specified in the code.
	                                              The magnitude of the measurement
	                            value in terms of                      the unit specified in the code.
	                        .
	 */
	public void setValue(java.lang.String value) {
	    this.value = value;
	}
	
	
	
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.medication");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:epsos-org:ep:medication", "PQR").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(PQR pQR, String location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (pQR != null){
   			net.ihe.gazelle.medication.CV.validateByModule(pQR, location, cvm, diagnostic);
    	}
    }

}
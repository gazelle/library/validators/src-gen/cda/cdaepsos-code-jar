package net.ihe.gazelle.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlType;

public class UpdateNames {
	
	public static void main(String[] args) throws IOException {
		File fil = new File("/home/aboufahj/workspaceTopcased/cdaepsos-code-jar/src/main/java/net/ihe/gazelle/medication2");
		for (File fis : fil.listFiles()) {
			if (fis.getName().endsWith(".java")) {
				String content = readDoc(fis.getAbsolutePath());
				Pattern pat = Pattern.compile("@XmlType\\(name = \"([^\"]*?)2\"");
				Matcher mat = pat.matcher(content);
				if (mat.find()) {
					content  = content.replace(mat.group(), "@XmlType(name = \"" + mat.group(1) + "\"");
				}
				printDoc(content, fis.getAbsolutePath());
			}
		}
	}
	
	public static void printDoc(String doc, String name) throws IOException {
		FileWriter fw = new FileWriter(new File(name));
		fw.append(doc);
		fw.close();
	}

	public static String readDoc(String name) throws IOException {
		BufferedReader scanner = new BufferedReader(new FileReader(name));
		StringBuilder res = new StringBuilder();
		try {
			String line = scanner.readLine();
			while (line != null) {
				res.append(line + "\n");
				line = scanner.readLine();
			}
		} finally {
			scanner.close();
		}
		return res.toString();
	}

}

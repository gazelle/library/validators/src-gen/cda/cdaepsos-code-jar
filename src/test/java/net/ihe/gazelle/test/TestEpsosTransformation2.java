package net.ihe.gazelle.test;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import net.ihe.gazelle.cdaepsos3.POCDMT000040ClinicalDocument;
import net.ihe.gazelle.common.marshaller.ObjectMarshallerImpl;

public class TestEpsosTransformation2 {

	@Test
	public void test1() throws FileNotFoundException, JAXBException {
		ObjectMarshallerImpl<POCDMT000040ClinicalDocument> om = new ObjectMarshallerImpl<POCDMT000040ClinicalDocument>(POCDMT000040ClinicalDocument.class);
		System.setProperty("javax.xml.parsers.SAXParserFactory","com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl");
		POCDMT000040ClinicalDocument cl = om.unmarshall(new FileInputStream("src/test/resources/eD.xml"));
		om.marshall(cl, System.out);
	}

}
